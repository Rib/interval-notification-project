package com.tasogare_soft.intervalnotification2.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.tasogare_soft.intervalnotification2.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TodaysIntervalsFragment extends Fragment {
    private final String LOG_TAG = TodaysIntervalsFragment.class.getSimpleName();

    Button mSearchButton;
    LinearLayout mTodaysIntervalsButton;
    ListView mTodaysIntervalsListView;

    public TodaysIntervalsFragment() {
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_menu, menu);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_todays_intervals, container, false);

        mSearchButton = (Button) view.findViewById(R.id.todays_intervals_search_button);
        mTodaysIntervalsButton = (LinearLayout) view.findViewById(R.id.todays_test_button);
        mTodaysIntervalsListView = (ListView) view.findViewById(R.id.todays_intervals_list);

        mSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.v(LOG_TAG, "Search button pressed");
            }
        });

        mTodaysIntervalsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.v(LOG_TAG, "Todays intervals button pressed");
            }
        });

        // Inflate the layout for this fragment
        return view;
    }

}
