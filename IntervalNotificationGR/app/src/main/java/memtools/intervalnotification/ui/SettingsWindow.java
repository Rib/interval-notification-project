package memtools.intervalnotification.ui;

import memtools.intervalnotification.IntervalNotification;
import memtools.intervalnotification.R;
import memtools.intervalnotification.Libs.Preferences;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

public class SettingsWindow {
	Context context;
	
	//fields
	EditText days_intervals_amount;
	EditText update_time;
	EditText target_success_rate;
	Button save_button;
	CheckBox minute_interval_notification;
	CheckBox lock_orientations;
	
	
	public SettingsWindow (Context context) {
		this.context = context;
	}
	
	public void setVisible() {
		((IntervalNotification)context).setContentView(R.layout.settings);
	}
	
	public void loadWindowContent() {
		findFields();
		
		//getting preferences
		SharedPreferences prefs = context.getSharedPreferences(
				IntervalNotification.SETTINGS_FILE, 0);

		//setting values for fields
		days_intervals_amount.setText(String.valueOf(prefs.getInt(
				Preferences.DAYS_INTERVALS.toString(), 100)));
		
		update_time.setText(String.valueOf(prefs.getLong(
				Preferences.DAY_TICK.toString(), 4*1000*60*60)));
		
		minute_interval_notification.setChecked(prefs.getBoolean(
				Preferences.MINUTE_NOTIFICATION.toString(), true));
		
		target_success_rate.setText(String.valueOf(prefs.getFloat(
				Preferences.TARGET_SUCCESS_RATE.toString(), 0.8f)));
		
		lock_orientations.setChecked(prefs.getBoolean(
				Preferences.ORIENTATION_LOCK.toString(), true));
		
		//adding onclick action for save button
		save_button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				save();
				//setting up message when this is called
				String cs = context.getString(R.string.settings);
				cs += " " + context.getString(R.string.were_modified);
				
				//setting up time this notification is displayed
				int duration = Toast.LENGTH_SHORT;
				
				Toast toast = Toast.makeText(context, cs, duration);
				toast.show();
			}
			
		});
		
		//setting done button action
		days_intervals_amount.setOnEditorActionListener(new OnEditorActionListener(){

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE) {
					save();
					update_time.requestFocus();
					return true;
				} else if (actionId == EditorInfo.IME_ACTION_NEXT) {
					save();
					update_time.requestFocus();
					return true;
				}

				return false;
			}
			
		});
		
		//setting done button action
		update_time.setOnEditorActionListener(new OnEditorActionListener(){

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE) {
					save();
					minute_interval_notification.requestFocus();
					return true;
				} else if (actionId == EditorInfo.IME_ACTION_NEXT) {
					save();
					minute_interval_notification.requestFocus();
					return true;
				}

				return false;
			}
			
		});
		
		//minute interval on and off
		minute_interval_notification.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				//just saving
				save();				
			}
			
		});
		
		lock_orientations.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				//just saving
				save();
			}
			
		});
	}
	
	private void save() {
		//just reading new contents and saving them into preferencese
		
		//getting preferences
		SharedPreferences prefs = context.getSharedPreferences(
				IntervalNotification.SETTINGS_FILE, 0);
		
		//getting edit
		Editor edit = prefs.edit();
		
		//setting days intervals amount
		edit.putInt(Preferences.DAYS_INTERVALS.toString(), Integer.valueOf(
				days_intervals_amount.getText().toString()));
		
		//then putting update time
		edit.putLong(Preferences.DAY_TICK.toString(), Long.valueOf(
				update_time.getText().toString()));
		
		edit.putBoolean(Preferences.MINUTE_NOTIFICATION.toString(), 
				minute_interval_notification.isChecked());
		
		edit.putFloat(Preferences.TARGET_SUCCESS_RATE.toString(),
				Float.parseFloat(target_success_rate.getText().toString()));
		
		edit.putBoolean(Preferences.ORIENTATION_LOCK.toString(),
				lock_orientations.isChecked());
		
		edit.commit();
		
		//then finally loading window contents again
		loadWindowContent();
	}
	
	private void findFields () {
		//amount of interval we do in a day field
		days_intervals_amount = (EditText) ((IntervalNotification)context)
				.findViewById(R.id.amount_of_days_intervals);
		
		//when day changes and new intervals become active
		update_time = (EditText) ((IntervalNotification)context)
				.findViewById(R.id.update_time);
		
		//target success rate
		target_success_rate = (EditText) ((IntervalNotification)context)
				.findViewById(R.id.target_success_rate);
		
		//save button
		save_button = (Button) ((IntervalNotification)context)
				.findViewById(R.id.save_settings_button);
		
		//is notifications about minute intervals on
		minute_interval_notification = (CheckBox) ((IntervalNotification)context)
				.findViewById(R.id.minute_interval_notification);
		
		//is orientation lockin on
		lock_orientations = (CheckBox) ((IntervalNotification)context)
				.findViewById(R.id.lock_orentations);
	}
}
