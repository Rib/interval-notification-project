package memtools.intervalnotification.ui;

import java.util.List;

import memtools.intervalnotification.IntervalNotification;
import memtools.intervalnotification.R;
import memtools.intervalnotification.Interface.InterfaceImplementation;
import memtools.intervalnotification.Libs.Category;
import memtools.intervalnotification.Libs.CategoryAdapter;
import memtools.intervalnotification.Libs.DayIntervalAdapter;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class ListAllCategoriesWindow {
	Context context;
	
	List<Category> categories;
	
	InterfaceImplementation iface;
	
	//fields
	AutoCompleteTextView search_field;
	ImageView search_button;
	ListView category_list_view;
	
	public ListAllCategoriesWindow (Context context) {
		this.context = context;
		
		iface = new InterfaceImplementation(this.context);
	}
	
	public void setVisible () {
		((IntervalNotification) context).setContentView(R.layout.categories);
	}
	
	public void loadWindowContent () {
		//getting all categories
		categories = iface.listAllCategories();
		
		loadNonDBContent();
	}

	public void setSavedInstanceStates(Bundle states) {
		findFields();
		String category = search_field.getText().toString();

	}
	
	private void loadNonDBContent () {
		//finding fields
		findFields();
		
		if (search_field.getAdapter() == null || 
				category_list_view.getAdapter() == null) {
			CategoryAdapter cat_adapter = 
					new CategoryAdapter(this.context, categories);
		
			search_field.setThreshold(1);
			search_field.setAdapter(cat_adapter);
			category_list_view.setAdapter(cat_adapter);	
		}
		
		InputMethodManager imm = (InputMethodManager)
				context.getSystemService(
			      Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(search_field.getWindowToken(), 0);
		
		//setting filter
		//cat_adapter.getFilter().filter(search_field.getText().toString());
		
		//finally assigning adapter
		
		//setting key listener to handle regexp changes
		search_field.setOnKeyListener(new OnKeyListener() {
			public boolean onKey(View arg0, int arg1, KeyEvent arg2) {
				((CategoryAdapter)category_list_view.getAdapter())
				.getFilter().filter(search_field.getText().toString());
				return false;
			}		
		});
			
		//setting done button action
		search_field.setOnEditorActionListener(new OnEditorActionListener(){

			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE || 
					actionId == EditorInfo.IME_ACTION_SEARCH) {
					((CategoryAdapter)category_list_view.getAdapter())
					.getFilter().filter(search_field.getText().toString());
					return true;
				}

				return false;
			}
			
		});
		
		search_button.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				((CategoryAdapter)category_list_view.getAdapter())
				.getFilter().filter(search_field.getText().toString());
				
				InputMethodManager imm = (InputMethodManager)
						context.getSystemService(
					      Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(search_field.getWindowToken(), 0);
			}
			
		});
		
		search_button.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					((ImageView)v).setImageResource(R.drawable.search_button_down);
				} else if (event.getAction() == MotionEvent.ACTION_UP ||
						event.getAction() == MotionEvent.ACTION_CANCEL ||
						event.getAction() == MotionEvent.ACTION_OUTSIDE) {
					((ImageView)v).setImageResource(R.drawable.search_button_up);
				}
				return false;
			}
			
		});
		
		//setting on item click listener for listview
		category_list_view.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				//first getting the category
				Category cat = new Category();
				cat = (Category)arg0.getItemAtPosition(arg2);
								
				//then going to the category
				((IntervalNotification)context).moveToCategory(cat);
				
			}
			
		});
	}
	
	private void findFields() {
		search_field = (AutoCompleteTextView)
				((IntervalNotification) context)
				.findViewById(R.id.category_category_search_field);
		
		search_button = (ImageView)
				((IntervalNotification) context)
				.findViewById(R.id.category_category_search_button);
		
		category_list_view = (ListView)
				((IntervalNotification) context).findViewById(R.id.category_category_list);
	}
	
}