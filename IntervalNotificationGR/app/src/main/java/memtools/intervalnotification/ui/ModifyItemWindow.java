package memtools.intervalnotification.ui;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import memtools.intervalnotification.Handlers.ProgramStateHandler;
import memtools.intervalnotification.IntervalNotification;
import memtools.intervalnotification.R;
import memtools.intervalnotification.Interface.InterfaceImplementation;
import memtools.intervalnotification.Libs.AddItemAdapter;
import memtools.intervalnotification.Libs.Category;
import memtools.intervalnotification.Libs.Data;
import memtools.intervalnotification.Libs.Datatype;
import memtools.intervalnotification.Libs.Interval;
import memtools.intervalnotification.Libs.Intervaltype;
import memtools.intervalnotification.Libs.MinuteInterval;
import memtools.intervalnotification.Libs.ThingToRemember;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ModifyItemWindow {
	
	Context context;
	
	ThingToRemember ttr;
	
	//list of data belonging to this ttr
	List<Data> datas;
	
	String category_text = "";
	boolean adding_interval = true;
	boolean is_minute_interval = false;
	String date = "";

	ListView items;
	ImageView save_button;
	ImageView delete_button;
	
	TextView ttr_field;
	
	//adapter, using same adapter as in additemwindow
	AddItemAdapter item_adapter;
	
	
	//interface
	InterfaceImplementation iface;
	
	public ModifyItemWindow(IntervalNotification context) {
		this.context = context;
		
		datas = new ArrayList<Data>();
		
		//making new instance of interface
		iface = new InterfaceImplementation(this.context);
	}
	
	public void setVisible (ThingToRemember ttr) {
		((IntervalNotification) context).setContentView(R.layout.modifyitem);
		
		//if for some reason we have null ttr we just don't do anything
		if (ttr == null) return;
		defaults(ttr);
	}
	
	public void loadWindowContent () {		
		//finding fields
		findFields();

		item_adapter = new AddItemAdapter(this.context,datas, 1, category_text, 
		adding_interval, is_minute_interval, date);
		
		//setting ttr for this adapter
		item_adapter.setTTR(ttr);
		
		//assigning adapter to listview
		items.setAdapter(item_adapter);
				
		//setting save button
		//saves current content
		save_button.setOnClickListener(new  OnClickListener () {

			public void onClick(View arg0) {
				updateItems();

				//deleting old thing to remember
				iface.deleteTTRDataContent(ttr);
				
				//adds items into ttr and gets most up to date information about ttr
				ttr = iface.addItem(datas, ttr);
								
				//if we have some different category we just try to create it
				Category temp_category = new Category();
				
				//just making sure we have some category and its not the same as before
				if (category_text.length() > 0) {
					temp_category = iface.addCategory(category_text);
				}
					
				//here we make sure we have the same text (should always be so)
				//and linking category to thing to remember
				if (category_text.length() > 0 && temp_category.getName().equals(category_text)) {
					//keeping the old category intact
					//joining category to ttr
					iface.setCategoryTTR((int)temp_category.getId(), 
							(int)ttr.getId());
				}
				
				//setting up message when this is called
				String cs = ttr.getInfob().getInfo();
				cs += " " + context.getString(R.string.was_modified);
				
				//setting up time this notification is displayed
				int duration = Toast.LENGTH_SHORT;
				
				Toast toast = Toast.makeText(context, cs, duration);
				toast.show();
				
				//then checking if we're adding interval
				if (((AddItemAdapter)items.getAdapter()).addingInterval()) {
					ttr = iface.getTTR(ttr.getId());
					saveAsInterval(ttr);
					//if we have a chance of change in todays intervals we update
					//content of todays intervals
					((IntervalNotification)context)
					.getTodaysIntervals().updateContent();
				}
				
				//loading defaults
				defaults(ttr);
				
				//loading window contents
				loadWindowContent();
			}
		});
		
		save_button.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					((ImageView)v).setImageResource(R.drawable.save_button_down);
				} else if (event.getAction() == MotionEvent.ACTION_UP ||
						event.getAction() == MotionEvent.ACTION_CANCEL ||
						event.getAction() == MotionEvent.ACTION_OUTSIDE) {
					((ImageView)v).setImageResource(R.drawable.save_button_up);
				}
				return false;
			}
			
		});
			
		//setting delete button
		//deletes current item
		delete_button.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				//setting up message when this is called
				final String cs = ttr.getInfob().getInfo()+ 
						" " + context.getString(R.string.was_deleted);
				
				AlertDialog.Builder builder = new AlertDialog.Builder(context);
				builder.setMessage(context.getString(R.string.delete) + " " + 
						ttr.getInfob().getInfo())
						.setCancelable(false)
						.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				           public void onClick(DialogInterface dialog, int id) {
								//setting up time this notification is displayed
								int duration = Toast.LENGTH_SHORT;
												
								//deleting thing to remember
								iface.deleteTTR(ttr);
								
								//then moving to item list
								((IntervalNotification)context).moveToAllItems();
								
								Toast toast = Toast.makeText(context, cs, duration);
								toast.show();
				                dialog.cancel();
				           }
				       })
				       .setNegativeButton("No", new DialogInterface.OnClickListener() {
				           public void onClick(DialogInterface dialog, int id) {
				                dialog.cancel();
				           }
				       });
					
				AlertDialog alert = builder.create();
				
				alert.show();
			}			
		});
		
		delete_button.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					((ImageView)v).setImageResource(R.drawable.delete_item_button_down);
				} else if (event.getAction() == MotionEvent.ACTION_UP ||
						event.getAction() == MotionEvent.ACTION_CANCEL ||
						event.getAction() == MotionEvent.ACTION_OUTSIDE) {
					((ImageView)v).setImageResource(R.drawable.delete_item_button_up);
				}
				return false;
			}
			
		});
	}
		
	//add operations
	public void itemAdd () {
		updateItems();
		
		//adding just adds empty item to the list
		Data temp_data = new Data();
		temp_data.setContent("");
		temp_data.setType(Datatype.TEXT);
		datas.add(temp_data);
	}
	
	private void saveAsInterval (ThingToRemember ttr) {
		//first finding fields (probably not necessary but doesn't cost much)
		findFields();
		//getting date
		Date temp_date;
		Intervaltype  temp_type;
		int temp_length = 1;
		
		//checking if we have minute interval or not
		if (!item_adapter.isMinuteInterval()) {
			DateFormat df = DateFormat.getDateInstance();
			//interpreting date
			try {
				temp_date = df.parse(item_adapter.getDate());
			} catch (ParseException e) {
				Calendar cal = Calendar.getInstance();
				temp_date = cal.getTime();
			}
			//type is normal day interval
			temp_type = Intervaltype.DAY;
			//setting length to 1
			temp_length = 1;
		} else {
			//getting today
			Calendar calendar = Calendar.getInstance();
			//getting current date
			temp_date = calendar.getTime();
			//making type minute interval
			temp_type = Intervaltype.MINUTE;
			//we have length too
			try {
				temp_length = Integer.parseInt(item_adapter.getDate());
			} catch (NumberFormatException e) {
				temp_length = 1;
			}
		}
		
		//making sure we have correct value in selected thing to remember
		//interval id (hence it should be -1)
		ttr.setIntervalId(-1);
		
		//finally calling interface to create our interval
		Interval temp_interval = iface.addInterval(temp_length, 
				temp_date, ttr, temp_type);
		
		//if we have minute interval we make new handler to make sure 
		//we get notifications on time
		if (temp_interval.getType().equals(Intervaltype.MINUTE)) {
			//we put handler up for this minute interval
			//TODO check that ttr is ok
			MinuteInterval mi = new MinuteInterval(this.context, ttr);
			/*AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			mi.makePendingIntent();
			am.set(AlarmManager.ELAPSED_REALTIME, 
					SystemClock.elapsedRealtime()+mi.getNextDelay(), mi.pendingIntent());*/
			//setting handler
			Handler handler = new Handler();
			//setting delay
			handler.postDelayed(mi, mi.getNextDelay());
			((IntervalNotification)context)
			.addMinuteIntervalHandler(handler);
		}
		
		//updating interval activities hence we just added interval
		iface.updateIntervalActivities();
	}
	
	private void findFields() {		
		//items
		items = (ListView) ((IntervalNotification)context).findViewById(
				R.id.modify_item_listview);
		
		//save button
		save_button = (ImageView) ((IntervalNotification)context).findViewById(
				R.id.modify_item_save_button);
		
		//delete button
		delete_button = (ImageView) ((IntervalNotification)context).findViewById(
				R.id.modify_item_delete_button);
		
		ttr_field = (TextView)((IntervalNotification)context).findViewById(
				R.id.modify_item_ttr_id);	
				
	}
	
	public void updateItems() {
		//going through all and updating items
		
		datas = item_adapter.getItems();
	}
	
	public void setStates (String category_text, boolean adding_interval, 
			boolean is_minute_interval, String date) {
		this.category_text = category_text;
		this.adding_interval = adding_interval;
		this.is_minute_interval = is_minute_interval;
		this.date = date;
	}
	
	private void defaults(ThingToRemember ttr) {
		findFields();
		
		//getting data that is in the database
		datas = iface.getTTRData(ttr);
		
		//updating string information of the ttr
		iface.updateTTRInfo(ttr);
				
		this.ttr = ttr;
		
		ttr_field.setText(ttr.getId()+"");
		
		category_text = ttr.getInfob().getCategory();
	}

	//loads items into datas to show them on screen later
	public void loadItems(List<String> content, List<String> type) {
		if (content.size() == type.size()) {
			int count = content.size();
			datas.clear();
			for (int i = 0; i < count; ++i) {
				Data data = new Data();
				data.setContent(content.get(i));
				data.setType(Datatype.valueOf(type.get(i)));
				datas.add(data);
			}
		}
	}
	
	//sets saved instance states
	public void setSavedInstanceStates (Bundle states) {
		findFields();
		//first getting ttr id (which is in a hidden field)
		long ttr_id = Integer.parseInt(ttr_field.getText().toString());
		
		//category
		String category = ((AddItemAdapter)items.getAdapter()).getCategory();
		
		//creating the list
		ArrayList<String> items_content = new ArrayList<String>();
		ArrayList<String> items_type = new ArrayList<String>();
		for(int i = 0; i < items.getAdapter().getCount()-2; ++i) {
			items_content.add(((Data)items.getItemAtPosition(i)).getContent());
			items_type.add(((Data)items.getItemAtPosition(i)).getType().toString());
		}
		
		//then put them into states
		ProgramStateHandler psh = new ProgramStateHandler();
		psh.writeModifyItemWindow(states, ttr_id, category, items_content, items_type);
	}
	
	//loads instance states
	public boolean loadSavedInstanceStates (ProgramStateHandler handler) {
		if (handler.isHas_mitw()) {
			ThingToRemember ttr = iface.getTTR(handler.getMitw_ttr_id());
			setVisible(ttr);

			//first finding fields
			findFields();

			ttr_field.setText(handler.getMitw_ttr_id()+"");

			category_text = handler.getMitw_category();

			loadItems(handler.getMitw_item_list(), handler.getMitw_item_type_list());

			loadWindowContent();

			return true;
		} else return false;

	}
}
