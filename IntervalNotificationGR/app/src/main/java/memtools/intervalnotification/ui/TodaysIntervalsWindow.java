package memtools.intervalnotification.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import memtools.intervalnotification.IntervalNotification;
import memtools.intervalnotification.R;
import memtools.intervalnotification.Interface.InterfaceImplementation;
import memtools.intervalnotification.Libs.DayIntervalAdapter;
import memtools.intervalnotification.Libs.Interval;
import memtools.intervalnotification.Libs.Preferences;
import memtools.intervalnotification.Libs.ThingToRemember;
import memtools.intervalnotification.Handlers.ProgramStateHandler;

import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class TodaysIntervalsWindow {
	private static final long refresh_delay = 30000;
	
	Context context;
	
	//value if adapter needs to be updated
	boolean update_adapter = false;
	
	//fields
	//listview declaration
	ListView interval_listview;
	
	//search field we want to show
	EditText tv;
	
	//intervals list
	List<Interval> intervals = new ArrayList<Interval>();
	
	ImageView search_button;
	
	LinearLayout todays_test_button;
	
	InterfaceImplementation iface;
	
	//timer for updating contents of this window
	Timer refresh_rate;
		
	//constructor
	public TodaysIntervalsWindow(Context context) {
		this.context = context;
		iface = new InterfaceImplementation(this.context);
		
		updateContent();
	}
	
	//sets todays intervals window visible
	public void setVisible() {	
		if (((IntervalNotification) context).getWindow().getDecorView()
				.findViewById(R.id.todays_intervals_list_layout) == null)
		((IntervalNotification) context).setContentView(R.layout.window_todays_intervals_layout);
				
		//loading window contents
		loadWindowContent();
		//(new Refresh(this)).execute((Void)null);
	}

	class Refresh extends AsyncTask<Void, String, Void> {		
		
		TodaysIntervalsWindow todaysIntervalsWindow;
		
		boolean running;
				
		public Refresh(TodaysIntervalsWindow todaysIntervalsWindow) {
			super();
			running = true;
			this.todaysIntervalsWindow = todaysIntervalsWindow;
		}

		@Override
		protected Void doInBackground(Void... params) {
				while (!((IntervalNotification)context)
						.hasWindowFocus()) {
					try {
						Thread.sleep(100);
						((IntervalNotification)context)
						.setCurrentView(R.layout.window_todays_intervals_layout);
					} catch (InterruptedException e) {
						//catch block
						e.printStackTrace();
						//if our thread has been interrupted to some reason we just quit it
						stop();
						return null;
					}
				}
				
				if (((IntervalNotification)context).getCurrentView() !=
							R.layout.window_todays_intervals_layout) {
					stop();
					return null;
				}
					
				this.publishProgress((new StringBuilder()).toString());
			
			return null;
		}

		//this update UI content (as onprogressupdate documentation states...)
		@Override
		protected void onProgressUpdate(String... values) {
			if( ! running ) {
				return; 
			}
			//TODO
			/*long nanos = System.currentTimeMillis();
			iface.updateIntervalActivities();
			nanos = System.currentTimeMillis()-nanos;
			Log.v("updateIntervalActivities", ""+nanos);*/
			todaysIntervalsWindow.loadWindowContent();
		}

		//this stops the loop that is done in the background
	    public void stop() {
	        running = false;
	    }
	}
	
	private void loadWindowContent () {		
		//finding fields
		findFields();		
		//this is quite slow, but hence there isn't anything better atm and this
		//seems to work perfectly we accept this :)
		//(slowness comes from extra and probably unnecessary database access)
		//iface.updateIntervalActivities();
		//nanos = System.nanoTime()-nanos;
		//Log.v("loadWindowContent updateIntervalActivities time", String.valueOf(nanos));
		//nanos = System.nanoTime();
		
		//this is here as long as we know what exactly we wan't with todays intervals
		/*updateContent();
		nanos = System.currentTimeMillis()-nanos;
		Log.v("loadWindowContent updateContent time", String.valueOf(nanos));
		nanos = System.currentTimeMillis();*/
		
		//intervals_inactive = iface.nextInactiveIntervals(0, 10);
				
		loadNondbContent();
	}
	
	//updates todays intervals content (doesn't affect anything else)
	public void updateContent () {
		//first clearing all notifications because we're in correct window now
		//notification manager
		if (((IntervalNotification)context).hasWindowFocus()) {
			NotificationManager not_mana = (NotificationManager)
				context.getSystemService(Context.NOTIFICATION_SERVICE);
		
			not_mana.cancelAll();
			
			//also setting the notification to null
			((IntervalNotification)context).setNotification(null);
		}
		
		//first creating and loading settings value for this one
		SharedPreferences prefs = context.getSharedPreferences(
				IntervalNotification.SETTINGS_FILE, 0);
		
		//TODO combine these two active intervals searches so we can get it way more
		//efficiently
		
		//getting rest of the content (getting inactive intervals that have highest priority)
		List<Interval> intervals_inactive = iface.nextInactiveIntervals(0,
				prefs.getInt(Preferences.DAYS_INTERVALS.toString(), 100));
		
		//first getting active intervals
		List<Interval> intervals_active = iface.nextActiveIntervals(0,
				prefs.getInt(Preferences.DAYS_INTERVALS.toString(), 100) -
						intervals_inactive.size());
				
		//then finally combining these two
		intervals.clear();
		intervals.addAll(intervals_active);
		intervals.addAll(intervals_inactive);
	}
	
	//moves to review window of this item
	public void moveToReview (int position) {
		//closing updating task
		//refresh_rate.cancel();
		//rf.cancel(true);
		
		ThingToRemember ttr = 
				((Interval)interval_listview.getItemAtPosition(position)).getTTR();
		
		if (ttr == null) {
			long interval_id = 
					((Interval)interval_listview.getItemAtPosition(position)).getId();
			
			Interval temp_interval = iface.getInterval(interval_id, false);
			
			ttr = temp_interval.getTTR();
		}
		
		//setting review window visible
		((IntervalNotification)context)
		.moveToReview(ttr, null);
	}
	
	public List<Interval> getIntervals() { return intervals; }
	public void setIntervals(List<Interval> intervals) { this.intervals = intervals; }
	
	//this sets all the necessary parameters to saved instance state
	public void setSavedInstanceStates (Bundle states) {
		//making sure we have visible window
		((IntervalNotification) context).setContentView(R.layout.window_todays_intervals_layout);
		//finding fields so we can access them as we want
		findFields();
		//storing category search
		String category = tv.getText().toString();
				
		//list where we store intervals marked active
		ArrayList<Integer> actives = new ArrayList<Integer>();
		for(int i = 0; interval_listview.getAdapter() != null && 
				i < (interval_listview.getAdapter().getCount()); ++i) {
			if (((Interval)interval_listview.getItemAtPosition(i)).isActive())
			{
				actives.add((int)interval_listview.getItemIdAtPosition(i));
			}
		}

		ProgramStateHandler psh = new ProgramStateHandler();
		psh.writeTodaysIntervals(states, category, actives);
	}
	
	//this loads all necessary stuff when on state is called
	public void loadSavedInstanceStates (ProgramStateHandler handler) {
		if (handler.isHas_ti()) {
			// dhem fields
			findFields();

			tv.setText(handler.getTi_search_category());

			ArrayList<Integer> actives = handler.getTi_active_intervals();

			int count = intervals.size();

			//TODO make faster sometime O(i*j) time is not really apealing
			for(int i = 0; i < count; ++i) {
				intervals.get(i).setActive(false);

				for (int j = 0; j < actives.size(); ++j) {
					if (intervals.get(i).getId() == actives.get(j)) {
						intervals.get(i).setActive(true);
					}
				}
			}
		}
	}
	
	private void loadNondbContent () {		
		Collections.sort(intervals);
		
		if (interval_listview == null) return;
				
		//setting adapter
		if (interval_listview.getAdapter() == null || update_adapter) {
			DayIntervalAdapter interval_adapter 
			= new DayIntervalAdapter(this.context, this, 
				intervals);
			
			interval_listview.setAdapter(interval_adapter);
		}
		
		InputMethodManager imm = (InputMethodManager)
				context.getSystemService(
			      Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(tv.getWindowToken(), 0);

		
		//interval_adapter.getFilter().filter(tv.getText().toString());

		//setting key listener to handle regexp changes
		tv.setOnKeyListener(new OnKeyListener() {
			public boolean onKey(View arg0, int arg1, KeyEvent arg2) {
				((DayIntervalAdapter)interval_listview.getAdapter())
				.getFilter().filter(tv.getText().toString());
				return false;
			}		
		});
		
		//setting done button action
		tv.setOnEditorActionListener(new OnEditorActionListener(){

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE || 
					actionId == EditorInfo.IME_ACTION_SEARCH) {
					((DayIntervalAdapter)interval_listview.getAdapter())
					.getFilter().filter(tv.getText().toString());
					
					InputMethodManager imm = (InputMethodManager)
							context.getSystemService(
						      Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(tv.getWindowToken(), 0);
					return true;
				}

				return false;
			}
			
		});
		
		search_button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				((DayIntervalAdapter)interval_listview.getAdapter())
				.getFilter().filter(tv.getText().toString());
				
				InputMethodManager imm = (InputMethodManager)
						context.getSystemService(
					      Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(tv.getWindowToken(), 0);
				//loadNondbContent();
			}
			
		});
		
		search_button.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					((ImageView)v).setImageResource(R.drawable.search_button_down);
				} else if (event.getAction() == MotionEvent.ACTION_UP ||
						event.getAction() == MotionEvent.ACTION_CANCEL ||
						event.getAction() == MotionEvent.ACTION_OUTSIDE) {
					((ImageView)v).setImageResource(R.drawable.search_button_up);
				}
				return false;
			}
			
		});
		
		todays_test_button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {	
				Interval temp_interval = nextInterval();
				if (temp_interval != null) {
					//Canceling background task when leaving this window
					//refresh_rate.cancel();
					//rf.cancel(true);
					if (temp_interval.getTTR() == null) 
						temp_interval = iface.getInterval(temp_interval.getId(), false);
					
					TodaysTestWindow ttw = new TodaysTestWindow(context, intervals);
					((IntervalNotification) context).moveToReview(temp_interval.getTTR(),ttw);
				}	
			}
			
		});
		
		todays_test_button.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					v
					.setBackgroundResource(R.drawable.todays_test_button_background_image_down);
				} else if (event.getAction() == MotionEvent.ACTION_UP ||
						event.getAction() == MotionEvent.ACTION_CANCEL ||
						event.getAction() == MotionEvent.ACTION_OUTSIDE) {
					v
					.setBackgroundResource(R.drawable.todays_test_button_background_image_up);
				}
				return false;
			}
			
		});
	}
	
	private Interval nextInterval() {
		updateContent();
		if (intervals.size() > 0 && intervals.get(0).isActive()) {
			return intervals.get(0);
		} else return null;
	}
	
	private void findFields() {
		//listview
		interval_listview = (ListView) 
				((IntervalNotification) 
						context).findViewById(R.id.todays_intervals_list);
		
		//category search field
		tv = (EditText) 
				((IntervalNotification) 
						context).findViewById(R.id.todays_intervals_search);
		
		search_button = (ImageView)
				((IntervalNotification)
						context).findViewById(R.id.todays_intervals_search_button);
		
		todays_test_button = (LinearLayout)
				((IntervalNotification)
						context).findViewById(R.id.todays_test_button);
				
	}
	
	private class ActivityTimerUpdate extends TimerTask {
		final Handler handler = new Handler();
	
		@Override
		public void run() {
			handler.post(new Runnable() {
                public void run() {

                    try {
                    	(new ActivityUpdate(context)).execute(new Object());
                    	
                    } catch (Exception e) {
                    	
                    }

                }
            });	
		}
	}
	
	private class ActivityUpdate extends AsyncTask {
		
		InterfaceImplementation iface;
        Context context;
		
		public ActivityUpdate (Context context) {
			this.context = context;
            iface = new InterfaceImplementation(context);
		}

		@Override
		protected Object doInBackground(Object... params) {
            update_adapter = iface.updateIntervalActivities();
			return null;
		}

		@Override
		public void onProgressUpdate (Object...values) {
			update_adapter = iface.updateIntervalActivities();
		}
		
	}
}
