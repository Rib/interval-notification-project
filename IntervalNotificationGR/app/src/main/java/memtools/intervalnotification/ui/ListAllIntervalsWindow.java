package memtools.intervalnotification.ui;

import java.util.List;

import memtools.intervalnotification.Handlers.ProgramStateHandler;
import memtools.intervalnotification.IntervalNotification;
import memtools.intervalnotification.R;
import memtools.intervalnotification.Interface.InterfaceImplementation;
import memtools.intervalnotification.Libs.Category;
import memtools.intervalnotification.Libs.CategoryAdapter;
import memtools.intervalnotification.Libs.Interval;
import memtools.intervalnotification.Libs.ListIntervalAdapter;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class ListAllIntervalsWindow {
	Context context;
	
	//fields
	//listview declaration
	ListView interval_listview;
	//auto completion field we want to show
	AutoCompleteTextView actv;
	
	//adapters
	//interval adapter
	ListIntervalAdapter interval_adapter;
	//category adapter
	ArrayAdapter<Category> category_adapter;
	
	//intervals list
	List<Interval> intervals;
	//temporary intervals list for regexp search
	List<Interval> temp_intervals;
	//categories list
	List<Category> categories;
	
	InterfaceImplementation iface;
	
	long time_in_load = 0;
	long time_in_nondbload = 0;
	
	//constructor
	public ListAllIntervalsWindow(IntervalNotification context) {
		this.context = context;
		
		iface = new InterfaceImplementation(this.context);
	}
	
	//setting this window visible
	public void setVisible () {
		((IntervalNotification) context).setContentView(R.layout.intervals);
	}
	
	//loading contents for this window
	public void loadWindowContent() {
		//first finding fields
		findFields();
		
		//getting all categories
		categories = iface.listAllCategories();
		
		//listing all intervals
		intervals = iface.listAllIntervals();
		
		loadNonDBContent();
	}

	public void setSavedInstanceStates(Bundle states) {
		findFields();

		String category = actv.getText().toString();
		ProgramStateHandler psh = new ProgramStateHandler();
		psh.writeListAllIntervalsWindow(states, category);
	}
	
	private void loadNonDBContent () {
		//setting categories adapter
		category_adapter = new CategoryAdapter(this.context, categories);
		
		//setting auto completion threshold to 1
		actv.setThreshold(1);
		//setting adapter
		actv.setAdapter(category_adapter);
				
		//setting interval adapter
		interval_adapter = new ListIntervalAdapter(this.context, intervals);
		interval_adapter.getFilter().filter(actv.getText().toString());
		//setting the adapter
		interval_listview.setAdapter(interval_adapter);
				
		//adding on item click listener
		actv.setOnItemClickListener(new OnItemClickListener() {  
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				//when item is clicked we load window again
				findFields();
				loadNonDBContent();
			}
		});
		
		actv.setOnKeyListener(new OnKeyListener() {

			public boolean onKey(View arg0, int arg1, KeyEvent arg2) {
				//when some key is pressed we just load content again
				findFields();
				loadNonDBContent();
				return false;
			}		
		});
		
		//interval_listview.setClickable(true);
		
		//setting on click listener for listview
		//moves to interval modification window
		interval_listview.setOnItemClickListener(new OnItemClickListener () {

			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				//then moving to modify interval
				
				((IntervalNotification)
						context).moveToModifyInterval((Interval)arg0
						.getItemAtPosition(arg2));
			}
		});
		
		//setting done button action
		actv.setOnEditorActionListener(new OnEditorActionListener(){

			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE || 
					actionId == EditorInfo.IME_ACTION_SEARCH) {
					loadWindowContent();
					return true;
				}

				return false;
			}
			
		});
	}
		
	private void findFields () {
		//listview
		interval_listview = (ListView) 
				((IntervalNotification) 
						context).findViewById(R.id.interval_list_view);
		
		//category search field
		actv = (AutoCompleteTextView) 
				((IntervalNotification) 
						context).findViewById(R.id.interval_restriction_field);
	}
}
