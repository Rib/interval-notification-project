package memtools.intervalnotification.ui;

import java.util.List;

import memtools.intervalnotification.IntervalNotification;
import memtools.intervalnotification.R;
import memtools.intervalnotification.Interface.InterfaceImplementation;
import memtools.intervalnotification.Libs.Interval;
import memtools.intervalnotification.Libs.Intervaltype;
import memtools.intervalnotification.Libs.Preferences;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.view.View;
import android.widget.Button;

public class MainWindow {
	//class variables
	Context context;
	
	//list of active intervals
	List<Interval> active_intervals;
	
	//buttons
	Button day_interval_button;
	Button add_interval_button;
	Button add_item_to_view_button;
	
	//interface
	InterfaceImplementation iface;
	
	//constructor
	public MainWindow(Context context) {
		this.context = context;
		
		iface = new InterfaceImplementation(this.context);
	}

	//sets main window visible in the screen
	public void setVisible() {
		((IntervalNotification) context).setContentView(R.layout.mainwindow);
	}
	
	public void loadWindowContent() {
		findFields();
		
		//getting settings
		SharedPreferences prefs = context.getSharedPreferences(
				IntervalNotification.SETTINGS_FILE, 0);
		
		active_intervals = iface.nextActiveIntervals(0, 
				prefs.getInt(Preferences.DAYS_INTERVALS.toString(), 100));
		
		//setting initial black color to text
		day_interval_button.setTextColor(Color.BLACK);
		
		//going through active intervals and see if there is minute intervals active
		for (int i = 0; i < active_intervals.size(); ++i) {
			if (active_intervals.get(i).getType() == Intervaltype.MINUTE) {
				//setting the text red if there is atleast one minute interval
				day_interval_button.setTextColor(Color.RED);
			}
		}
		
		String temp_day_interval_text = day_interval_button.getText().toString();
		temp_day_interval_text += " (" + active_intervals.size() + ")";
		
		day_interval_button.setText(temp_day_interval_text);
	}

	public void moveToTodaysIntervals (View v) {
		setVisible();
	}
	
	//finds all the necessary fields in the window
	private void findFields() {
		day_interval_button = (Button) 
				((IntervalNotification)context).findViewById(R.id.day_interval_button);
		
		add_interval_button = (Button)
				((IntervalNotification)context).findViewById(R.id.add_interval_button);
		
		add_item_to_view_button = (Button)
				((IntervalNotification)context).findViewById(R.id.add_item_toview_button);
	}
	
}
