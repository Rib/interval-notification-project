package memtools.intervalnotification.ui;

import java.text.DateFormat;
import java.util.Calendar;

import memtools.intervalnotification.IntervalNotification;
import android.app.DatePickerDialog;
import android.content.Context;
import android.widget.DatePicker;
import android.widget.EditText;

public class DateDialog {
	Context context;
	
	int return_id;
	
	//constructor
	public DateDialog (IntervalNotification context) {
		this.context = context;
	}
	
	//shows the dialog and return_id is the edit fields id
	public void setVisible(int return_id) {
		((IntervalNotification) this.context).showDialog(0);
		this.return_id = return_id;
	}
	
	//gets date picker listener
	public DatePickerDialog.OnDateSetListener getDatePicker() { return dpd; }
	
	//setting date picker listener
	private DatePickerDialog.OnDateSetListener dpd = new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			Calendar now = Calendar.getInstance();
			now.set(Calendar.YEAR, year);
			now.set(Calendar.MONTH, monthOfYear);
			now.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			

			
	        //finding correct return field
	        EditText tempet = (EditText) 
	        		((IntervalNotification) 
	        				context).findViewById(return_id);
	        
			DateFormat df = DateFormat.getDateInstance();
			
			tempet.setText(df.format(now.getTime()));
	        
	        /*//setting date with string builder
	        tempet.setText(new StringBuilder()
	        // Month is 0 based so add 1
	        .append(monthOfYear+1).append("/")
	        .append(dayOfMonth).append("/")
	        .append(year).append(""));*/
		}
    };

}
