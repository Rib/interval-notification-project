package memtools.intervalnotification.Handlers;

import java.util.ArrayList;

import android.os.Bundle;

import memtools.intervalnotification.R;

/**
 * Created by tasogare on 8.8.2015.
 */
public class ProgramStateHandler {
    // static constants of the states
    public static final String SETTINGS_FILE = "config";

    public static final String CURRENT_VIEW = "CURRENT_VIEW";
    public static final String VIEWS = "VIEWS";

    public static final String SUCESS_FACTOR = "success_factor";
    public static final String FAILURE_FACTOR = "failure_factor";
    public static final String GOAL_SUCCESS_RATE = "goal_success_rate";

    // add interval window constants
    public static final String aintw_category_static = "CATEGORY";
    public static final String aintw_item_spinner_static = "ITEM_SPINNER_VALUE";
    public static final String aintw_choose_all_categories_static = "SELECT_ALL_CATEGORIES";
    public static final String aintw_is_minute_interval_static = "IS_MINUTE_INTERVAL";
    public static final String aintw_interval_date_static = "INTERVAL_STARTING_DATE";

    // add item window constants
    public static final String aiw_category_static = "CATEGORY";
    public static final String aiw_item_list_static = "ITEM_LIST";
    public static final String aiw_item_typelist_static = "ITEM_TYPELIST";
    public static final String aiw_add_as_interval_static = "ADD_AS_INTERVAL";
    public static final String aiw_is_minute_interval_static = "IS_MINUTE_INTERVAL";
    public static final String aiw_date_static = "DATE";

    // item view window constants
    public static final String ivw_ttr_id_static = "INTERVAL_ID";
    public static final String ivw_data_index_static = "DATA_INDEX";

    // list all categories constants
    public static final String lacw_search_category_static = "CATEGORY";

    // list all intervals constants
    public static final String laic_search_category_static = "CATEGORY";

    // list all items constants
    public static final String laitw_search_category_static = "CATEGORY";
    public static final String laitw_non_intervals_static = "LIST_NON_INTERVALS";

    // modify category window constants
    public static final String mcw_category_static = "CATEGORY";
    public static final String mcw_category_id_static = "CATEGORY_ID";

    // modify interval window constants
    public static final String miw_interval_id_static = "INTERVAL_ID";
    public static final String miw_date_static = "INTERVAL_DATE";

    // modify item window constants
    public static final String mitw_category_static = "CATEGORY";
    public static final String mitw_ttr_id_static = "TTR_ID";
    public static final String mitw_item_list_static = "ITEM_LIST";
    public static final String mitw_item_type_list_static = "ITEM_TYPE_LIST";

    // todays intervals window constants
    public static final String ti_search_category_static = "CATEGORY";
    public static final String ti_active_intervals_static = "ACTIVE_INTERVALS";

    // from notification constants
    public static final String nc_ttr_id_static = "TTR_ID";
    public static final String nc_data_index_static = "DATA_INDEX";
    public static final String nc_from_notification_static = "FROM_NOTIFICATION";

    // current windows id
    int current_window_id = -1;
    boolean has_aitw = false;
    boolean has_ivw = false;
    boolean has_ti = false;
    boolean has_aintw = false;
    boolean has_laic = false;
    boolean has_laitw = false;
    boolean has_mitw = false;
    boolean has_miw = false;
    boolean has_lacw = false;
    boolean has_mcw = false;

    // add interval window variables
    String aintw_category = "";
    boolean aintw_is_all_categories = false;
    int aintw_spinner_index = -1;
    boolean aintw_is_minute_interval = false;
    String aintw_starting_date = "";

    // add item variables
    String aiw_category = "";
    boolean aiw_save_as_interval = true;
    boolean aiw_is_minute_interval = false;
    String aiw_time_text = "";
    ArrayList<String> aiw_item_list = new ArrayList<String>();
    ArrayList<String> aiw_item_type_list = new ArrayList<String>();

    // item view window variables
    int ivw_ttr = -1;
    int ivw_data_index = 1;

    // list all categories variables
    String lacw_search_category = "";

    // list all intervals variables
    String laic_search_category = "";

    // list all items variables
    String laitw_search_category = "";
    boolean laitw_list_only_non_intervals = false;

    // modify category variables
    String mcw_category = "";
    long mcw_category_id = -1;

    // modify interval variables
    long miw_interval_id = -1;
    String miw_date = "";

    // modify item variables
    String mitw_category = "";
    long mitw_ttr_id = -1;
    ArrayList<String> mitw_item_list = new ArrayList<String>();
    ArrayList<String> mitw_item_type_list = new ArrayList<String>();

    // todays intervals variables
    String ti_search_category = "";
    ArrayList<Integer> ti_active_intervals = new ArrayList<Integer>();

    // from notification variables
    long nc_ttr_id = -1;
    int nc_data_index = 1;
    boolean nc_from_notification = false;

    public ProgramStateHandler () {

    }

    private boolean readAddIntervalWindow(final Bundle states) {
        if (states.containsKey(CURRENT_VIEW) && states.getInt(CURRENT_VIEW) == R.layout.addinterval) {
            if (states.containsKey(aintw_category_static)) {
                aintw_category = states.getString(aintw_category_static);
            } else return false;

            if (states.containsKey(aintw_item_spinner_static)) {
                aintw_spinner_index = states.getInt(aintw_item_spinner_static);
            } else return false;

            if (states.containsKey(aintw_choose_all_categories_static)) {
                aintw_is_all_categories = states.getBoolean(aintw_choose_all_categories_static);
            } else return false;

            if (states.containsKey(aintw_is_minute_interval_static)) {
                aintw_is_minute_interval = states.getBoolean(aintw_is_minute_interval_static);
            } else return false;

            if (states.containsKey(aintw_interval_date_static)) {
                aintw_starting_date = states.getString(aintw_interval_date_static);
            } else return false;

            return true;
        } else return false;
    }

    private boolean readAddItemWindow(final Bundle states) {
        if (states.containsKey(CURRENT_VIEW)  && states.getInt(CURRENT_VIEW) == R.layout.additem) {
            if (states.containsKey(aiw_category_static)) {
                aiw_category = states.getString(aiw_category_static);
            } else return false;

            if (states.containsKey(aiw_add_as_interval_static)) {
                aiw_save_as_interval = states.getBoolean(aiw_add_as_interval_static);
            } else return false;

            if (states.containsKey(aiw_is_minute_interval_static)) {
                aiw_is_minute_interval = states.getBoolean(aiw_is_minute_interval_static);
            } else return false;

            if (states.containsKey(aiw_date_static)) {
                aiw_time_text = states.getString(aiw_date_static);
            } else return false;

            if (states.containsKey(aiw_item_list_static)) {
                aiw_item_list = states.getStringArrayList(aiw_item_list_static);
            } else return false;

            if (states.containsKey(aiw_item_typelist_static)) {
                aiw_item_type_list = states.getStringArrayList(aiw_item_typelist_static);
            } else return false;

            return true;
        } else return false;
    }

    private boolean readItemViewWindow(final Bundle states) {
        if (states.containsKey(CURRENT_VIEW) && states.getInt(CURRENT_VIEW) == R.layout.thingtoremember) {
            if (states.containsKey(ivw_ttr_id_static)) {
                ivw_ttr = states.getInt(ivw_ttr_id_static);
            } else  return false;

            if (states.containsKey(ivw_data_index_static)) {
                ivw_data_index = states.getInt(ivw_data_index_static);
            } else return false;

            return true;
        } else return false;
    }

    private boolean readListAllCategoriesWindow(final Bundle states) {
        if (states.containsKey((CURRENT_VIEW)) && states.getInt(CURRENT_VIEW) == R.layout.categories) {
            if (states.containsKey(lacw_search_category_static)) {
                lacw_search_category = states.getString(lacw_search_category_static);
            } else return false;

            return true;
        } else return false;
    }

    private boolean readListAllIntervalsWindow(final Bundle states) {
        if (states.containsKey(CURRENT_VIEW) && states.getInt(CURRENT_VIEW) == R.layout.intervals) {
            if (states.containsKey(laic_search_category_static)) {
                laic_search_category = states.getString(laic_search_category_static);
            } else return false;

            return true;
        } else return false;
    }

    private boolean readListAllItemsWindow(final Bundle states) {
        if (states.containsKey(CURRENT_VIEW) && states.getInt(CURRENT_VIEW) == R.layout.intervals) {
            if (states.containsKey(laitw_search_category_static)) {
                laitw_search_category = states.getString(laitw_search_category);
            } else return false;

            if (states.containsKey(laitw_non_intervals_static)) {
                laitw_list_only_non_intervals = states.getBoolean(laitw_non_intervals_static);
            } else return false;

            return true;
        } else return false;
    }

    private boolean readModifyCategoryWindow(final Bundle states) {
        if (states.containsKey(CURRENT_VIEW) && states.getInt(CURRENT_VIEW) == R.layout.modify_category) {
            if (states.containsKey(mcw_category_static)) {
                mcw_category = states.getString(mcw_category_static);
            } else return false;

            if (states.containsKey(mcw_category_id_static)) {
                mcw_category_id = states.getLong(mcw_category_id_static);
            } else return false;

            return true;
        } else return false;
    }

    private boolean readModifyIntervalWindow(final Bundle states) {
        if (states.containsKey(CURRENT_VIEW) && states.getInt(CURRENT_VIEW) == R.layout.modify_interval) {
            if (states.containsKey(miw_interval_id_static)) {
                miw_interval_id = states.getLong(miw_interval_id_static);
            } else return false;

            if(states.containsKey(miw_date_static)) {
                miw_date = states.getString(miw_date_static);
            } else return false;

            return true;
        } else return false;
    }

    private boolean readModifyItemWindow (final Bundle states) {
        if (states.containsKey(CURRENT_VIEW) && states.getInt(CURRENT_VIEW) == R.layout.modifyitem) {
            if (states.containsKey(mitw_category_static)) {
                mitw_category = states.getString(mitw_category_static);
            } else return false;

            if (states.containsKey(mitw_ttr_id_static)) {
                mitw_ttr_id = states.getLong(mitw_ttr_id_static);
            } else return false;

            if (states.containsKey(mitw_item_list_static)) {
                mitw_item_list = states.getStringArrayList(mitw_item_list_static);
            } else return false;

            if (states.containsKey((mitw_item_type_list_static))) {
                mitw_item_type_list = states.getStringArrayList(mitw_item_type_list_static);
            } else return false;

            return true;
        } else  return false;
    }

    private boolean readTodaysIntervals(final Bundle states) {
        if (states.containsKey(CURRENT_VIEW) && states.getInt(CURRENT_VIEW) == R.layout.start_screen_base_layout) {
            if (states.containsKey(ti_search_category_static)) {
                ti_search_category = states.getString(ti_search_category_static);
            } else return false;

            if (states.containsKey(ti_active_intervals_static)) {
                ti_active_intervals = states.getIntegerArrayList(ti_active_intervals_static);
            } else return false;

            return true;
        } else return false;
    }

    private boolean readFromNotification(final Bundle states) {
        if (states.containsKey(CURRENT_VIEW) && states.getInt(CURRENT_VIEW) == R.layout.thingtoremember) {
            if (states.containsKey(nc_ttr_id_static)) {
                nc_ttr_id = states.getLong(nc_ttr_id_static);
            } else return false;

            if (states.containsKey(nc_data_index_static)) {
                nc_data_index = states.getInt(nc_data_index_static);
            } else return false;

            if (states.containsKey(nc_from_notification_static)) {
                nc_from_notification = states.getBoolean(nc_from_notification_static);
            } else return false;

            return nc_from_notification;
        } else return false;
    }

    public boolean readSavedBundleStates (final Bundle states) {
        if (states == null) return false;

        if (states.containsKey(CURRENT_VIEW)) current_window_id = states.getInt(CURRENT_VIEW);
        else return false;

        if (readAddIntervalWindow(states)) {
            has_aintw = true;
            return true;
        }

        if (readAddItemWindow(states)) {
            has_aitw = true;
            return true;
        }

        if (readItemViewWindow(states)) {
            has_ivw = true;
            return true;
        }

        if (readListAllCategoriesWindow(states)) {
            has_lacw = true;
            return true;
        }

        if (readListAllIntervalsWindow(states)) {
            has_laic = true;
            return true;
        }

        if (readListAllItemsWindow(states)) {
            has_laitw = true;
            return true;
        }

        if (readModifyCategoryWindow(states)) {
            has_mcw = true;
            return true;
        }

        if (readModifyIntervalWindow(states)) {
            has_miw = true;
            return true;
        }

        if (readModifyItemWindow(states)) {
            has_mitw = true;
            return true;
        }

        if (readTodaysIntervals(states)) {
            has_ti = true;
            return true;
        }

        return readFromNotification(states);

    }

    public void writeTodaysIntervals(Bundle states, String category, ArrayList<Integer> active_intervals) {
        states.putString(ti_search_category_static, category);
        states.putIntegerArrayList(ti_active_intervals_static, active_intervals);
    }

    public void writeListAllItemsWindow(Bundle states, String category, boolean non_intervals) {
        states.putString(laitw_search_category_static, category);
        states.putBoolean(laitw_non_intervals_static, non_intervals);
    }

    public void writeModifyItemWindow(Bundle states, long ttr_id, String category, ArrayList<String> items, ArrayList<String> item_types) {
        states.putLong(mitw_ttr_id_static, ttr_id);
        states.putString(mitw_category_static, category);
        states.putStringArrayList(mitw_item_list_static, items);
        states.putStringArrayList(mitw_item_type_list_static, item_types);
    }

    public void writeAddIntervalWindow(Bundle states, String category, boolean all_categories, boolean is_minute_interval,
                                       String date, int spinner_index) {
        states.putString(aintw_category_static, category);
        states.putBoolean(aintw_choose_all_categories_static, all_categories);
        states.putBoolean(aintw_is_minute_interval_static, is_minute_interval);
        states.putString(aintw_interval_date_static, date);
        states.putInt(aintw_item_spinner_static, spinner_index);
    }

    public void writeListAllIntervalsWindow(Bundle states, String category) {
        states.putString(laic_search_category_static, category);
    }

    public void writeModifyIntervalWindow(Bundle states, long interval_id, String date) {
        states.putLong(miw_interval_id_static, interval_id);
        states.putString(miw_date_static, date);
    }

    public void writeListAllCategoriesWindow(Bundle states, String category) {
        states.putString(lacw_search_category_static, category);
    }

    public void writeModifyCategoryWindow(Bundle states, long category_id, String category) {
        states.putString(mcw_category_static,category);
        states.putLong(mcw_category_id_static, category_id);
    }

    public int getCurrent_window_id() {
        return current_window_id;
    }

    public boolean isHas_aitw() {
        return has_aitw;
    }

    public boolean isHas_ivw() {
        return has_ivw;
    }

    public boolean isHas_ti() {
        return has_ti;
    }

    public boolean isHas_aintw() {
        return has_aintw;
    }

    public boolean isHas_laic() {
        return has_laic;
    }

    public boolean isHas_laitw() {
        return has_laitw;
    }

    public boolean isHas_mitw() {
        return has_mitw;
    }

    public boolean isHas_miw() {
        return has_miw;
    }

    public boolean isHas_lacw() {
        return has_lacw;
    }

    public boolean isHas_mcw() {
        return has_mcw;
    }

    public String getAiw_category() {
        return aiw_category;
    }

    public boolean isAiw_save_as_interval() {
        return aiw_save_as_interval;
    }

    public boolean isAiw_is_minute_interval() {
        return aiw_is_minute_interval;
    }

    public String getAiw_time_text() {
        return aiw_time_text;
    }

    public ArrayList<String> getAiw_item_list() {
        return aiw_item_list;
    }

    public ArrayList<String> getAiw_item_type_list() {
        return aiw_item_type_list;
    }

    public long getNc_ttr_id() {
        return nc_ttr_id;
    }

    public int getNc_data_index() {
        return nc_data_index;
    }

    public boolean isNc_from_notification() {
        return nc_from_notification;
    }

    public String getTi_search_category() {
        return ti_search_category;
    }

    public ArrayList<Integer> getTi_active_intervals() {
        return ti_active_intervals;
    }

    public String getAintw_category() {
        return aintw_category;
    }

    public boolean isAintw_is_all_categories() {
        return aintw_is_all_categories;
    }

    public int getAintw_spinner_index() {
        return aintw_spinner_index;
    }

    public boolean isAintw_is_minute_interval() {
        return aintw_is_minute_interval;
    }

    public String getAintw_starting_date() {
        return aintw_starting_date;
    }

    public String getLaic_search_category() {
        return laic_search_category;
    }

    public String getLaitw_search_category() {
        return laitw_search_category;
    }

    public boolean isLaitw_list_only_non_intervals() {
        return laitw_list_only_non_intervals;
    }

    public String getMitw_category() {
        return mitw_category;
    }

    public long getMitw_ttr_id() {
        return mitw_ttr_id;
    }

    public ArrayList<String> getMitw_item_list() {
        return mitw_item_list;
    }

    public ArrayList<String> getMitw_item_type_list() {
        return mitw_item_type_list;
    }

    public long getMiw_interval_id() {
        return miw_interval_id;
    }

    public String getMiw_date() {
        return miw_date;
    }

    public String getLacw_search_category() {
        return lacw_search_category;
    }

    public String getMcw_category() {
        return mcw_category;
    }

    public long getMcw_category_id() {
        return mcw_category_id;
    }
}
