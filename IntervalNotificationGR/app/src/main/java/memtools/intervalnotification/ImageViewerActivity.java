package memtools.intervalnotification;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class ImageViewerActivity extends Activity {
	public static final String FILE = "FILE";
	
	View convertView;
	
	@Override
	public void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
				
		//if we're on portrait mode we just finish the video
		/*if (this.getResources().getConfiguration().orientation ==
				Configuration.ORIENTATION_LANDSCAPE) { 
			System.gc();
			this.finish(); 
		}*/
		
		this.setContentView(R.layout.image_view);
						
		ImagePlayer();
	}
	
	private void ImagePlayer() {
		ImageView image = (ImageView) findViewById(R.id.image_view_id);
	
		BitmapFactory.Options options = new BitmapFactory.Options();
		
		//only decoding bounds first
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(getIntent().getStringExtra(FILE), options);
		
		int height = 
				getWindowManager().getDefaultDisplay().getHeight();
		int width = 
				getWindowManager().getDefaultDisplay().getWidth();
		
		//calculating sample size
		options.inSampleSize = calculateInSampleSize(options, height, width);
		
		//now really decoding image
	    options.inJustDecodeBounds = false;
	    Bitmap bm = BitmapFactory.decodeFile(getIntent().getStringExtra(FILE), options);
		
		image.setImageBitmap(bm);
		
		//setting onclick for getting out of this activity
		image.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
			
		});
	}
		
	@Override
	public void onBackPressed () {
		//overriding back press (for good reason :D)
		this.finish();
	}
	
	public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
	    // Raw height and width of image
	    final int height = options.outHeight;
	    final int width = options.outWidth;
	    int inSampleSize = 1;
	
	    if (height > reqHeight || width > reqWidth) {
	        if (width > height) {
	            inSampleSize = Math.round((float)height / (float)reqHeight);
	        } else {
	            inSampleSize = Math.round((float)width / (float)reqWidth);
	        }
	    }
	    return inSampleSize;
	}
}
