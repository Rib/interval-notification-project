//This is the main activity class for this project
//It basically only summons the first necessary view

package memtools.intervalnotification;

import java.util.Calendar;
import java.util.List;

import memtools.intervalnotification.Interface.InterfaceImplementation;
import memtools.intervalnotification.Libs.AddItemAdapter;
import memtools.intervalnotification.Libs.Category;
import memtools.intervalnotification.Libs.Data;
import memtools.intervalnotification.Libs.Datatype;
import memtools.intervalnotification.Libs.DayTick;
import memtools.intervalnotification.Libs.Interval;
import memtools.intervalnotification.Libs.MinuteInterval;
import memtools.intervalnotification.Libs.Preferences;
import memtools.intervalnotification.Libs.ThingToRemember;
import memtools.intervalnotification.ui.AddIntervalWindow;
import memtools.intervalnotification.ui.AddItemWindow;
import memtools.intervalnotification.ui.DateDialog;
import memtools.intervalnotification.ui.ItemViewWindow;
import memtools.intervalnotification.ui.ListAllCategoriesWindow;
import memtools.intervalnotification.ui.ListAllIntervalsWindow;
import memtools.intervalnotification.ui.ListAllItemsWindow;
import memtools.intervalnotification.ui.ModifyCategoryWindow;
import memtools.intervalnotification.ui.ModifyIntervalWindow;
import memtools.intervalnotification.ui.ModifyItemWindow;
import memtools.intervalnotification.ui.SettingsWindow;
import memtools.intervalnotification.ui.StatisticsWindow;
import memtools.intervalnotification.ui.TodaysIntervals;
import memtools.intervalnotification.ui.TodaysTestWindow;
import memtools.intervalnotification.Handlers.ProgramStateHandler;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;

public class IntervalNotification extends ActionBarActivity {
	public static final String SETTINGS_FILE = "config";

	public static final String VIEWS = "VIEWS";
	
	public static final String SUCESS_FACTOR = "success_factor";
	public static final String FAILURE_FACTOR = "failure_factor";
	public static final String GOAL_SUCCESS_RATE = "goal_success_rate";

	public static final int file_browser_result = 25;
	public static final int camera_result = 5;
	
	@Override 
	protected void onStart() {
		super.onStart();
		
		//loads other instances
		otherInstances();	
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		
		for (int i = 0; i < minute_intervals.size(); ++i) {
			minute_intervals.get(i).removeCallbacksAndMessages(null);
		}
		
		minute_intervals.clear();
	}
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        final InterfaceImplementation iface = new InterfaceImplementation(this);

        ProgramStateHandler psh = new ProgramStateHandler();
        boolean has_saved_states = psh.readSavedBundleStates(savedInstanceState);

        // we we have not come from another instance of this activity
		if (!has_saved_states) {
			setContentView(R.layout.loadscreen);
			iface.updateIntervalActivities();
            current_view = R.layout.start_screen_base_layout;
		} else current_view = psh.getCurrent_window_id();
		
		//loads ui instances
		uiInstances();

        ProgramStateHandler psh_extras = new ProgramStateHandler();
        boolean has_extras = psh_extras.readSavedBundleStates(this.getIntent().getExtras());

        //first we check if we have come to the app from notification (or some other route)
        //and handle it accordingly
        if (has_extras) {
            minute_interval_notification = null;

            switch (psh_extras.getCurrent_window_id()) {
                case R.layout.thingtoremember: {
                    ItemViewWindow ivw = new ItemViewWindow(this, null, false);
                    ivw.setVisible();

                    ivw.loadSavedInstanceStates(psh_extras);

                    current_view = R.layout.thingtoremember;
                } break;
            }
        } else if (has_saved_states && psh.getCurrent_window_id() > 0) {
			//if we have something save into instance state we move to correct place
			moveToView(psh.getCurrent_window_id());
					
			//then finally making a case where we return certain values if there is something stored
			switch (psh.getCurrent_window_id()) {
                case R.layout.start_screen_base_layout: {
                    todays_intervals.loadSavedInstanceStates(psh);
                    todays_intervals.setVisible();
                } break;
						
                //add interval window
                //TODO remove this shit from the program (add interval window)
                case R.layout.addinterval: {
                    //if we have anything we know we can return all...
                    AddIntervalWindow temp_add_interval = new AddIntervalWindow(this);
                    temp_add_interval.loadSavedInstanceStates(psh);
					temp_add_interval.setVisible();
                    break;
                }
						
                //add item window
                case R.layout.additem: {
                    AddItemWindow aiw = new AddItemWindow(this);
                    aiw.loadSavedInstanceStates(psh);
					aiw.setVisible();
                    break;
                }
						
			case R.layout.intervals: {
				if (psh.isHas_laic()) {
					//only category
					((AutoCompleteTextView)findViewById(R.id.interval_restriction_field))
					.setText(psh.getLaic_search_category());
								
					//then loading window contents again
					ListAllIntervalsWindow intervals = new ListAllIntervalsWindow(this);
					intervals.loadWindowContent();
                    intervals.setVisible();
				}
				
				break;
			}
						
			case R.layout.items: {
				ListAllItemsWindow laiw = new ListAllItemsWindow(this);
				laiw.loadSavedInstanceStates(psh);
				laiw.loadWindowContent();
                laiw.setVisible();
				
				break;
			}
						
			case R.layout.modifyitem: {
				ModifyItemWindow miw = new ModifyItemWindow(this);
                miw.loadSavedInstanceStates(psh);
                miw.loadWindowContent();

				break;
			}
						
			case R.layout.modify_interval: {
				if (psh.isHas_miw()) {
					//TODO change class cast
					
					Interval interval = iface.getInterval(psh.getMiw_interval_id(),false);
								
					//then moving to correct window
					moveToModifyInterval(interval);
								
					//filling necessary fields
					((EditText)findViewById(R.id.modify_interval_date))
						.setText(psh.getMiw_date());
				}
							
				break;
			}
						
			case R.layout.thingtoremember: {
				ItemViewWindow ivw = new ItemViewWindow(this, null, false);
                ivw.loadSavedInstanceStates(psh);
			} break;
						
			case R.layout.categories: {
				if (psh.isHas_lacw()) {
					//only category
					((AutoCompleteTextView)findViewById(R.id.category_category_search_field))
					.setText(psh.getLacw_search_category());
								
					//the loading categories window again
					ListAllCategoriesWindow cat_win = new ListAllCategoriesWindow(this);
								
					cat_win.loadWindowContent();
				}
				
				break;
			}
						
			case R.layout.modify_category: {
				if (psh.isHas_mcw()) {
					//making new modify category
					ModifyCategoryWindow mod_cat = new ModifyCategoryWindow(this);
					
					Category cat = new Category();
					cat.setId((int)psh.getMcw_category_id());
					cat.setName(psh.getMcw_category());
								
					//now  setting window visible
					mod_cat.setVisible(cat);
								
					//setting current view
					current_view = R.layout.modify_category;
								
					//only category text
					((EditText)findViewById(R.id.modify_category_name))
					.setText(psh.getMcw_category());
								
					//finally loading window contents
					mod_cat.loadWindowContent();
				}
				break;
			}
			
			case R.layout.statistics: {
				moveToView(R.layout.statistics);
				break;
			}
			}
		} else {
			//after loading preferences we go to main window
			moveToView(current_view);
		}
				
		//finally setting from notification to false
		this.getIntent().putExtra(ProgramStateHandler.nc_from_notification_static, false);
	}
		
	@Override
	public void onResume() {
		super.onResume();
		
		todays_intervals.updateContent();
		
		switch (current_view) {
			case R.layout.start_screen_base_layout: {
				//leading todays intervals contents on resume
				//to make sure we have all the necessary stuff visible
				todays_intervals.setVisible();
				
				break;
			}
		}
	}
	
	@Override
	//Saving the necessary information if for some reason this instance is destroyed
	public void onSaveInstanceState (Bundle onState) {
		//just putting current view to we can resume into correct view
		//even tho we "crashed"... some things are unreturnable though
		onState.putInt(ProgramStateHandler.CURRENT_VIEW, current_view);
		
		//then saving necessary information about the view we're currently in
		switch (current_view) {
			case R.layout.start_screen_base_layout: {
				todays_intervals.setSavedInstanceStates(onState);			
				break;
			}
			case R.layout.addinterval: {
				//finding the necessary information
                AddIntervalWindow temp_add_interval = new AddIntervalWindow(this);
                temp_add_interval.setSavedInstanceStates(onState);

				break;
			}
			case R.layout.additem: {
				AddItemWindow aiw = new AddItemWindow(this);
				aiw.setSavedInstanceStates(onState);
				break;
			}
			
			case R.layout.intervals: {
				ListAllIntervalsWindow laiw = new ListAllIntervalsWindow(this);
				laiw.setSavedInstanceStates(onState);
				break;
			}
			
			case R.layout.items: {
				ListAllItemsWindow laiw = new ListAllItemsWindow(this);
				laiw.setSavedInstanceStates(onState);	
				break;
			}
			
			case R.layout.modifyitem: {
				ModifyItemWindow miw = new ModifyItemWindow(this);
				miw.setSavedInstanceStates(onState);
				break;
			}
			
			case R.layout.modify_interval: {
				ModifyIntervalWindow miw = new ModifyIntervalWindow(this);
				miw.setSavedInstanceStates(onState);
				break;
			}
			
			case R.layout.thingtoremember: {
				ItemViewWindow ivw = new ItemViewWindow(this, null, false);
				ivw.setSavedInstanceStates(onState);		
				break;
			}
			
			case R.layout.categories: {
                ListAllCategoriesWindow lacw = new ListAllCategoriesWindow(this);
                lacw.setSavedInstanceStates(onState);
				break;
			}
			
			case R.layout.modify_category: {
				ModifyCategoryWindow mcw = new ModifyCategoryWindow(this);
                mcw.setSavedInstanceStates(onState);
				break;
			}
		}
	}
	
	//options menu inflater
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_menu_bar, menu);
		return true;
	}
	
	//actions to what to do when item on the menu is pressed
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    if (item.getItemId() == R.id.add_item_options) {
			//moving to add items
			moveToAddItems(new View(this));
			return true;
		} else if (item.getItemId() == R.id.exit_options_button) {
			this.stopService(this.getIntent());
			finish();
			return true;
		} else if (item.getItemId() == R.id.all_interval_list) {
			//moving to interval list
			moveToAllIntervals();
			return true;
		} else if (item.getItemId() == R.id.all_item_list) {
			moveToAllItems();
			return true;
		} else if (item.getItemId() == R.id.settings_options_button) {
			moveToSettings();
			return true;
		} else if (item.getItemId() == R.id.todays_intervals_options_button) {
			moveToTodaysIntervals();
			return true;
		} else if (item.getItemId() == R.id.all_categories_list) {
			moveToAllCategories();
			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
	}
			
	private void uiInstances() {
		//loading instances here 'should' be more efficient unless memory becomes a problem
		//then we just move these instances to where they are needed
		
		//loading review (it should be relatively small to keep active all the time)
		//and it is much easier this way
		//review = new ItemViewWindow(this);
				
		//date dialog is small and necessary to keep this way
		date_dialog = new DateDialog(this);
		
		todays_intervals = new TodaysIntervals(this);
		
		
	}
	
	public void otherInstances() {
		//first clearing all notifications because we're in the program now
		//notification manager
		NotificationManager not_mana = (NotificationManager)
				getSystemService(Context.NOTIFICATION_SERVICE);
		
		not_mana.cancelAll();

		
		//also loading minute intervals to memory if necessary
		minute_intervals = MinuteInterval.reactivateMinuteIntervals(this);
		//TODO
		
		//if we haven't got certain preferences already we create them
		SharedPreferences prefs = getSharedPreferences(SETTINGS_FILE,0);
		
		//new editor to edit preferences if they don't exist
		Editor edit = prefs.edit();
		
		if (!prefs.contains(Preferences.DAYS_INTERVALS.toString())) {
			edit.putInt(Preferences.DAYS_INTERVALS.toString(), 100);
		}
		
		if (!prefs.contains(Preferences.DAY_TICK.toString())) {
			edit.putLong(Preferences.DAY_TICK.toString(), 4*1000*60*60);
		}
		
		edit.commit();
		
		//first making new daytick instance
		//which updates all the activities of items
		DayTick r = new DayTick(this);
		//then setting up handler
		Handler daytick = new Handler();
		daytick.postDelayed(r, r.getNextTime());
		
		addMinuteIntervalHandler(daytick);
	}
		
	//*****get functions*****
	//-----------------------
	//***********************
	
	public int getCurrentView () { return current_view;	}
	public Notification getNotification() { return minute_interval_notification; }
	public TodaysIntervals getTodaysIntervals () { return todays_intervals; }
	
	//*****set functions*****
	//-----------------------
	//***********************
	public void setCurrentView (int view_id) { current_view = view_id; }
	public void setNotification(Notification minute_interval_notification) {
		this.minute_interval_notification = minute_interval_notification;
	}
	public void addMinuteIntervalHandler(Handler min_hand) {
		minute_intervals.add(min_hand);
	}
		
	//*****move functions*****
	//------------------------
	//************************
	
	//general move function takes view id as parameter and recognizes 
	//from that where to move
	public void moveToView(int view_id) {
		switch (view_id) {			
			//if we're going to see all intervals
			case R.layout.intervals:  {
				moveToAllIntervals();
				break; 
			}
			
			//if we're going to see todays intervals
			case R.layout.start_screen_base_layout: {
				//we pass random view (as we don't need it anyhow)
				moveToTodaysIntervals();
				break;
			}
			
			//if we're going to add intervals
			case R.layout.addinterval: {
				//we pass random view (because we don't need it)
				moveToAddIntervals(findViewById(R.id.add_interval_button));
				break;
			}
			
			//if we're going to add items
			case R.layout.additem: {
				//again random view
				moveToAddItems(findViewById(R.id.add_interval_button));
				break;
			}
						
			//if we're going to see all items
			case R.layout.items:
			{
				moveToAllItems();
				break;
			}
			
			//if we're going to settings
			case R.layout.settings: {
				moveToSettings();
				break;
			}
			
			//if we're going to list all categories
			case R.layout.categories: {
				moveToAllCategories();
				break;
			}
			
			//if we're going to statistics
			case R.layout.statistics: {
				moveToStatistics();
				break;
			}
		}
	}
	
	//button action for moving into todays intervals
	public void moveToTodaysIntervals () {
		SharedPreferences prefs = this.getSharedPreferences(SETTINGS_FILE, 0);
		if (prefs.getBoolean(Preferences.ORIENTATION_LOCK.toString(),
				true)) {
			//making orientation locked to portrait
			if (this.getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE)
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		} else {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
		}
		
		current_view = R.layout.start_screen_base_layout;
				
		//we pressed button days intervals button so we just change view
		todays_intervals.setVisible();
		
		//loading its contents
		//today_interval.loadWindowContent();
	}
	
	//button action for moving to add new intervals
	public void moveToAddIntervals (View v) {
		//Creating new add interval window
		AddIntervalWindow add_interval = new AddIntervalWindow(this);
		
		//we just change view into add intervals
		add_interval.setVisible();
		current_view = R.layout.addinterval;
		//the load its content
		add_interval.loadWindowContent();
	}
	
	//button action for moving to create new items
	public void moveToAddItems (View v) {
		//creating add items
		AddItemWindow add_item = new AddItemWindow(this);
		//just changes view to item creation
		add_item.setVisible();
		current_view = R.layout.additem;
		//loading window contents
		add_item.loadWindowContent();
	}
	
	public void moveToAllIntervals() {
		//creating list intervals
		ListAllIntervalsWindow intervals = new ListAllIntervalsWindow(this);
		
		//setting screen visible
		intervals.setVisible();
		
		current_view = R.layout.intervals;
		
		//loading window contents
		intervals.loadWindowContent();
	}
	
	public void moveToAllItems() {
		//creating list items
		ListAllItemsWindow items = new ListAllItemsWindow(this);
		
		//setting screen visible
		items.setVisible();
		
		current_view = R.layout.items;

		//loading window contents
		items.loadWindowContent();
	}
	
	public void moveToReview(ThingToRemember ttr, TodaysTestWindow ttw) {
		//making orientation orientation sensor based
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
		
		current_view = R.layout.thingtoremember;
		
		if (ttw == null) {
			ItemViewWindow review = new ItemViewWindow(this, 
					null, false);
			review.setVisible();
			review.loadWindowContent(ttr);
		} else {
			ttw.show();
		}
	}
	
	public void moveToModifyInterval (Interval interval) {
		//creating modify interval window
		ModifyIntervalWindow modify_interval = new ModifyIntervalWindow(this);
		
		//setting modify interval visible
		modify_interval.setVisible(interval);
		current_view = R.layout.modify_interval;
		//loading contents for the window
		modify_interval.loadWindowContent();
	}
	
	public void moveToModifyItem (ThingToRemember ttr) {
		ModifyItemWindow miw = new ModifyItemWindow(this);
		//setting screen visible
		miw.setVisible(ttr);
		current_view = R.layout.modifyitem;
		//loading contents
		miw.loadWindowContent();
	}
		
	public void moveToSettings() {
		//creating new settings window
		SettingsWindow settings = new SettingsWindow(this);
		
		//setting screen visible
		settings.setVisible();
		
		current_view = R.layout.settings;
		
		//loading window contents
		settings.loadWindowContent();
	}
	
	public void moveToAllCategories () {
		//setting correct screen visible
		ListAllCategoriesWindow cat_win = new ListAllCategoriesWindow(this);
		
		//setting it visible
		cat_win.setVisible();
		current_view = R.layout.categories;
		//loading its contents
		cat_win.loadWindowContent();
	}
	
	public void moveToCategory(Category category) {
		ModifyCategoryWindow mod_cat = new ModifyCategoryWindow(this);
		
		//setting window visible
		mod_cat.setVisible(category);
		
		//assigning current view
		current_view = R.layout.modify_category;
		
		//loading contents for the window
		mod_cat.loadWindowContent();
	}
	
	public void moveToStatistics () {
		StatisticsWindow stats = new StatisticsWindow(this);
		
		//setting window visible
		stats.setVisible();
		
		current_view = R.layout.statistics;
				
		//loading window contents
		stats.loadWindowcontent();
	}
		
	//****click functions****
	//-----------------------
	//***********************
	
	//when view all is clicked we move to all intervals window
	//TODO
	//probably going to items anyhow
	public void menuAllIntervalsClick (View v) {
		moveToView(R.layout.items);
	}
	
	//when categories is clicked on menubar we move to categories
	public void menuCategoryClick (View v) {
		moveToView(R.layout.categories);
	}
	
	//when add item in visible menu is clicked we move to add item
	public void menuAddItemClick (View v) {
		moveToView(R.layout.additem);
	}
	
	//when statistics button is pressed
	public void menuStatisticsClick (View v) {
		moveToView(R.layout.statistics);
	}
	
	//when menu button is pressed again
	public void menuReturnClick(View v) {
		moveToView(R.layout.start_screen_base_layout);
	}
			
	/*public void itemDeleteClick(View v) {
		//first deletes the item
		add_item.itemDelete(v);
		//it is necessary to call adapters again
		add_item.loadWindowContent();
	}*/

	public void dateDialogClick (View v) {
		
		//finding minute interval checkbox
		CheckBox micb = (CheckBox) findViewById(R.id.is_minute_interval);
		
		//comparing if it's checked to see if we popup dialog of not
		if (!micb.isChecked()) {	
			if (current_view == R.layout.additem) {
				date_dialog.setVisible(R.id.add_item_iterval_starting_date);
			} else if (current_view == R.layout.modifyitem) {
				date_dialog.setVisible(R.id.modify_item_iterval_starting_date);
			} else {
				//showing the dialog
				date_dialog.setVisible(R.id.date_edit_text);
			}
		}
	}
	
	public void dateDialogModifyIntervalClick(View v) {
		date_dialog.setVisible(R.id.modify_interval_date);
	}
							
	//****Override functions*****
	//---------------------------
	//***************************
	
	//we have to override dialog on creation here
	//seems mandatory
	@Override
	protected Dialog onCreateDialog(int id) {
		// get the current date to date field(s)
        Calendar c = Calendar.getInstance();
		
	    switch (id) {
	    case 0:
	        return new DatePickerDialog(this,
	                    date_dialog.getDatePicker(),
	                    c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
	    }
	    return null;
	}
	
	//when pressing back button we do stuff
	@Override
	public void onBackPressed () {
		//making hierarchy of views
		switch (current_view) {
			//if we're on main window we just exit the program
			case R.layout.start_screen_base_layout: {
				this.moveTaskToBack(true);
				break;
			}
			//if we're in any of the lists we return to main window
			//also item creation, interval creation and settings return to main window
			case R.layout.additem:
			case R.layout.addinterval:
			case R.layout.intervals:
			case R.layout.items:
			case R.layout.settings:
			case R.layout.categories :
			case R.layout.thingtoremember: 
			case R.layout.statistics: {
				moveToView(R.layout.start_screen_base_layout);
				break;
			}
						
			//if we're modifying an item we move to items window
			case R.layout.modifyitem: {
				moveToView(R.layout.items);
				break;
			}
			
			//if  we're modifying an interval we move to intervals window
			case R.layout.modify_interval: {
				moveToView(R.layout.intervals);
				break;
			}
			
			//if we're modifying category we move back to category list
			case R.layout.modify_category: {
				moveToView(R.layout.categories);
				break;
			}
				
		}
		
		return;
	}
	
	//overriding activity result
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {     
	  super.onActivityResult(requestCode, resultCode, data); 
	  switch(requestCode) {
	  
	  //handling file from the file browser
	  case file_browser_result: { 
	      if ((resultCode == Activity.RESULT_OK) && 
	    		  (current_view == R.layout.additem || 
	    		  current_view == R.layout.modifyitem)) { 
	    	  ListView listview;
	    	  
	    	  if (current_view == R.layout.additem) {
	    		  //finding the list view we want to change
	    		  listview = 
	    				  (ListView)findViewById(R.id.item_list_creation);
	    	  } else {
	    		  //finding the list view we want to change
	    		  listview = 
	    				  (ListView)findViewById(R.id.modify_item_listview);
	    	  }
	    		  
	    	  //getting the the text we got from the activity
	    	  String file = data.getStringExtra(FileBrowserActivity.FILE_IDENTIFIER);
	    		  
	    	  int position = this.getIntent()
	    			  .getExtras()
	    			  .getInt(FileBrowserActivity.INDEX_IDENTIFIER);
	    		   
	    	  //also setting up file type
	    	  ((Data)listview.getItemAtPosition(position)).setContent(
	    			  file);
	    	  ((Data)listview.getItemAtPosition(position)).setType(Datatype.FILE);
	    		  
	    	  //then notifying that soemthing has changed
	    	  ((AddItemAdapter)listview.getAdapter()).notifyDataSetChanged();

	      } 
	      break; 
	    }
	  
	  	//handling image from the camera
		case camera_result: {
			 if ((resultCode == Activity.RESULT_OK) && 
		    		  (current_view == R.layout.additem || 
		    		  current_view == R.layout.modifyitem)) { 
				 ListView listview;
				 if (current_view == R.layout.additem) {
					 //finding the list view we want to change
					 listview = 
							 (ListView)findViewById(R.id.item_list_creation);
				 } else {
					 //finding the list view we want to change
					 listview = 
							 (ListView)findViewById(R.id.modify_item_listview);
				 }
				 
	    		  //getting the the text we got from the activity
	    		  String file;
	    		  file = this.getIntent()
	    					 .getExtras().getString(MediaStore.EXTRA_OUTPUT);
	    		 
	    		  //String file = data.getStringExtra(FileBrowserActivity.FILE_IDENTIFIER);
	    		  
	    		  int position = this.getIntent()
	    				  .getExtras()
	    				  .getInt(FileBrowserActivity.INDEX_IDENTIFIER);
	    		   
	    		  //also setting up file type
	    		  ((Data)listview.getItemAtPosition(position)).setContent(
	    				  file);
	    		  ((Data)listview.getItemAtPosition(position)).setType(Datatype.FILE);
	    		  
	    		  //then notifying that soemthing has changed
	    		  ((AddItemAdapter)listview.getAdapter()).notifyDataSetChanged();
	    		  
	    	  }
	    	  break;
		}
	  } 
	}
	
	//*****other functions*****
	//-------------------------
	//*************************
	
	
	//*****variables*****
	//-------------------
	//*******************
	
	//views for back button
	//List<Integer> views = new ArrayList<Integer>();
	
	int current_view = 0;
	
	//ui instances
	//ItemViewWindow review;

	//ModifyItemWindow modify_item;
	DateDialog date_dialog;
	
	Notification minute_interval_notification;
	
	TodaysIntervals todays_intervals;
	
	List<Handler> minute_intervals;

}	

