//This class is for providing proper services to UI

package memtools.intervalnotification.Interface;

import java.util.Date;
import java.util.List;

import memtools.intervalnotification.Libs.Category;
import memtools.intervalnotification.Libs.CompoundStatistics;
import memtools.intervalnotification.Libs.Data;
import memtools.intervalnotification.Libs.DataSmallInfo;
import memtools.intervalnotification.Libs.Interval;
import memtools.intervalnotification.Libs.Intervaltype;
import memtools.intervalnotification.Libs.Repetition;
import memtools.intervalnotification.Libs.ThingToRemember;

public interface Interface {
	
	//constructor takes proper settings file where it can 
	//load settings for the program
	//parameters: first is the name of the file we want to have the settings
	//second is the activity we want to have those settings
	//boolean loadSettings(String settingsfile, Activity activity);
	//settings here seems unnecessary
	
	//simply creates new "ThingToRemember" and returns it
	ThingToRemember newTTR ();
	
	//deletes all items of this thing to remember
	void deleteTTRDataContent(ThingToRemember ttr);
	
	//deletes thing to remember
	void deleteTTR (ThingToRemember ttr);
	
	//returns info about this thing to remember
	DataSmallInfo getTTRInfo (ThingToRemember ttr);
	
	//updates recent info into this ttr
	void updateTTRInfo (ThingToRemember ttr);
	
	//creates new category or recognises old one
	Category addCategory(String category);
	
	//creates new data with correct type(s)
	//parameters: ttr is the item where this data is joined
	ThingToRemember addItem (List<Data> items, ThingToRemember ttr);
	
	//adds lots of items
	 List<ThingToRemember> addItems (List<List<Data>> datas, List<ThingToRemember> ttr);
	 
	 //list all items with that category string and empty category lists all items
	 List<ThingToRemember> listItems (String category, boolean include_interval);
	
	//sets category to list of data
	void setCategoryTTR(Integer category_id, Integer ttr_id);
	
	//lists all categories
	List<Category> listAllCategories ();
	
	//gets items of this thing to remember
	List<Data> getTTRData (ThingToRemember ttr);
	
	//adds data from file
	boolean addFromFile (String filename);
	
	//updates activities of intervals into database
	boolean updateIntervalActivities();
	
	//returns list of intervals which starts from "start" index
	//and how many from the starting index
	List<Interval> nextActiveIntervals (int start, int amount);
	List<Interval> nextInactiveIntervals (int start, int amount);
	
	//gives the amount of currently active intervals
	int amountOfActiveIntervals ();
	//gives the amount of currently inactive intervals
	int amountOfInactiveIntervals ();
	
	
	//updates interval with given id and grade
	boolean updateInterval (double grade, Interval interval, boolean gradeable);
	
	//returns list of data blocks search that search has provided
	//List<Data> searchData (String searchword);

	//gets list of current settings
	//Settings getSettings();
	
	//changes settings
	//boolean changeSettings(Settings settings);
	
	//adds new interval for a thing to remember
	Interval addInterval (int length, Date date, ThingToRemember ttr, Intervaltype type);
	
	//removes interval
	boolean removeInterval (Interval interval);
	
	//lists all intervals that somehow match with search word
	//empty search word should give all the results
	List<Interval> listAllIntervals ();
	
	//gets full interval from the database, with this id
	//has multiple method (look for details in databasedatasource) 
	Interval getInterval(long id, boolean multiple);
	
	//updates data with new string and category
	boolean updateData (Data data, String newcategory, String newdata);
	
	//returns full object of thing to remember
	ThingToRemember getTTR(long ttr_id);
	
	//updates category into the database
	boolean updateCategory(Category category);
	
	//updates necessary statistics
	void updateStatistics (Repetition rep);
	
	//returns statistic from the database
	CompoundStatistics getStatistics();
}
