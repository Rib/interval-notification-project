package memtools.intervalnotification.Interface;

import android.view.LayoutInflater;
import android.view.View;

/**
 * Created by Oiva Moisio on 27.8.2015.
 */
public interface ListItemViewInterface {
    int getViewType();
    View getView(LayoutInflater inflater, View convertView);
}
