package memtools.intervalnotification.Libs;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ViewFlipper;

public class CorrectedViewFlipper extends ViewFlipper {

	public CorrectedViewFlipper(Context context) {
		super(context);
	}
	
    public CorrectedViewFlipper(Context context, AttributeSet attrs) {
    	super(context, attrs);
    }
    
    //then finally we do our magic to stop the program crashing...
    @Override
	protected void onDetachedFromWindow() {
		try {
			super.onDetachedFromWindow();
		}
		catch (IllegalArgumentException e) {
			// Call stopFlipping() in order to kick off updateRunning()
			e.printStackTrace();
			stopFlipping();
		}
	}
}
