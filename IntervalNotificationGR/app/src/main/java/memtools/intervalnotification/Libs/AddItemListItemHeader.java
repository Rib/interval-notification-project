package memtools.intervalnotification.Libs;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.List;

import memtools.intervalnotification.Interface.InterfaceImplementation;
import memtools.intervalnotification.Interface.ListItemViewInterface;
import memtools.intervalnotification.IntervalNotification;
import memtools.intervalnotification.R;

/**
 * Created by Oiva Moisio on 27.8.2015.
 */
public class AddItemListItemHeader implements ListItemViewInterface {
    Context context;

    private String category_text = "";
    private boolean add_as_interval = true;
    private boolean add_as_minute_interval = false;
    private String time = "";

    public AddItemListItemHeader(Context context) {
        this.context = context;

        //getting this day
        Calendar now = Calendar.getInstance();
        DateFormat df = DateFormat.getDateInstance();
        this.time = df.format(now.getTime());
    }

    public String getCategory_text() {
        return category_text;
    }

    public boolean isAdd_as_interval() {
        return add_as_interval;
    }

    public boolean isAdd_as_minute_interval() {
        return add_as_minute_interval;
    }

    public String getTime() {
        return time;
    }

    public void setCategory_text(String category_text) {
        this.category_text = category_text;
    }

    public void setAdd_as_interval(boolean add_as_interval) {
        if (!add_as_interval) add_as_minute_interval = false;

        this.add_as_interval = add_as_interval;
    }

    public void setAdd_as_minute_interval(boolean add_as_minute_interval) {
        boolean changed = (add_as_minute_interval != this.add_as_minute_interval);

        if (changed) {
            if (add_as_minute_interval) this.time = "1";
            else {
                //getting this day
                Calendar now = Calendar.getInstance();
                DateFormat df = DateFormat.getDateInstance();
                this.time = df.format(now.getTime());
            }
        }

        this.add_as_minute_interval = add_as_minute_interval;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getViewType() {
        return AddItemAdapter.RowType.HEADER_ITEM.ordinal();
    }

    public View getView(LayoutInflater inflater, View convertView) {
        // initializations
        View view;
        final ViewHolder viewholder;

        if (convertView == null) {
            view = inflater.inflate(R.layout.view_add_item_header, null);
        } else {
            view = convertView;
        }

        if (view == null) return null;

        if (view.getTag() == null) {
            viewholder = new ViewHolder();
            viewholder.category_text = (AutoCompleteTextView)view.findViewById(R.id.category_input_text);
            viewholder.category_list_button = (ImageView)view.findViewById(R.id.add_item_threshold_trigger_button);
            viewholder.add_as_interval = (CheckBox)view.findViewById(R.id.add_as_interval);
            viewholder.add_as_minute_interval = (CheckBox)view.findViewById(R.id.is_minute_interval);
            viewholder.time = (EditText)view.findViewById(R.id.add_item_iterval_starting_date);

            view.setTag(viewholder);
        } else {
            viewholder = (ViewHolder)view.getTag();
        }


        // setting up values
        viewholder.category_text.setText(getCategory_text());
        viewholder.add_as_interval.setChecked(isAdd_as_interval());
        viewholder.add_as_minute_interval.setChecked(isAdd_as_minute_interval());
        viewholder.time.setText(getTime());

        // setting up listeners and stuff
        viewholder.category_text.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    textView.requestFocus(View.FOCUS_DOWN);
                }

                return true;
            }
        });

        viewholder.category_text.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            public void afterTextChanged(Editable editable) {
                setCategory_text(editable.toString());
            }
        });

        viewholder.time.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (!isAdd_as_minute_interval()) {
                    ((IntervalNotification) context).dateDialogClick(view);
                    return true;
                }

                return false;
            }
        });

        viewholder.time.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            public void afterTextChanged(Editable editable) {
                setTime(editable.toString());
            }
        });

        viewholder.add_as_interval.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                setAdd_as_interval(isChecked);

                if (isChecked) {
                    viewholder.add_as_minute_interval.setVisibility(View.VISIBLE);
                    viewholder.time.setVisibility(View.VISIBLE);
                } else {
                    viewholder.add_as_minute_interval.setVisibility(View.GONE);
                    viewholder.time.setVisibility(View.GONE);
                }
            }
        });

        viewholder.category_list_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                viewholder.category_text.showDropDown();
            }
        });

        // setting up the autocomplete and category shit
        InterfaceImplementation iface = new InterfaceImplementation(context);
        //list of all categories
        List<Category> categories = iface.listAllCategories();

        //adapter for category text field
        ArrayAdapter<Category> category_adapter = new CategoryAdapter(context, categories);

        viewholder.category_text.setThreshold(1);
        viewholder.category_text.setAdapter(category_adapter);

        // setting visibility of certain items
        if (viewholder.add_as_interval.isChecked()) {
            viewholder.add_as_minute_interval.setVisibility(View.VISIBLE);
            viewholder.time.setVisibility(View.VISIBLE);

            // adding listener to minute interval checkbox
            viewholder.add_as_minute_interval.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    setAdd_as_minute_interval(isChecked);
                }
            });
        } else {
            viewholder.add_as_minute_interval.setVisibility(View.GONE);
            viewholder.time.setVisibility(View.GONE);
        }

        return view;
    }

    private class ViewHolder {
        AutoCompleteTextView category_text;
        ImageView category_list_button;
        CheckBox add_as_interval;
        CheckBox add_as_minute_interval;
        EditText time;
    }
}
