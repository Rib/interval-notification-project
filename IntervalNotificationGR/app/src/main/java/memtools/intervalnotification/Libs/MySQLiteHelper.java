//this is a helper class for creating and renewing the database

package memtools.intervalnotification.Libs;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySQLiteHelper extends SQLiteOpenHelper {	
	//defining database name
	public static final String DATABASE_NAME = "intervaldatabase.db";
	public static final int DATABASE_VERSION = 5;
	
	//defining all the names to the tables
	public static final String TABLE_CATEGORY = "category";
	public static final String COLUMN_NAME = "name";
	
	public static final String TABLE_CATEGORY_THING_TO_REMEMBER = "categoryremember";
	public static final String CATEGORY_ID = "category_id";
	public static final String TTR_ID = "thingtoremember_id";
	
	public static final String TABLE_THING_TO_REMEMBER = "thingtoremember";
	public static final String INTERVAL_ID = "interval_id";
	
	public static final String TABLE_INTERVAL = "interval";
	public static final String COLUMN_TYPE = "type";
	public static final String COLUMN_ACTIVE = "active";
	public static final String COLUMN_PREVIOUS_DATE = "prev_date";
	public static final String COLUMN_NEXT_DATE = "next_date";
	public static final String COLUMN_LENGTH = "interval_length";
	public static final String COLUMN_MINUTE = "is_minute_interval";
	
	public static final String TABLE_DATA = "data";
	public static final String COLUMN_CONTENT = "content";
	
	public static final String TABLE_REPETITION = "repetition";
	public static final String COLUMN_TIMESTAMP = "timestamp";
	public static final String COLUMN_SUCCESS = "success";
	
	
	//type casts
	private static final String PRIMARY_AUTO = 
			" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ";
	private static final String CT = "CREATE TABLE IF NOT EXISTS ";
	
	//defining all general column names
	public static final String COLUMN_ID = "_id";
	
	//creation statements
	//they are in correct order in which they should be created
	public static final String CREATE_CATEGORY = 
	CT + TABLE_CATEGORY + 
	"( " + COLUMN_ID + PRIMARY_AUTO + ", " +
	COLUMN_NAME + " text unique not null);";
	
	public static final String CREATE_THING_TO_REMEMBER = 
	CT + TABLE_THING_TO_REMEMBER + 
	"( " + COLUMN_ID + PRIMARY_AUTO + ", " +
	INTERVAL_ID + " integer, " +
	"foreign key( " + INTERVAL_ID + " ) references " +  
	TABLE_INTERVAL + "( " + COLUMN_ID + " ) "+ 
	"ON UPDATE CASCADE ON DELETE SET NULL " + ");";
	
	public static final String CREATE_DATA = CT +
	TABLE_DATA + "( " +  COLUMN_ID + PRIMARY_AUTO + ", " +
	COLUMN_TYPE + " text not null, " + 
	COLUMN_CONTENT + " text not null, " +
	TTR_ID + " integer not null, " +
	"foreign key( " + TTR_ID + " ) references " + 
	TABLE_THING_TO_REMEMBER + "( " + COLUMN_ID + " ) " + 
	"ON UPDATE CASCADE ON DELETE CASCADE " + ");";
	
	public static final String  CREATE_CATEGORY_THING_TO_REMEMBER = CT +
	TABLE_CATEGORY_THING_TO_REMEMBER + "( " + CATEGORY_ID + " integer not null, " +
	TTR_ID + " integer not null, " + 
	"primary key( " + CATEGORY_ID + ", " + TTR_ID + " ), " + 
	"foreign key( " + CATEGORY_ID + " ) references " + 
	TABLE_CATEGORY + "( " + COLUMN_ID + " ) ON UPDATE CASCADE ON DELETE CASCADE, " + 
	"foreign key( " + TTR_ID + " ) references " + 
	TABLE_THING_TO_REMEMBER + "( " + COLUMN_ID + " ) " + 
	"ON UPDATE CASCADE ON DELETE CASCADE " + ");";
	
	public static final String CREATE_INTERVAL = CT +
	TABLE_INTERVAL + "( " + COLUMN_ID + PRIMARY_AUTO + ", " + 
	COLUMN_TYPE + " text not null, " + 
	COLUMN_ACTIVE + " integer not null, " +
	COLUMN_PREVIOUS_DATE + " text not null, " + 
	COLUMN_NEXT_DATE + " text not null, " + 
	COLUMN_LENGTH + " integer not null );";
	
	public static final String CREATE_REPETITION = CT +
	TABLE_REPETITION + "( " + COLUMN_ID + PRIMARY_AUTO + ", " +
	COLUMN_TIMESTAMP + " integer not null, " +
	COLUMN_SUCCESS + " integer not null, " +
	INTERVAL_ID + " integer not null, " +
	"foreign key( " + INTERVAL_ID + " ) references " +  
	TABLE_INTERVAL + "( " + COLUMN_ID + " ) " +
	"ON UPDATE CASCADE ON DELETE CASCADE " + ");";
		
	//full database creation statement
	public static final String DATABASE_CREATE = CREATE_CATEGORY + " " + 
	CREATE_THING_TO_REMEMBER + " " + 
	CREATE_DATA + " " + 
	CREATE_CATEGORY_THING_TO_REMEMBER + " " +
	CREATE_INTERVAL;
	
	//constructor
	public MySQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	//what is done when database is created
	@Override
	public void onCreate(SQLiteDatabase database) {
		database.beginTransaction();
		try {
			//executing create sentences
			database.execSQL(CREATE_CATEGORY);
			database.execSQL(CREATE_THING_TO_REMEMBER);
			database.execSQL(CREATE_DATA);
			database.execSQL(CREATE_CATEGORY_THING_TO_REMEMBER);
			database.execSQL(CREATE_INTERVAL);
			database.execSQL(CREATE_REPETITION);
			//making transaction successful
			database.setTransactionSuccessful();
			
		} finally {
			//ending transaction
			database.endTransaction();
		}
	}
	
	//when opening database we make sure it really exists...
	//some bug in sqlite implementation doesn't create automatically all tables
	@Override
	public void onOpen(SQLiteDatabase database) {
		database.beginTransaction();
		try {
			//executing create sentences
			database.execSQL(CREATE_CATEGORY);
			database.execSQL(CREATE_THING_TO_REMEMBER);
			database.execSQL(CREATE_DATA);
			database.execSQL(CREATE_CATEGORY_THING_TO_REMEMBER);
			database.execSQL(CREATE_INTERVAL);
			database.execSQL(CREATE_REPETITION);
			//making transaction successful
			database.setTransactionSuccessful();
			
		} finally {
			//ending transaction
			database.endTransaction();
		}
	}
		
	//what is done when database is upgraded
	//in this case we just drop all the tables and then create new database
	//from scratch
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		//making upgrade log
		Log.w(MySQLiteHelper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		
		db.beginTransaction();
		try {
			//dropping all the tables (if version is too early
			if (oldVersion < 4) {
				db.execSQL("DROP TABLE IF EXISTS " + TABLE_DATA);
				db.execSQL("DROP TABLE IF EXISTS " + TABLE_INTERVAL);
				db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORY_THING_TO_REMEMBER);
				db.execSQL("DROP TABLE IF EXISTS " + TABLE_THING_TO_REMEMBER);
				db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORY);
			}
			//if we're under certain version we drop also repetition
			//statistics table
			if (newVersion <= 5) {
				db.execSQL("DROP TABLE IF EXISTS " + TABLE_REPETITION);
			}
			
			//setting transaction successful (commit)
			db.setTransactionSuccessful();
		} finally {
			//ending transaction
			db.endTransaction();
		}
		onCreate(db);
	}
}