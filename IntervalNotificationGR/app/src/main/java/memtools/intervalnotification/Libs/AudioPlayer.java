package memtools.intervalnotification.Libs;

import java.io.IOException;
import java.util.Random;

import android.content.Context;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.MediaController;
import android.widget.MediaController.MediaPlayerControl;

public class AudioPlayer extends View implements MediaPlayerControl  {
	Context context;
	
	private MediaController mMediaController;
	private MediaPlayer mMediaPlayer;
	private Handler mHandler = new Handler();
	private int audio_session_id = (new Random()).nextInt();
	
	
	public AudioPlayer(Context context) {
		super(context);
		this.context = context;
	}
	
    public AudioPlayer(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public AudioPlayer(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
    }
	
	public void setContent (Data file, View anchor) {
		if (this.getVisibility() == AudioPlayer.VISIBLE) {
			//making new media player
			mMediaPlayer = new MediaPlayer();
			
			//setting media controllers
	        mMediaController = new MediaController(context);
	        
	        //setting media player for this activity
	        mMediaController.setMediaPlayer(AudioPlayer.this);
	        
	        mMediaController.setAnchorView(anchor);
			
	        try {
	        	//setting data source
	            mMediaPlayer.setDataSource(file.getContent());
	            //preparing (might take a while if slow)
	            mMediaPlayer.prepareAsync();
	            //mMediaPlayer.prepare();
	        } catch (IOException e) {
	            Log.e("AudioPlayerActivity", "Could not open file " 
	            		+ file.getContent() + " for playback.", e);
	        }
	        
	        //setting prepared listener so we know when to start playing
	        mMediaPlayer.setOnPreparedListener(new OnPreparedListener() {
	            @Override
	            public void onPrepared(MediaPlayer mp) {
	            		//when audio is prepared we start playing
	                    mHandler.post(new Runnable() {
	                            public void run() {
	                                    mMediaController.show(10000);
	                                    //mMediaPlayer.start();
	                            }
	                    });
	            }
	        });
		}
	}
	
	@Override
	protected void onFocusChanged(boolean gainFocus, int direction, Rect previouslyFocusedRect) {
		super.onFocusChanged(gainFocus, direction, previouslyFocusedRect);
		
		if (!gainFocus && mMediaPlayer != null) {
			pause();
			stop();
		}
	}
			
	//methods controller
	@Override
	public boolean canPause() {
		return true;
	}

	@Override
	public boolean canSeekBackward() {
		return false;
	}

	@Override
	public boolean canSeekForward() {
		return false;
	}

	@Override
	public int getBufferPercentage() {
		//making sure we don't divide by zero
		int percentage = 0;
		
		if (mMediaPlayer != null) {
			percentage = mMediaPlayer.getDuration() > 0 ?
				(mMediaPlayer.getCurrentPosition() * 100) / 
				mMediaPlayer.getDuration() : 100;
		}
		return percentage;
	}

	@Override
	public int getCurrentPosition() {	
		return mMediaPlayer != null ? mMediaPlayer.getCurrentPosition() : 0;
	}

	@Override
	public int getDuration() {
		return mMediaPlayer != null ? mMediaPlayer.getDuration() : 0;
	}

	@Override
	public boolean isPlaying() {
		return mMediaPlayer != null ? mMediaPlayer.isPlaying() : false;
	}

	@Override
	public void pause() {
		if(mMediaPlayer != null && mMediaPlayer.isPlaying())
			mMediaPlayer.pause();
	}

	@Override
	public void seekTo(int pos) {
		if (mMediaPlayer != null)
		mMediaPlayer.seekTo(pos);		
	}

	@Override
	public void start() {
		if (mMediaPlayer != null)
		mMediaPlayer.start();
	}
	
    @Override
    public boolean onTouchEvent(MotionEvent event) {
    	if (mMediaPlayer != null)
        mMediaController.show();
        
        return true;
    }

	@Override
	public int getAudioSessionId() {
		return audio_session_id;
	}
	
	public void stop() {
		if (mMediaPlayer != null) {
			//this stops and releases resources
			mMediaPlayer.stop();
			mMediaPlayer.release();
		}
	}
}
