//this is the small info about the data packed in this form

package memtools.intervalnotification.Libs;

public class DataSmallInfo {
	//Category
	String category;
	
	//info about the item
	String info;
	
	//date when next active
	String date;
	
	public DataSmallInfo () {
		//initializing variables
		info = "";
		category = "";
		date = "";
	}
	
	//category functions
	public void setCategory(String category) {
		this.category = category;
	}
	
	public String getCategory () { return category; }
	
	//info functions
	public void setInfo (String info) { this.info = info; }
	
	public String getInfo () { return info; }
	
	//date functions
	public void setDate (String date) { this.date = date; }
	
	public String getDate() { return date; }
}
