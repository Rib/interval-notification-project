package memtools.intervalnotification.Libs;

import java.util.Calendar;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class Repetition {
	//static columns
	public static String allColumns[] = { MySQLiteHelper.COLUMN_ID, 
			MySQLiteHelper.COLUMN_TIMESTAMP, MySQLiteHelper.COLUMN_SUCCESS };
	
	//some constants for this class
	public static int ALL = 0;
	public static int SUCCESS = 1;
	public static int UNSUCCESSFUL = 2;
	
	public Repetition () {
		//making id -1 so we know there isn't one :)
		id = -1;
	}
	
	//id operations
	public long getId() { return id; }
	public void setId(Integer id) { this.id = id; }
	
	//timestamp operations
	public long getTimestamp() { return timestamp; }
	public void setTimestamp (long timestamp) { this.timestamp = timestamp; }
	
	//successful operations
	public boolean isSuccessful () { return success; }
	public void setSuccessful (boolean success) { this.success = success; }
	
	//interval operations
	public Interval getInterval() { return interval; }
	public void setInterval (Interval interval) { this.interval = interval; }
	

	public void update(SQLiteDatabase database) {
		
		//assigning content values
		ContentValues content = new ContentValues();
		content.put(MySQLiteHelper.COLUMN_TIMESTAMP, timestamp);
		content.put(MySQLiteHelper.COLUMN_SUCCESS, success);
		content.put(MySQLiteHelper.INTERVAL_ID, interval.getId());
		
		//starting transaction
		database.beginTransaction();
		
		try {
			database.insert(MySQLiteHelper.TABLE_REPETITION,
					null, content);
			
			database.setTransactionSuccessful();
		} catch (SQLException e){
			Log.e("Repetition", "Inserting into database failed " + e);
		} finally {
			database.endTransaction();
		}
	}
	
	public static CompoundStatistics getStatistics(SQLiteDatabase database) {
		//making new statistics
		CompoundStatistics stats = new CompoundStatistics();
		//initializing them
		stats.success_rate = 0;
		stats.avg_things = 0;
		stats.success_rate_100 = 0;
		
		//starting transaction
		database.beginTransaction();
		
		try {
			//"SUM("+allColumns[2]+")/
			
			Cursor cursor = database.query(MySQLiteHelper.TABLE_REPETITION,
					allColumns, null, null, null, null, null);
			
			Calendar now = Calendar.getInstance();
			now.add(Calendar.DAY_OF_MONTH, -7);
			
			int total_reps = 0;
			int total_successful_reps = 0;
			int week_reps = 0;
			int week_successful_reps = 0;
			
			//id,timetamp,success
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				stats.repetitions.add(cursorTransform(cursor));
				++total_reps;
				if (cursor.getLong(1) > now.getTimeInMillis()) {
					++week_reps;
				}
				if (cursor.getInt(2) == 1) {
					++total_successful_reps;
					if (cursor.getLong(1) > now.getTimeInMillis()) {
						++week_successful_reps;
					}
				}
				
				while (!cursor.isLast()) {
					cursor.moveToNext();
					stats.repetitions.add(cursorTransform(cursor));
					++total_reps;
					
					if (cursor.getLong(1) > now.getTimeInMillis()) {
						++week_reps;
					}
					if (cursor.getInt(2) == 1) {
						++total_successful_reps;
						if (cursor.getLong(1) > now.getTimeInMillis()) {
							++week_successful_reps;
						}
					}
				}
			}
						
			cursor.close();
			
			stats.success_rate = total_reps > 0 ? 
					((Integer)total_successful_reps).doubleValue()/
					((Integer)total_reps).doubleValue():0;
			stats.success_rate_100 = week_reps > 0 ? 
					 ((Integer)week_successful_reps).doubleValue()/
							 ((Integer)week_reps).doubleValue():0;
			stats.reviews = total_reps;
			stats.days7_reviews = week_reps; 

			database.setTransactionSuccessful();
		} catch (SQLException e) {
			Log.e("Repetition", "Getting compound statistics failed " + e);
		} finally {
			database.endTransaction();
		}
		
		return stats;
	}
	
	private static Repetition cursorTransform(Cursor cursor) {
		Repetition temp_rep = new Repetition();
		
		temp_rep.setId(cursor.getInt(0));
		temp_rep.setTimestamp(cursor.getLong(1));
		temp_rep.setSuccessful(cursor.getInt(2) == 1 ? true : false);
		
		return temp_rep;
	}
	
	private long id;
	private long timestamp;
	private boolean success;
	private Interval interval;

}
