package memtools.intervalnotification.Libs;

import java.util.List;

import memtools.intervalnotification.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class CategoryListAdapter extends ArrayAdapter<Category> {
	//adapters context
	private final Context context;
	//string values
	private final List<Category> values;
	//constuctor
	public CategoryListAdapter(Context context, List<Category> values) {
		super(context, R.layout.category_list_item,values);
		this.context = context;
		this.values = values;
	}
	
	@Override
	public TextView getView(int position, View convertview, ViewGroup parent) {
		//atm. I have no idea what this does :D
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		//inflates whole row view
		View rowView = inflater.inflate(R.layout.category_list_item, parent, false);
		
		//text field for single row
		TextView textView = 
				(TextView) rowView.findViewById(R.id.category_list_field);
		//textView.setText(values.get(position).getName());
				
		return textView;	
	}
}
