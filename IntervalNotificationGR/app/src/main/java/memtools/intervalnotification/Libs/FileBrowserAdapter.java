package memtools.intervalnotification.Libs;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import memtools.intervalnotification.ImageViewerActivity;
import memtools.intervalnotification.R;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

public class FileBrowserAdapter extends ArrayAdapter<File> {
	Context context;
	
	List<File> file;
	File original;
	List<File> filtered;
	
	Filter filter;
	
	public FileBrowserAdapter (Context context, File file){
		super(context, R.layout.file_browser_single_file, file.listFiles());
		this.context = context;
		this.file = Arrays.asList(file.listFiles());
		this.original = file;
		
		filtered = new ArrayList<File>();
	}

	@Override
	public int getCount() {
		return file.size();
	}

	@Override
	public File getItem(int arg0) {
		return file.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		//gettin the inflater
		LayoutInflater mInflater = LayoutInflater.from(context);
		
		//inflating the view
		convertView = mInflater.inflate(R.layout.file_browser_single_file, parent, false);
		
		
		ViewHolder holder = new ViewHolder();
		
		Data data = new Data();
		data.setContent(getItem(position).toString());
		interpretType(data);
		
		if (convertView == null || convertView.getTag() == null) {
			//finding field(s)
			holder.file_name =
					(TextView)convertView.findViewById(R.id.file_single_file);
			holder.image_indicator =
					(ImageView)convertView.findViewById(R.id.file_image_indicator);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		//setting text if nothing went wrong
		if (file.size() > position && holder.file_name != null)
		holder.file_name.setText(file.get(position).getName());
		
		if (file.get(position).isDirectory()) {
			holder.image_indicator.setImageResource(R.drawable.file_browser_folder);		
		} else if (data.getType() != null && data.getType().equals(Datatype.IMAGE)) {
			//TODO
			//there is a problem with memory release in virtual machine budget
			//holder.image_indicator.setImageResource(R.drawable.file_browser_image);
			BitmapFactory.Options options = new BitmapFactory.Options();
			//only decoding bounds first
			options.inJustDecodeBounds = true;
			BitmapFactory.decodeFile(data.getContent(), options);
			
			//doing sizes to 160 hence it's the biggest size
			int height = 160;
			int width = 160;
			
			//calculating sample size
			options.inSampleSize = ImageViewerActivity.calculateInSampleSize(options, height, width);
			
			//now really decoding image
		    options.inJustDecodeBounds = false;
		    Bitmap bm = BitmapFactory.decodeFile(data.getContent(), options);
		    
		    holder.image_indicator.setImageBitmap(bm);
			
		} else if (data.getType() != null && data.getType().equals(Datatype.VIDEO)) {
			holder.image_indicator.setImageResource(R.drawable.file_browser_audio);
		} else {
			holder.image_indicator.setImageResource(R.drawable.file_browser_text);
		}
				
		return convertView;
	}
	
	@Override
	public Filter getFilter() {
		
		if (filter == null)
			filter = new FileFilter();
				
		return filter;
	}
	
	private class ViewHolder {
		ImageView image_indicator;
		TextView file_name;
	}
	
	//this function recognizes the type of this file/data
	private void interpretType (Data data) {
		if (data.getContent().endsWith(".mp4") || 
				data.getContent().endsWith(".3gp") ||
				data.getContent().endsWith(".webm")) {
			data.setType(Datatype.VIDEO);
		} else if (data.getContent().endsWith(".jpg") ||
				data.getContent().endsWith(".gif") ||
				data.getContent().endsWith(".png") ||
				data.getContent().endsWith(".bmp") ||
				data.getContent().endsWith(".webp")) {
			data.setType(Datatype.IMAGE);
		} else if (data.getContent().endsWith(".flac") ||
				data.getContent().endsWith(".mp3") ||
				data.getContent().endsWith(".mid") ||
				data.getContent().endsWith(".xmf") ||
				data.getContent().endsWith(".mxmf") ||
				data.getContent().endsWith(".rtttl") ||
				data.getContent().endsWith(".rtx") ||
				data.getContent().endsWith(".ota") ||
				data.getContent().endsWith(".imy") ||
				data.getContent().endsWith(".ogg") ||
				data.getContent().endsWith(".wav") ||
				data.getContent().endsWith(".amr")) {
			data.setType(Datatype.AUDIO);
		} else {
			data.setType(Datatype.FILE);
		}
	}
	
	private class FileFilter extends Filter {
		
		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			//making new filter results
			FilterResults result = new FilterResults();
			
			final ArrayList<File> filt = new ArrayList<File>();
            final List<File> lItems = 
            		Arrays.asList(original.listFiles());
            
            //getting the size of items
            int count  = lItems.size();
            
            //then manually filtering results
            for(int i = 0; i < count; ++i) {
                final File temp_file = lItems.get(i);
                
                //here we have the real filtering of items
                if(temp_file.canRead())
                    filt.add(temp_file);
            }
            
            result.count = filt.size();
            result.values = filt;
            
			return result;
		}
	
		@SuppressWarnings("unchecked")
		@Override
		protected void publishResults(CharSequence arg0, FilterResults results) {
			filtered = (ArrayList<File>)results.values;
			notifyDataSetChanged();
			
			file = filtered;
			notifyDataSetInvalidated();	
			
		}
		
	}
}
