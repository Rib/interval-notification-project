package memtools.intervalnotification.Libs;

import java.util.Calendar;

import memtools.intervalnotification.IntervalNotification;
import memtools.intervalnotification.Interface.InterfaceImplementation;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.util.Log;

public class DayTick implements Runnable {
	Context context;
	
	InterfaceImplementation iface;
	
	//constructor
	public DayTick(Context context) {
		this.context = context;
		
		SharedPreferences prefs = 
				this.context.getSharedPreferences(IntervalNotification.SETTINGS_FILE,0);
		
		time_of_day = prefs.getLong(Preferences.DAY_TICK.toString(), 4*1000*60*60);
		
		iface = new InterfaceImplementation(this.context);
	}

	@Override
	public void run() {
		//first updating activities of intervals
		iface.updateIntervalActivities();
		
		//then loading next handler for next update
		((IntervalNotification)context).otherInstances();
		
		//updating todays intervals list
		((IntervalNotification)context).getTodaysIntervals().updateContent();
				
		//making new daytick instance
		//which updates all the activities of items
		DayTick r = new DayTick(context);
		//then setting up handler
		Handler daytick = new Handler();
		daytick.postDelayed(r, r.getNextTime());
		
		((IntervalNotification)context).addMinuteIntervalHandler(daytick);
	}
	
	//gets next time distance in milliseconds
	public long getNextTime () {
		//first getting current time
		Calendar now = Calendar.getInstance();
		
		next_time = 0;
		
		//making current time first
		long hours = now.get(Calendar.HOUR_OF_DAY)*60*60*1000;
		next_time += hours;
		//next_time += 60*60*1000*24-hours;
		long minutes = now.get(Calendar.MINUTE)*60*1000;
		next_time += minutes;
		//next_time += 60*1000*60-minutes;
		long seconds = now.get(Calendar.SECOND)*1000;
		next_time += seconds;
		//next_time += 60*1000-seconds;
		long m_seconds = now.get(Calendar.MILLISECOND);
		next_time += m_seconds;
		//next_time += 1000-m_seconds;
		
		//calculating distance to next time from current time
		next_time = time_of_day-next_time;
		//if it's smaller than 0 we just add 24 hours to make it correct
		if (next_time < 0) next_time += 24*60*60*1000;
		
		Log.v("we have gotten next time", String.valueOf(next_time));
		
		//return 60*1000;
		return next_time;
	}
	
	long time_of_day;
	long next_time;

}
