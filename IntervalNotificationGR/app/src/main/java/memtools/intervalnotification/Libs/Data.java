package memtools.intervalnotification.Libs;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class Data {
	//public stuff
	
	//all columns of data
	public static String allColumns[] = {MySQLiteHelper.COLUMN_ID, MySQLiteHelper.COLUMN_TYPE, 
			MySQLiteHelper.COLUMN_CONTENT, MySQLiteHelper.TTR_ID};
	
	//constructor
	public Data () {
		id = -1;
	}
	
	//id operations
	public long getId() { return id; }
	public void setId(Integer id) { this.id = id; }
	
	//type operations
	public Datatype getType() { return type; }
	public void setType(Datatype type) { this.type = type; }
	
	//content operations
	public String getContent() { return content; }
	public void setContent(String content) {this.content = content; }
	
	//foreign key operations
	public long getTTRID() { return ttr_id; }
	public void setTTRID(Integer ttr_id) { this.ttr_id = ttr_id; }
	
	//tag operations
	public Object getTag() { return tag; }
	public void setTag(Object tag) { this.tag = tag; }
	
	//this will return information about this class in string format
	@Override
	public String toString() {
		return content;
	}
	
	//creates new data
	public static Data create (SQLiteDatabase database,
			Data data, Integer ttr_id) {
		
		//creating content values
		ContentValues content = new ContentValues();
		//adding data to contents
		content.put(MySQLiteHelper.COLUMN_CONTENT, data.getContent());
		content.put(MySQLiteHelper.COLUMN_TYPE, data.getType().toString());
		content.put(MySQLiteHelper.TTR_ID, ttr_id);
		
		//starting transaction
		database.beginTransaction();
		
		try {
			//if data already has some id we know we're just updating
			if (data.getId() != -1) {
				int temp_rows = database.update(MySQLiteHelper.TABLE_DATA,
						content, MySQLiteHelper.COLUMN_ID + " = " + data.getId(),
						null);
			} else {
				//inserting new data
				long insertId = database.insert(MySQLiteHelper.TABLE_DATA, 
						null, content);
				
				//selects -this- data from the database
				Cursor cursor = database.query(MySQLiteHelper.TABLE_DATA, 
						Data.allColumns, MySQLiteHelper.COLUMN_ID + " = " + insertId,
						null, null, null, null);
				
				//checking if we have something
				if (cursor.getCount() > 0) {
					//moving to only content
					cursor.moveToFirst();
					
					data = cursorTransform(cursor);
				}
				
				cursor.close();
			}
			database.setTransactionSuccessful();
			
		} finally {
			database.endTransaction();
		}
		
		return data;
	}
	
	public static void delete (SQLiteDatabase database, Data data) {
		//starting transaciton
		database.beginTransaction();
		
		try {
			database.delete(MySQLiteHelper.TABLE_DATA, 
					MySQLiteHelper.COLUMN_ID + " = " + data.getId(), null);
			
			database.setTransactionSuccessful();
		} finally {
			database.endTransaction();
		}
	}
	
	//transforms cursor of data into data
	public static Data cursorTransform (Cursor cursor) {
		Data tempdata = new Data();
		Datatype tempdatatype;
		
		//selecting data type
		if (cursor.getString(1).equals(Datatype.TEXT.toString())) {
			tempdatatype = Datatype.TEXT;
		} else {
			tempdatatype = Datatype.FILE;
		}
		
		//setting data from the cursor
		tempdata.setId(cursor.getInt(0));
		tempdata.setType(tempdatatype);
		tempdata.setContent(cursor.getString(2));
		tempdata.setTTRID(cursor.getInt(3));
		
		return tempdata;
	}
	
	public static List<Data> findData(SQLiteDatabase database, ThingToRemember ttr) {
		//initializing data list
		List<Data> return_list = new ArrayList<Data>();
		
		//starting transaction
		database.beginTransaction();
		
		try {
			//getting the cursor for all data
			Cursor cursor = database.query(MySQLiteHelper.TABLE_DATA, Data.allColumns,
					MySQLiteHelper.TTR_ID + " = " + ttr.getId(),
					null, null, null, MySQLiteHelper.COLUMN_ID + " ASC");
			
			if (cursor.getCount() > 0) {
				//moving to first item
				cursor.moveToFirst();
				
				return_list.add(Data.cursorTransform(cursor));
				
				while (!cursor.isLast()) {
					cursor.moveToNext();
					
					return_list.add(Data.cursorTransform(cursor));
				}
			}
			
			database.setTransactionSuccessful();
		} finally {
			database.endTransaction();
		}
		
		return return_list;
	}
	
	//private stuff
	
	//id of this data
	private Integer id;
	//type of this data
	private Datatype type;
	//content of this data
	private String content;
	//where this data is joined
	private Integer ttr_id;
	
	//tag for binding data to something
	Object tag;
}
