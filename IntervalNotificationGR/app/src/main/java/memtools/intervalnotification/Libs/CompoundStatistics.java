package memtools.intervalnotification.Libs;

import java.util.ArrayList;
import java.util.List;

public class CompoundStatistics {
	public double success_rate;
	public double success_rate_100;
	public int reviews;
	public int days7_reviews;
	public List<Repetition> repetitions = new ArrayList<Repetition>();
	public double avg_things; 
}
