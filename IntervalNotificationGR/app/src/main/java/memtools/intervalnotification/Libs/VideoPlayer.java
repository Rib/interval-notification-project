package memtools.intervalnotification.Libs;

import java.io.Serializable;

import android.content.Context;
import android.os.Parcel;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.VideoView;

public class VideoPlayer extends VideoView implements Serializable {
	Context context;
	
	VideoView video;
	ImageView thumb;
	
	int position = 0;
	
	String data;
		
	public VideoPlayer(Context context) {
		super(context);
		this.context = context;
	}
	
    public VideoPlayer(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public VideoPlayer(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
    }

	
	public void setContent(ImageView thumb, String data, VideoView video) {
		this.thumb = thumb;
		this.data = data;
		this.video = video;
		
		playVideo();
	}

	private void playVideo () {									
			//setting path
			video.setVideoPath(data);
			
			MediaController controller = new MediaController(context);
			
			//adding controllers to video
			video.setMediaController(controller);
						
			//requesting focus
			video.requestFocus();
	}
	
	@Override
	public void stopPlayback() {
		position = video.getCurrentPosition();
		super.stopPlayback();
	}
	
	@Override
	public void resume() {
		super.resume();
		video.seekTo(position);
		
	}
	
	@Override
    public void pause() {
        super.pause();
        position = video.getCurrentPosition();
        video.setVisibility(VideoPlayer.INVISIBLE);
        thumb.setVisibility(ImageView.VISIBLE);
    }

    @Override
    public void start() {
        super.start();
        if (position > 0) video.seekTo(position);
        video.setVisibility(VideoPlayer.VISIBLE);
        video.bringToFront();
        thumb.setVisibility(ImageView.INVISIBLE);
    }
    
    public String getPath () {
    	return data;
    }
}
