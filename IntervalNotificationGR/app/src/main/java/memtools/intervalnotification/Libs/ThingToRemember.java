package memtools.intervalnotification.Libs;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class ThingToRemember {
	//public stuff
	public static String tableName = MySQLiteHelper.TABLE_THING_TO_REMEMBER;
	public static String allColumns[] = { MySQLiteHelper.TABLE_THING_TO_REMEMBER + 
		"." + MySQLiteHelper.COLUMN_ID,
		MySQLiteHelper.TABLE_THING_TO_REMEMBER + 
		"." + MySQLiteHelper.INTERVAL_ID};
	
	//constructor
	public ThingToRemember () {
		interval_id = -1;
	}
	
	//id operations
	public long getId() { return id; }
	public void setId(Integer id) { this.id = id; }
	
	//inserval operations
	public long getIntervalId() { return interval_id; }
	public void setIntervalId(Integer interval_id) { this.interval_id = interval_id; }
	
	//tag operations
	public Object getTag() { return tag; }
	public void setTag(Object tag) { this.tag = tag; }
	
	//this will return information about this class in string format
	@Override
	public String toString() {
		return getInfob().info;
	}
	
	//gets cursor to all categories this data block has
	public Cursor getCategoryCursor(SQLiteDatabase database) {	
		//helper strings for query
		String cat_id = MySQLiteHelper.TABLE_CATEGORY + "." + MySQLiteHelper.COLUMN_ID;
		String cat_name = MySQLiteHelper.TABLE_CATEGORY + "." + MySQLiteHelper.COLUMN_NAME;
		String select = "SELECT " + cat_id + ", " + cat_name + " ";
		String from = "FROM " + 
		MySQLiteHelper.TABLE_CATEGORY + ", " +
		MySQLiteHelper.TABLE_CATEGORY_THING_TO_REMEMBER + ", " +
		MySQLiteHelper.TABLE_THING_TO_REMEMBER + " ";
		String ttrcat_cat_id = MySQLiteHelper.TABLE_CATEGORY_THING_TO_REMEMBER + "." +
		MySQLiteHelper.CATEGORY_ID;
		String ttrcat_ttr_id = MySQLiteHelper.TABLE_CATEGORY_THING_TO_REMEMBER + "." +
		MySQLiteHelper.TTR_ID;
		String where = "WHERE " + 
		id.toString() + " = " + ttrcat_ttr_id + " AND " +
		ttrcat_cat_id + " = " + cat_id + ";";
		
		//cursor which we return back
		Cursor cursor = null;
		
		//starting transaction
		database.beginTransaction();
				
		try {
			//making the query
			cursor = database.rawQuery(select+from+where, null);
			database.setTransactionSuccessful();
		} finally {
			database.endTransaction();
		}
		
		return cursor;
	}
		
	//gets first data blocks' string
	public DataSmallInfo getInfo( SQLiteDatabase database ) { 
		//getting "fresh" info from the database
		getInfoFromDB(database);
		
		return info; 
	}
	
	//gets raw information (might not be recent)
	public DataSmallInfo getInfob () { return info; }
	
	public void setInfo(DataSmallInfo info)  { this.info = info; }
	
	//updates info on thing to remember
	public void update (SQLiteDatabase database, int interval_id) {
		//making content values
		ContentValues content = new ContentValues();
		//then putting data into it
		content.put(MySQLiteHelper.INTERVAL_ID, interval_id);
				
		//starting the transaction
		database.beginTransaction();
		
		try {
			database.update(MySQLiteHelper.TABLE_THING_TO_REMEMBER,
					content, MySQLiteHelper.COLUMN_ID + " = " + this.id, null);
			
			//setting interval id also to this object
			this.interval_id = interval_id;
			
			database.setTransactionSuccessful();
		} catch (SQLException e){
			Log.e("SQL ERROR", "UPDATING TTR FAILED");
			//trying to end transaction
			database.endTransaction();
		} finally {
			database.endTransaction();
		}
	}
	
	//creates new thing to remember into database
	public static ThingToRemember create (SQLiteDatabase database) {
		ThingToRemember newttr = new ThingToRemember();
		
		//we don't know yet to which interval we put this thing
		//so putting null here
		ContentValues content = new ContentValues();
		content.putNull(MySQLiteHelper.INTERVAL_ID);
		
		database.beginTransaction();

 		try {
 			//executing raw sql (for some reason insert function doesn't
 			//support just inserting default values...)
 			//database.execSQL("INSERT INTO " + MySQLiteHelper.TABLE_THING_TO_REMEMBER +
 				//	" DEFAULT VALUES");
 			long insertId = database.insert(MySQLiteHelper.TABLE_THING_TO_REMEMBER, 
 					null, content);
 			 			
 			//getting cursor for this data
 			//getting all things for remember and sorting them in descending order
 			//so we can choose the one we just inserted
 			Cursor cursor = database.query(MySQLiteHelper.TABLE_THING_TO_REMEMBER,
 					ThingToRemember.allColumns, 
 					MySQLiteHelper.COLUMN_ID + " = " + insertId,
 					null, null, null, null);
 			
 			//if for some reason query was unsuccessful
 			if (cursor.getCount() > 0) {
 				//getting first and only data
 				cursor.moveToFirst();
		
 				//making new thing to remember
 				newttr = ThingToRemember.CursorTransform(cursor);
 			}
 			
 			cursor.close();
 			database.setTransactionSuccessful();
 		} finally {
 			database.endTransaction();
 		}
 		
 		return newttr;
	}
	
	public static boolean delete (SQLiteDatabase database, ThingToRemember ttr) {
		//starting transaction
		database.beginTransaction();
		
		int deleted_rows;
		
		try {
			deleted_rows = database.delete(MySQLiteHelper.TABLE_THING_TO_REMEMBER, 
					MySQLiteHelper.COLUMN_ID + " = " +  ttr.getId(), null);
			
			database.setTransactionSuccessful();
		} finally {
			database.endTransaction();
		}

		return deleted_rows > 0;
	}
	
	//returns most up to date information about ttr
	public static ThingToRemember getTTR (SQLiteDatabase database, long id) {
		//starting transaction
		database.beginTransaction();
		
		ThingToRemember temp_ttr;
		
		try {
			//making the query
			Cursor cursor = database.query(MySQLiteHelper.TABLE_THING_TO_REMEMBER, 
					ThingToRemember.allColumns, 
					MySQLiteHelper.COLUMN_ID + " = " + id, 
					null,null,null,null);
			
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				temp_ttr = ThingToRemember.CursorTransform(cursor);
			} else {
				temp_ttr = new ThingToRemember();
			}
			
			database.setTransactionSuccessful();
		} finally {
			database.endTransaction();
		}
		
		return temp_ttr;
	}
	
	//returns all ttrs with certain category restriction
	//boolean values tells if we include already assigned intervals
	//setting the value true one can send anything through category string
	public static List<ThingToRemember> getAll (SQLiteDatabase database, 
			String category, boolean include_interval) {
		//Initializing list of ttrs
		List<ThingToRemember> ttrs = new ArrayList<ThingToRemember>();
		//making the cursor
		Cursor cursor;
		
		//starting transaction
		database.beginTransaction();
		
		try {
			//getting all necessary data
			
			//if there is no category specified we simply
			//get all things to remember
			if (category.length() == 0 || include_interval) {
				
				//we're checking if we want all or just row that have no interval
				if (include_interval) {
					cursor = database.query(
							MySQLiteHelper.TABLE_THING_TO_REMEMBER,
							ThingToRemember.allColumns, category, null,null,null,null);
				} else {
					cursor = database.query(
							MySQLiteHelper.TABLE_THING_TO_REMEMBER,
							ThingToRemember.allColumns, MySQLiteHelper.INTERVAL_ID + " is null", 
							null,null,null,null);
				}
			
				//checking if there is anything
				if (cursor.getCount() > 0) {
					cursor.moveToFirst();
					
					//adding all the stuff to the list
					ttrs.add(ThingToRemember.CursorTransform(cursor));
					
					while (!cursor.isLast()) {
						cursor.moveToNext();
						ttrs.add(ThingToRemember.CursorTransform(cursor));
					}
					
					cursor.close();
				}
			}
			
			else {
				String ttr_id = MySQLiteHelper.TABLE_THING_TO_REMEMBER + "." + MySQLiteHelper.COLUMN_ID + " ";
				String interval_id = MySQLiteHelper.TABLE_THING_TO_REMEMBER + "." + MySQLiteHelper.INTERVAL_ID + " ";
				String cat_id = MySQLiteHelper.TABLE_CATEGORY + "." + MySQLiteHelper.COLUMN_ID + " ";
				String cat_name = MySQLiteHelper.TABLE_CATEGORY + "." + MySQLiteHelper.COLUMN_NAME + " ";
				String catttr_ttr_id = MySQLiteHelper.TABLE_CATEGORY_THING_TO_REMEMBER + "." +
						MySQLiteHelper.TTR_ID + " ";
				String catttr_cat_id = MySQLiteHelper.TABLE_CATEGORY_THING_TO_REMEMBER + "." +
						MySQLiteHelper.CATEGORY_ID + " ";
				
				String select = "SELECT " + ttr_id + ", " + interval_id;
				String from = "FROM " +  MySQLiteHelper.TABLE_THING_TO_REMEMBER +
						", " + MySQLiteHelper.TABLE_CATEGORY + 
						", " + MySQLiteHelper.TABLE_CATEGORY_THING_TO_REMEMBER + " ";
				String where = "WHERE " + 
						ttr_id + " = " + catttr_ttr_id + " AND " +
						catttr_cat_id + " = " + cat_id + " AND " + 
						cat_name + " = ?";
				
				//we add extra condition for null check
				if (!include_interval) {
					where = where + " AND " + MySQLiteHelper.INTERVAL_ID + " is null";
				}
				
				String[] prep = { category };
				//making raw query because it seem impossible to do joins with normal query
				cursor = database.rawQuery(select+from+where, prep);
								
				if (cursor.getCount() > 0) {
					//making the list
					cursor.moveToFirst();
				
					ttrs.add(ThingToRemember.CursorTransform(cursor));
					while (!cursor.isLast()) {
						cursor.moveToNext();
						ttrs.add(ThingToRemember.CursorTransform(cursor));
					}
				}
			}
			
			//telling that transaction was successful
			database.setTransactionSuccessful();
			
		} finally {
			database.endTransaction();
		}
		
		return ttrs;
	}
	
	//transforms cursor to thing to remember
	public static ThingToRemember CursorTransform (Cursor cursor) {
		ThingToRemember tempttr = new ThingToRemember();
		
		tempttr.setId(cursor.getInt(0));
		
		try {
			//try to assign interval id
			tempttr.setIntervalId(cursor.getInt(1));
			//also if our id is 0 we can safely assume something has gone wrong
			//so that also indicates null value in some systems...
			if (cursor.getInt(1)  == 0) {
				tempttr.setIntervalId(-1);
			}
		} catch (SQLException e) {
			//if we had null value we just put -1 to indicate that
			tempttr.setIntervalId(-1);
			
		}
		
		return tempttr;
	}
	
	//private stuff
	
	private void getInfoFromDB (SQLiteDatabase database) {	
		//starting the transaction
		database.beginTransaction();
		
		try {
			//getting data
			Cursor cursor = database.query(MySQLiteHelper.TABLE_DATA, 
				Data.allColumns, MySQLiteHelper.TTR_ID + " = " + id,
				null, null, null, MySQLiteHelper.COLUMN_ID + " ASC");
			
			//getting gategory
			Cursor cursor2 = this.getCategoryCursor(database);
						
			//getting category
			if (cursor2.getCount() > 0) {
				cursor2.moveToFirst();
				
				info.setCategory(Category.cursorTransform(cursor2).getName());
			}
			
			
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				
				//interval_id = cursor.getInt(1);
				//setting the info
				info.setInfo(Data.cursorTransform(cursor).getContent());
			}
			
			database.setTransactionSuccessful();
			
		} finally {
			database.endTransaction();
		}	
	}
	
	//id of this data
	private Integer id;
	private Integer interval_id;
	private DataSmallInfo info = new DataSmallInfo();
	
	//tag for binding some information to this object
	private Object tag;
}
