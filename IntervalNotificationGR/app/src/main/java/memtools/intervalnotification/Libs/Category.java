package memtools.intervalnotification.Libs;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class Category {
	//public stuff
	
	//all columns of category
	public static String allColumns[] = {MySQLiteHelper.COLUMN_ID, 
		MySQLiteHelper.COLUMN_NAME};
	
	//id operations
	public long getId() { return id; }
	public void setId(Integer id) { this.id = id; }
	
	//name operations
	public String getName() { return name; }
	public void setName(String name) { this.name = name; }
	
	//creates new category into database
	public static Category create (SQLiteDatabase database, String name) {
		Category newcategory = new Category();
		
		//creating content values
		ContentValues content = new ContentValues();
		//adding category into content
		content.put(MySQLiteHelper.COLUMN_NAME, name);
		
		//starting transaction
		database.beginTransaction();
		
		try {
			//first checking if we already have this category
			Cursor cursor = database.query(MySQLiteHelper.TABLE_CATEGORY, 
					Category.allColumns,  MySQLiteHelper.COLUMN_NAME + " = '" + name + "'", 
					null, null, null, null);
			
			if (cursor.getCount() > 0) {
				//now we know there already is same named category
				cursor.moveToFirst();
				
				//sending the category
				newcategory = cursorTransform(cursor);
			}
			
			else {
			
				//getting insert id (and of course inserting data into database
				long insertId = database.insert(MySQLiteHelper.TABLE_CATEGORY,
				null, content);
		
				//getting the cursor of this first data
				cursor = database.query(MySQLiteHelper.TABLE_CATEGORY,
					Category.allColumns, MySQLiteHelper.COLUMN_ID + " = " + insertId,
					null, null, null, null);
		
				if (cursor.getCount() > 0) {
					//moving to first and only cursor location
					cursor.moveToFirst();
		
					//makes new category
					newcategory = cursorTransform(cursor);
				}
			}
			
			cursor.close();
			database.setTransactionSuccessful();
		} finally {
			database.endTransaction();
		}
		
		return newcategory;
	}

	public static Category cursorTransform (Cursor cursor) {
		Category tempcategory = new Category();
		
		//setting proper values to category
		tempcategory.setId(cursor.getInt(0));
		tempcategory.setName(cursor.getString(1));
		
		return tempcategory;
	}
	
	public boolean update(SQLiteDatabase database) {
		//starting transaction
		database.beginTransaction();
		
		int result;
		
		try {
			ContentValues content = new ContentValues();
			content.put(MySQLiteHelper.COLUMN_NAME, this.getName());
			
			//just updating contents
			result = database.update(MySQLiteHelper.TABLE_CATEGORY,
					content, MySQLiteHelper.COLUMN_ID + " = " + this.getId(), null);
			
			database.setTransactionSuccessful();
		} finally {
			database.endTransaction();
		}
		return result > 0 ? true : false;
	}
	
	//this will return information about this class in string format
	@Override
	public String toString() {
		return name == null ? "" :
			name.toLowerCase();
	}
		
	//private stuff
	
	//id of this data
	private Integer id;
	//name of the category
	private String name;


}
