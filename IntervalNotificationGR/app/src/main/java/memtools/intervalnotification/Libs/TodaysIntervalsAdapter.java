package memtools.intervalnotification.Libs;

import java.util.ArrayList;

import memtools.intervalnotification.R;
import memtools.intervalnotification.ui.TodaysIntervalsWindow;
import android.content.Context;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class TodaysIntervalsAdapter extends ArrayAdapter<Interval> {
	Context context;
	TodaysIntervalsWindow window_context;
	
	ArrayList<Interval> intervals;
	ArrayList<Interval> original;
	ArrayList<Interval> filtered = new ArrayList<Interval>();
	
	Filter filter;
	
	public TodaysIntervalsAdapter(Context context,
								  TodaysIntervalsWindow window_context, ArrayList<Interval> intervals) {
		super(context, R.layout.window_todays_intervals_layout, intervals);
		this.context = context;
		this.intervals = new ArrayList<Interval>(intervals);
		this.original = new ArrayList<Interval>(intervals);
		this.window_context = window_context;
	}

	//gets list size 
	@Override
	public int getCount() {
		return intervals.size();
	}

	//gets the item from the list
	@Override
	public Interval getItem(int arg0) {
		return intervals.get(arg0);
	}

	//gets the id of the item
	@Override
	public long getItemId(int arg0) {
		return intervals.get(arg0).getId();
	}
	
	@Override
	public Filter getFilter() {
		
		if (filter == null)
			filter = new IntervalFilter();
				
		return filter;
	}

	//get view override
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		//getting the inflater
		LayoutInflater mInflater = LayoutInflater.from(context);
		
		//creating holder
		ViewHolder holder;
		
		//inflating the item		
		/*convertView = intervals.get(position).isActive() ? 
				mInflater.inflate(R.layout.item_todays_intervals_list, parent, false) :
				mInflater.inflate(R.layout.days_intervals_inactive_item, parent, false)	;*/
		if (convertView == null || convertView.getTag() == null) {
			
			if (convertView == null)
			convertView = mInflater.inflate(R.layout.item_todays_intervals_list, parent, false);

            Drawable bg_control = convertView.getBackground();
            bg_control.setAlpha(175);
            convertView.setBackgroundDrawable(bg_control);

            //setting tag of current view for further use
            intervals.get(position).setTag(convertView);
						
			//making new viewholder
			holder = new ViewHolder();
				
			//assigning views
			holder.category = (TextView) 
					convertView.findViewById(R.id.days_intervals_active_category);
			holder.info = (TextView)
					convertView.findViewById(R.id.days_intervals_active_info);
			holder.type = (ImageView)
					convertView.findViewById(R.id.days_intervals_image_type);
			holder.bind = (LinearLayout)
					convertView.findViewById(R.id.days_intervals_active_single_item);
			
			convertView.setTag(holder);
		
		} else {
			holder = (ViewHolder) convertView.getTag();
			convertView.setBackgroundResource(R.drawable.ttr_button);

            Drawable bg_control = convertView.getBackground();
            bg_control.setAlpha(175);
            convertView.setBackgroundDrawable(bg_control);
		}
		
		//choosing view either is active or not
		if (intervals.get(position).isActive()) {
            Drawable draw = context.getResources().getDrawable(R.drawable.ic_todays_interval_item_is_active);

            //To generate negative image
            float[] colorMatrix_Negative = {
                    0, 0, 0, 0, 255, //red
                    0, 0, 0, 0, 255, //green
                    0, 0, 0, 0, 255, //blue
                    0, 0, 0, 1.0f, 0 //alpha
            };

            ColorFilter colorFilter_Negative = new ColorMatrixColorFilter(colorMatrix_Negative);

            if (draw != null) {
                draw.setColorFilter(colorFilter_Negative);
            }

            holder.type.setImageDrawable(draw);
			
		} else {
            Drawable draw = context.getResources().getDrawable(R.drawable.ic_todays_intervals_item_is_inactive);

            //To generate negative image
            float[] colorMatrix_Negative = {
                    -1.0f, 0, 0, 0, 255, //red
                    0, -1.0f, 0, 0, 255, //green
                    0, 0, -1.0f, 0, 255, //blue
                    0, 0, 0, 1.0f, 0 //alpha
            };

            ColorFilter colorFilter_Negative = new ColorMatrixColorFilter(colorMatrix_Negative);

            if (draw != null) {
                draw.setColorFilter(colorFilter_Negative);
            }

            holder.type.setImageDrawable(draw);
		}
		
		if (intervals.get(position).getTTR() != null) {
			//finding maximum lengths
			int cat_length = intervals.get(position).getTTR().getInfob().getCategory().length();
			cat_length = cat_length < 20 ? cat_length : 20;
			
			int info_length = intervals.get(position).getTTR().getInfob().getInfo().length();
			info_length = info_length < 20 ? info_length : 20;
			
			//category assignment
			if (cat_length > 0) {
				holder.category.setText(
						intervals.get(position).getTTR().getInfob().getCategory().subSequence(0, cat_length));
			} else {
				holder.category.setText("");
			}
			
			//info assignment
			if (info_length > 0) {
				holder.info.setText(
						intervals.get(position).getTTR().getInfob().getInfo().subSequence(0, info_length));
			} else {
				holder.info.setText("");
			}
		
		}
		
		//then finally see if we have active minute interval
		if (intervals.get(position).getType() == Intervaltype.MINUTE) {
            Drawable draw = context.getResources().getDrawable(R.drawable.ic_todays_intervals_item_minute);

            //To generate negative image
            float[] colorMatrix_Negative = {
                    0, 0, 0, 0, 255, //red
                    0, 0, 0, 0, 255, //green
                    0, 0, 0, 0, 255, //blue
                    0, 0, 0, 1.0f, 0 //alpha
            };

            ColorFilter colorFilter_Negative = new ColorMatrixColorFilter(colorMatrix_Negative);

            if (draw != null) {
                draw.setColorFilter(colorFilter_Negative);
            }

            holder.type.setImageDrawable(draw);
		}
		
		//setting click action to bindview
		holder.bind.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				window_context.moveToReview(position);
			}
			
		});
		
		convertView.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View convertView, MotionEvent action) {
				if (action.getAction() == MotionEvent.ACTION_DOWN) {
					convertView.setBackgroundResource(R.drawable.ttr_button_down);
				} else if (action.getAction() == MotionEvent.ACTION_UP ||
						action.getAction() == MotionEvent.ACTION_CANCEL ||
						action.getAction() == MotionEvent.ACTION_OUTSIDE) {
					convertView.setBackgroundResource(R.drawable.ttr_button);
				}
                Drawable bg_control = convertView.getBackground();
                bg_control.setAlpha(175);
                convertView.setBackgroundDrawable(bg_control);
				return false;
			}

		});
						
		return convertView;
	}
	
	class ViewHolder {
		TextView category;
		TextView info;
		ImageView type;
		LinearLayout bind;
	}
	
	private class IntervalFilter extends Filter{
		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			//making new filter results
			FilterResults result = new FilterResults();
			
			String prefix = "";
			
			if (constraint != null)
				prefix = constraint.toString().toLowerCase();
			
			if(constraint != null && prefix.length() > 0) {
				synchronized(this){
					final ArrayList<Interval> filt = new ArrayList<Interval>();
	                final ArrayList<Interval> lItems = new ArrayList<Interval>(original);
	                
	                //getting the size of items
	                int count  = lItems.size();
	                
	                //manually filtering results
	                for(int i = 0; i < count; ++i) {
	                    final Interval interval = lItems.get(i);
	    				String category = interval.getTTR().getInfob().getCategory();
	    				String info = interval.getTTR().getInfob().getInfo();
	                    final String interval_string = (category + " " + info).toLowerCase();	
	                    
	                    if(interval_string.contains(prefix) && 
	                    	(interval.isActive() || 
	                    	(!interval.getType().equals(Intervaltype.MINUTE))))
	                        filt.add(interval);
	                }
	                
	                //then adding into filtered stuff
	                result.count = filt.size();
	                result.values = filt;
				}
			} else {
				synchronized(this){
					final ArrayList<Interval> filt = new ArrayList<Interval>();
	                final ArrayList<Interval> lItems = new ArrayList<Interval>(original);
	                
	                //getting the size of items
	                int count  = lItems.size();
	                
	                //manually filtering results
	                for(int i = 0; i < count; ++i) {
	                    final Interval interval = lItems.get(i);
	                    if (interval.isActive() || (!interval.getType().equals(Intervaltype.MINUTE)))
	                    	filt.add(interval);
	                }
	                
	                //then adding into filtered stuff
	                result.count = filt.size();
	                result.values = filt;
				}
            }
			
			return result;
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void publishResults(CharSequence constraint,
				FilterResults results) {
			filtered = (ArrayList<Interval>)results.values;
			notifyDataSetChanged();
				
			//setting categories
			intervals = filtered;
			notifyDataSetInvalidated();
		}
	}
	
}
