package memtools.intervalnotification;

import java.io.File;

import memtools.intervalnotification.Libs.FileBrowserAdapter;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class FileBrowserActivity extends Activity {

	public static final String FILE_IDENTIFIER = "FILE_IDENTIFIER";
	public static final String INDEX_IDENTIFIER = "INDEX_IDENTIFIER";
	
	ListView file_list;
	TextView header;
	
	File file;
	
	//on activity creation we read current directory contents and show it
	@Override
	public void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//(this)
		//.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
		
		//setting content view
		setContentView(R.layout.file_browser_layout);
		
		//opening the file
		file = new File("/sdcard/");
		
		loadWindowContent();
	}
	
	//this method loads contents of this file browser
	private void loadWindowContent() {		
		//finding necessary fields
		findFields();
		
		header.setText(file.getAbsolutePath());
				
		//making new array adapter
		FileBrowserAdapter file_adapter = 
				new FileBrowserAdapter(this, file);
		
		//this line is required for some reason to activate the filter
		file_adapter.getFilter().filter("");
		
		//assigning adapter
		file_list.setAdapter(file_adapter);
		
		//assigning operation for item click
		file_list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View arg1, int postion,
					long arg3) {
				//when we click on directory
				if (((File)adapter.getItemAtPosition(postion)).isDirectory()) {
					file = new File(((File)adapter.getItemAtPosition(postion)).getAbsolutePath());
					loadWindowContent();
				} else if (((File)adapter.getItemAtPosition(postion)).isFile()) {
					Intent resultIntent = new Intent();
					
					//putting extra to this new random intent
					resultIntent.putExtra(FILE_IDENTIFIER, 
							((File)adapter.getItemAtPosition(postion)).getAbsolutePath());
					
					setResult(Activity.RESULT_OK, resultIntent);
					finish();
				}
			}
			
		});
	}
	
	//overriding back button for convenience
	@Override
	public void onBackPressed () {
		Log.v("parent", file.getParent());
		//if we're at the root directory we just end this activity
		if (file.getParent() == null || file.getParent().equals("/")) {
			this.finish();
		} else {
			file = file.getParentFile();
			loadWindowContent();
		}
	}

		
	//finds file browser fields
	private void findFields() {
		file_list = (ListView) findViewById(R.id.file_browser_list);
		header = (TextView) findViewById(R.id.file_browser_file_path);
	}

}
