package memtools.intervalnotification;

import memtools.intervalnotification.Libs.VideoPlayer;
import android.app.Activity;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

public class VideoPlayerActivity extends Activity {
	public static final String FILE = "FILE";
	
	public static final String VIDEO_CONTINUE = "VIDEO_CONTINUE";
	
	View convertView;
	
	VideoPlayer video_view;
	
	@Override
	public void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.setContentView(R.layout.video_view);
				
		//checking if we're returning from this activity
		if (savedInstanceState != null && 
				savedInstanceState.containsKey("FILE")) {
		
			videoPlayer(savedInstanceState.getString(FILE));
			video_view.seekTo(savedInstanceState.getInt(VIDEO_CONTINUE));
			video_view.start();
		} else {
			String file = getIntent().getStringExtra(FILE);
			videoPlayer(file);
			video_view.seekTo(getIntent().getIntExtra(VIDEO_CONTINUE, 0));
		}
		
		final GestureDetector gd = 
				new GestureDetector(new CustomVideoGestureDetector());
		
		View.OnTouchListener votl = new View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return gd.onTouchEvent(event);
			}
			
		};
		
		Button fl = (Button) findViewById(R.id.video_view_action_button);
		
		fl.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});

		fl.setOnTouchListener(votl);
	}
	
	@Override
	public void onSaveInstanceState (Bundle onState) {
		//first saving filename
		onState.putString(FILE, getIntent().getStringExtra(FILE));
		
		//then putting current video position
		onState.putInt(VIDEO_CONTINUE, video_view.getCurrentPosition());
	}
	
	private void videoPlayer(String file) {
		Bitmap curThumb = 
				ThumbnailUtils.createVideoThumbnail(file,
				MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);
						
		video_view = (VideoPlayer) findViewById(R.id.video_view_id);
		
		ImageView thumb = (ImageView) this.findViewById(R.id.thumbnail_in_video_view);
		thumb.setImageBitmap(curThumb);
		video_view.setContent(thumb, getIntent().getStringExtra(FILE), video_view);
	}
	
	@Override
	public void onBackPressed () {
		//overriding back press (for good reason :D)
		this.finish();
	}
	
	private class CustomVideoGestureDetector extends SimpleOnGestureListener
	implements OnDoubleTapListener {
		
        //on double tap we go to full screen
 		@Override
 		public boolean onDoubleTap(MotionEvent e) {
 			video_view.pause();
 	    	finish();
 	    	return true;
 		}
 		
 		@Override
 		public boolean onDoubleTapEvent(MotionEvent e) {
 			video_view.pause();
 			finish();
 			return true;
 		}
 		
		//on confirmed single tap we start playing
		@Override
		public boolean onSingleTapConfirmed(MotionEvent e) {
			if (!video_view.isPlaying()) video_view.start();
			else video_view.pause();
			
			return false;
		}
	}
}
