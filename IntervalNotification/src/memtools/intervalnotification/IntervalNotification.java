//This is the main activity class for this project
//It basically only summons the first necessary view

package memtools.intervalnotification;

import java.util.Calendar;
import java.util.List;

import memtools.intervalnotification.Interface.InterfaceImplementation;
import memtools.intervalnotification.Libs.AddItemAdapter;
import memtools.intervalnotification.Libs.Category;
import memtools.intervalnotification.Libs.Data;
import memtools.intervalnotification.Libs.Datatype;
import memtools.intervalnotification.Libs.DayTick;
import memtools.intervalnotification.Libs.Interval;
import memtools.intervalnotification.Libs.MinuteInterval;
import memtools.intervalnotification.Libs.Preferences;
import memtools.intervalnotification.Libs.ThingToRemember;
import memtools.intervalnotification.ui.AddIntervalWindow;
import memtools.intervalnotification.ui.AddItemWindow;
import memtools.intervalnotification.ui.DateDialog;
import memtools.intervalnotification.ui.ItemViewWindow;
import memtools.intervalnotification.ui.ListAllCategoriesWindow;
import memtools.intervalnotification.ui.ListAllIntervalsWindow;
import memtools.intervalnotification.ui.ListAllItemsWindow;
import memtools.intervalnotification.ui.ModifyCategoryWindow;
import memtools.intervalnotification.ui.ModifyIntervalWindow;
import memtools.intervalnotification.ui.ModifyItemWindow;
import memtools.intervalnotification.ui.SettingsWindow;
import memtools.intervalnotification.ui.StatisticsWindow;
import memtools.intervalnotification.ui.TodaysIntervals;
import memtools.intervalnotification.ui.TodaysTestWindow;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

public class IntervalNotification extends Activity {
	public static final String SETTINGS_FILE = "config";
	
	public static final String CURRENT_VIEW = "CURRENT_VIEW";
	public static final String VIEWS = "VIEWS";
	
	public static final String SUCESS_FACTOR = "success_factor";
	public static final String FAILURE_FACTOR = "failure_factor";
	public static final String GOAL_SUCCESS_RATE = "goal_success_rate";
		
	//add interval required fields
	public static final String category_static = "CATEGORY";
	public static final String category_selection_satic = "CATEGORY_SELECTION";
	public static final String all_categories_static = "ALL_CATEGORIES";
	public static final String spinner_static = "SPINNER_VALUE";
	public static final String minute_interval_static = "MINUTE_INTERVAL";
	public static final String date_static = "STARTING_DATE";
	
	//interval id static
	public static final String interval_id_static = "INTERVAL_ID";
	
	//ttr id static
	public static final String ttr_id_static = "TTR_ID";
	
	//add item special
	public static final String item_list_static = "ITEM_LIST";
	public static final String item_typelist_static = "ITEM_TYPELIST";
	
	//modify item special
	public static final String item_id_static = "ITEM_ID";
			
	//modify category special
	public static final String category_id_static = "CATEGORY_ID";
	
	//has come from the notification
	public static final String from_notification = "FROM_NOTIFICATION";
	
	public static final int file_browser_result = 25;
	public static final int camera_result = 5;
	
	@Override 
	protected void onStart() {
		super.onStart();
		
		//loads other instances
		otherInstances();	
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		
		for (int i = 0; i < minute_intervals.size(); ++i) {
			minute_intervals.get(i).removeCallbacksAndMessages(null);
		}
		
		minute_intervals.clear();
	}
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        final InterfaceImplementation iface = new InterfaceImplementation(this);
        
        final boolean has_return = (savedInstanceState != null ? 
        		savedInstanceState.containsKey(CURRENT_VIEW) : false);
        
		if (!has_return) {
			setContentView(R.layout.loadscreen);
			iface.updateIntervalActivities();
		}
		
		//loads ui instances
		uiInstances();
				
		//defaulting current view to mainwindow
		current_view = R.layout.daysintervals;
        
		/*while (!((IntervalNotification)context)
				.hasWindowFocus()) {
		}*/
		
		//first we check if we have come to the app from notification (or some other route)
		//and handle it accordingly
		if (this.getIntent().hasExtra(CURRENT_VIEW) && 
				this.getIntent().getBooleanExtra(from_notification, false)) {
			//setting notification to null hence we have atleast cleared it
			minute_interval_notification = null;
					
			//checking if we have come from notification
			switch (this.getIntent().getIntExtra(CURRENT_VIEW, R.layout.thingtoremember)) {
				case R.layout.thingtoremember: {
					if (this.getIntent().hasExtra(CURRENT_VIEW)) {
						long interval_id = this.getIntent()
								.getLongExtra(ItemViewWindow.ttr_id_static, -1);
						Bundle states = new Bundle();
						states.putLong(ItemViewWindow.ttr_id_static, interval_id);
						states.putInt(ItemViewWindow.data_index_static,
								this.getIntent()
								.getIntExtra(ItemViewWindow.data_index_static, 1));
						ItemViewWindow ivw = new ItemViewWindow(this, null, false);
						ivw.setVisible();
						ivw.loadSavedInstanceStates(states);
						current_view = R.layout.thingtoremember;
					}
					break;
				}
			}
			//if we have come with minute notification action we directly 
					//move to todays intervals (parameter is some random view)
		} else if (savedInstanceState != null && 
				savedInstanceState.containsKey(CURRENT_VIEW) && 
				savedInstanceState.getInt(CURRENT_VIEW) != 0) {

			//if we have something save into instance state we move to correct place
			moveToView(savedInstanceState.getInt(CURRENT_VIEW));
					
			//then finally making a case where we return certain values if there is something stored
			switch (savedInstanceState.getInt(CURRENT_VIEW)) {
			case R.layout.daysintervals: {
				todays_intervals.loadSavedInstanceStates(savedInstanceState);
				todays_intervals.setVisible();
			} break;
						
			//add interval window
			case R.layout.addinterval: {
				//if we have anything we know we can return all...
				if (savedInstanceState.containsKey(category_static)) {
					//first category
					((AutoCompleteTextView)findViewById(R.id.category_autocomplete_view))
					.setText(savedInstanceState.getString(category_static));
								
					//loading window contents
					AddIntervalWindow temp_add_interval = new AddIntervalWindow(this);
					temp_add_interval.loadWindowContent();
								
					//then checking (or unchecking all categories checkbox)
					((CheckBox)findViewById(R.id.choose_all_categories))
					.setChecked(savedInstanceState.getBoolean(all_categories_static));
								
					//minute interval checkbox
					((CheckBox)findViewById(R.id.is_minute_interval))
					.setChecked(savedInstanceState.getBoolean(minute_interval_static));
								
					//date field
					((EditText)findViewById(R.id.date_edit_text))
					.setText(savedInstanceState.getString(date_static));
								
					//then finally setting that item
					Spinner temp_spinner = ((Spinner)findViewById(R.id.item_chooser));
					temp_spinner.setSelection(savedInstanceState.getInt(spinner_static));
				}
							
				break;
			}
						
			//add item window
			case R.layout.additem: {
				AddItemWindow aiw = new AddItemWindow(this);
				aiw.loadSavedInstanceStates(savedInstanceState);					
				break;
			}
						
			case R.layout.intervals: {
				if (savedInstanceState.containsKey(category_static)) {
					//only category
					((AutoCompleteTextView)findViewById(R.id.interval_restriction_field))
					.setText(savedInstanceState.getString(category_static));
								
					//then loading window contents again
					ListAllIntervalsWindow intervals = new ListAllIntervalsWindow(this);
								
					intervals.loadWindowContent();
				}
				
				break;
			}
						
			case R.layout.items: {
				ListAllItemsWindow laiw = new ListAllItemsWindow(this);
				laiw.loadSavedInstanceStates(savedInstanceState);
				laiw.loadWindowContent();
				
				break;
			}
						
			case R.layout.modifyitem: {
				ModifyItemWindow miw = new ModifyItemWindow(this);
				if (miw.loadSavedInstanceStates(savedInstanceState)) {
					current_view = R.layout.modifyitem;
				}
							
				break;
			}
						
			case R.layout.modify_interval: {
				if (savedInstanceState.containsKey(date_static)) {
					//TODO change class cast
					long interval_id = savedInstanceState
							.getLong(IntervalNotification.interval_id_static);
					
					Interval interval = iface.getInterval(interval_id,false);
								
					//then moving to correct window
					moveToModifyInterval(interval);
								
					//filling necessary fields
					((EditText)findViewById(R.id.modify_interval_date))
						.setText(savedInstanceState.getString(date_static));
				}
							
				break;
			}
						
			case R.layout.thingtoremember: {
				current_view = R.layout.thingtoremember;
				ItemViewWindow ivw = new ItemViewWindow(this, null, false);
				ivw.loadSavedInstanceStates(savedInstanceState);
			} break;
						
			case R.layout.categories: {
				if (savedInstanceState.containsKey(category_static)) {
					//only category
					((AutoCompleteTextView)findViewById(R.id.category_category_search_field))
					.setText(savedInstanceState.getString(category_static));
								
					//the loading categories window again
					ListAllCategoriesWindow cat_win = new ListAllCategoriesWindow(this);
								
					cat_win.loadWindowContent();
				}
				
				break;
			}
						
			case R.layout.modify_category: {
				if (savedInstanceState.containsKey(category_static)) {
					//making new modify category
					ModifyCategoryWindow mod_cat = new ModifyCategoryWindow(this);
					
					Category cat = new Category();
					cat.setId((int)savedInstanceState.getLong(category_id_static));
					cat.setName(savedInstanceState.getString(category_static));
								
					//now  setting window visible
					mod_cat.setVisible(cat);
								
					//setting current view
					current_view = R.layout.modify_category;
								
					//only category text
					((EditText)findViewById(R.id.modify_category_name))
					.setText(savedInstanceState.getString(category_static));
								
					//finally loading window contents
					mod_cat.loadWindowContent();
				}
				break;
			}
			
			case R.layout.statistics: {
				moveToView(R.layout.statistics);
				break;
			}
			}
		} else {
			//after loading preferences we go to main window
			moveToView(current_view);
		}
				
		//finally setting from notification to false
		this.getIntent().putExtra(from_notification, false);
	}
		
	@Override
	public void onResume() {
		super.onResume();
		
		todays_intervals.updateContent();
		
		switch (current_view) {
			case R.layout.daysintervals: {
				//leading todays intervals contents on resume
				//to make sure we have all the necessary stuff visible
				todays_intervals.setVisible();
				
				break;
			}
		}
	}
	
	@Override
	//Saving the necessary information if for some reason this instance is destroyed
	public void onSaveInstanceState (Bundle onState) {
		//just putting current view to we can resume into correct view
		//even tho we "crashed"... some things are unreturnable though
		onState.putInt(CURRENT_VIEW, current_view);
		
		//then saving necessary information about the view we're currently in
		switch (current_view) {
			case R.layout.daysintervals: {
				todays_intervals.setSavedInstanceStates(onState);			
				break;
			}
			case R.layout.addinterval: {
				//finding the necessary information
				String category = 
						((AutoCompleteTextView)findViewById(R.id.category_autocomplete_view))
						.getText().toString();
				
				boolean all_categories = 
						((CheckBox)findViewById(R.id.choose_all_categories)).isChecked();
				boolean minute_interval =
						((CheckBox)findViewById(R.id.is_minute_interval)).isChecked();
				String date =
						((EditText)findViewById(R.id.date_edit_text)).getText().toString();
				
				Spinner temp_item = ((Spinner)findViewById(R.id.item_chooser));
				int item_position = temp_item.getSelectedItemPosition();
				
				//we just save necessary stuff what need to be retained in the fields
				onState.putString(category_static, category);
				onState.putBoolean(all_categories_static, all_categories);
				onState.putBoolean(minute_interval_static, minute_interval);
				onState.putString(date_static, date);
				onState.putInt(spinner_static, item_position);
				
				break;
			}
			case R.layout.additem: {
				AddItemWindow aiw = new AddItemWindow(this);
				aiw.setSavedInstanceStates(onState);
				break;
			}
			
			case R.layout.intervals: {
				//only category has to be preserved
				String category =
						((AutoCompleteTextView)findViewById(R.id.interval_restriction_field))
						.getText().toString();
				
				//then adding it into onstate
				onState.putString(category_static, category);
				
				break;
			}
			
			case R.layout.items: {
				ListAllItemsWindow laiw = new ListAllItemsWindow(this);
				laiw.setSavedInstanceStates(onState);	
				break;
			}
			
			case R.layout.modifyitem: {
				ModifyItemWindow miw = new ModifyItemWindow(this);
				miw.setSavedInstanceStates(onState);
				break;
			}
			
			case R.layout.modify_interval: {
				//finding the interval id
				long interval_id = Long.valueOf(((TextView)findViewById(R.id.modify_interval_interval_id))
						.getText().toString());
				
				//finding date
				String date = ((EditText)findViewById(R.id.modify_interval_date)).getText().toString();
				
				//then putting them into onstate
				//TODO change class cast
				onState.putLong(IntervalNotification.interval_id_static, interval_id);
				onState.putString(date_static, date);	
				
				break;
			}
			
			case R.layout.thingtoremember: {
				ItemViewWindow ivw = new ItemViewWindow(this, null, false);
				ivw.setSavedInstanceStates(onState);		
				break;
			}
			
			case R.layout.categories: {
				//only need to preserve category search text
				String category = 
						((AutoCompleteTextView)findViewById(R.id.category_category_search_field))
						.getText().toString();
				
				//then just adding it into onstate
				onState.putString(category_static, category);
				
				break;
			}
			
			case R.layout.modify_category: {
				//only need to perserve category text...
				String category =
						((EditText)findViewById(R.id.modify_category_name))
						.getText().toString();
				
				long category_id =
						Long.valueOf(((TextView)findViewById(R.id.modify_category_category_id))
						.getText().toString());
				
				
				//the putting it into onstate
				onState.putLong(category_id_static, category_id);
				onState.putString(category_static, category);
				
				break;
			}
		}
	}
	
	//options menu inflater
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.layout.options_menu, menu);
		return true;
	}
	
	//actions to what to do when item on the menu is pressed
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    if (item.getItemId() == R.id.add_item_options) {
			//moving to add items
			moveToAddItems(new View(this));
			return true;
		} else if (item.getItemId() == R.id.exit_options_button) {
			this.stopService(this.getIntent());
			finish();
			return true;
		} else if (item.getItemId() == R.id.all_interval_list) {
			//moving to interval list
			moveToAllIntervals();
			return true;
		} else if (item.getItemId() == R.id.all_item_list) {
			moveToAllItems();
			return true;
		} else if (item.getItemId() == R.id.settings_options_button) {
			moveToSettings();
			return true;
		} else if (item.getItemId() == R.id.todays_intervals_options_button) {
			moveToTodaysIntervals();
			return true;
		} else if (item.getItemId() == R.id.all_categories_list) {
			moveToAllCategories();
			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
	}
			
	private void uiInstances() {
		//loading instances here 'should' be more efficient unless memory becomes a problem
		//then we just move these instances to where they are needed
		
		//loading review (it should be relatively small to keep active all the time)
		//and it is much easier this way
		//review = new ItemViewWindow(this);
				
		//date dialog is small and necessary to keep this way
		date_dialog = new DateDialog(this);
		
		todays_intervals = new TodaysIntervals(this);
		
		
	}
	
	public void otherInstances() {
		//first clearing all notifications because we're in the program now
		//notification manager
		NotificationManager not_mana = (NotificationManager)
				getSystemService(Context.NOTIFICATION_SERVICE);
		
		not_mana.cancelAll();
		
		//also loading minute intervals to memory if necessary
		minute_intervals = MinuteInterval.reactivateMinuteIntervals(this);
		//TODO
		
		//if we haven't got certain preferences already we create them
		SharedPreferences prefs = getSharedPreferences(SETTINGS_FILE,0);
		
		//new editor to edit preferences if they don't exist
		Editor edit = prefs.edit();
		
		if (!prefs.contains(Preferences.DAYS_INTERVALS.toString())) {
			edit.putInt(Preferences.DAYS_INTERVALS.toString(), 100);
		}
		
		if (!prefs.contains(Preferences.DAY_TICK.toString())) {
			edit.putLong(Preferences.DAY_TICK.toString(), 4*1000*60*60);
		}
		
		edit.commit();
		
		//first making new daytick instance
		//which updates all the activities of items
		DayTick r = new DayTick(this);
		//then setting up handler
		Handler daytick = new Handler();
		daytick.postDelayed(r, r.getNextTime());
		
		addMinuteIntervalHandler(daytick);
	}
		
	//*****get functions*****
	//-----------------------
	//***********************
	
	public int getCurrentView () { return current_view;	}
	public Notification getNotification() { return minute_interval_notification; }
	public TodaysIntervals getTodaysIntervals () { return todays_intervals; }
	
	//*****set functions*****
	//-----------------------
	//***********************
	public void setCurrentView (int view_id) { current_view = view_id; }
	public void setNotification(Notification minute_interval_notification) {
		this.minute_interval_notification = minute_interval_notification;
	}
	public void addMinuteIntervalHandler(Handler min_hand) {
		minute_intervals.add(min_hand);
	}
		
	//*****move functions*****
	//------------------------
	//************************
	
	//general move function takes view id as parameter and recognizes 
	//from that where to move
	public void moveToView(int view_id) {
		switch (view_id) {			
			//if we're going to see all intervals
			case R.layout.intervals:  {
				moveToAllIntervals();
				break; 
			}
			
			//if we're going to see todays intervals
			case R.layout.daysintervals: {
				//we pass random view (as we don't need it anyhow)
				moveToTodaysIntervals();
				break;
			}
			
			//if we're going to add intervals
			case R.layout.addinterval: {
				//we pass random view (because we don't need it)
				moveToAddIntervals(findViewById(R.id.add_interval_button));
				break;
			}
			
			//if we're going to add items
			case R.layout.additem: {
				//again random view
				moveToAddItems(findViewById(R.id.add_interval_button));
				break;
			}
						
			//if we're going to see all items
			case R.layout.items:
			{
				moveToAllItems();
				break;
			}
			
			//if we're going to settings
			case R.layout.settings: {
				moveToSettings();
				break;
			}
			
			//if we're going to list all categories
			case R.layout.categories: {
				moveToAllCategories();
				break;
			}
			
			//if we're going to statistics
			case R.layout.statistics: {
				moveToStatistics();
				break;
			}
		}
	}
	
	//button action for moving into todays intervals
	public void moveToTodaysIntervals () {
		SharedPreferences prefs = this.getSharedPreferences(SETTINGS_FILE, 0);
		if (prefs.getBoolean(Preferences.ORIENTATION_LOCK.toString(),
				true)) {
			//making orientation locked to portrait
			if (this.getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE)
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		} else {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
		}
		
		current_view = R.layout.daysintervals;
				
		//we pressed button days intervals button so we just change view
		todays_intervals.setVisible();
		
		//loading its contents
		//today_interval.loadWindowContent();
	}
	
	//button action for moving to add new intervals
	public void moveToAddIntervals (View v) {
		//Creating new add interval window
		AddIntervalWindow add_interval = new AddIntervalWindow(this);
		
		//we just change view into add intervals
		add_interval.setVisible();
		current_view = R.layout.addinterval;
		//the load its content
		add_interval.loadWindowContent();
	}
	
	//button action for moving to create new items
	public void moveToAddItems (View v) {
		//creating add items
		AddItemWindow add_item = new AddItemWindow(this);
		//just changes view to item creation
		add_item.setVisible();
		current_view = R.layout.additem;
		//loading window contents
		add_item.loadWindowContent();
	}
	
	public void moveToAllIntervals() {
		//creating list intervals
		ListAllIntervalsWindow intervals = new ListAllIntervalsWindow(this);
		
		//setting screen visible
		intervals.setVisible();
		
		current_view = R.layout.intervals;
		
		//loading window contents
		intervals.loadWindowContent();
	}
	
	public void moveToAllItems() {
		//creating list items
		ListAllItemsWindow items = new ListAllItemsWindow(this);
		
		//setting screen visible
		items.setVisible();
		
		current_view = R.layout.items;

		//loading window contents
		items.loadWindowContent();
	}
	
	public void moveToReview(ThingToRemember ttr, TodaysTestWindow ttw) {
		//making orientation orientation sensor based
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
		
		current_view = R.layout.thingtoremember;
		
		if (ttw == null) {
			ItemViewWindow review = new ItemViewWindow(this, 
					null, false);
			review.setVisible();
			review.loadWindowContent(ttr);
		} else {
			ttw.show();
		}
	}
	
	public void moveToModifyInterval (Interval interval) {
		//creating modify interval window
		ModifyIntervalWindow modify_interval = new ModifyIntervalWindow(this);
		
		//setting modify interval visible
		modify_interval.setVisible(interval);
		current_view = R.layout.modify_interval;
		//loading contents for the window
		modify_interval.loadWindowContent();
	}
	
	public void moveToModifyItem (ThingToRemember ttr) {
		ModifyItemWindow miw = new ModifyItemWindow(this);
		//setting screen visible
		miw.setVisible(ttr);
		current_view = R.layout.modifyitem;
		//loading contents
		miw.loadWindowContent();
	}
		
	public void moveToSettings() {
		//creating new settings window
		SettingsWindow settings = new SettingsWindow(this);
		
		//setting screen visible
		settings.setVisible();
		
		current_view = R.layout.settings;
		
		//loading window contents
		settings.loadWindowContent();
	}
	
	public void moveToAllCategories () {
		//setting correct screen visible
		ListAllCategoriesWindow cat_win = new ListAllCategoriesWindow(this);
		
		//setting it visible
		cat_win.setVisible();
		current_view = R.layout.categories;
		//loading its contents
		cat_win.loadWindowContent();
	}
	
	public void moveToCategory(Category category) {
		ModifyCategoryWindow mod_cat = new ModifyCategoryWindow(this);
		
		//setting window visible
		mod_cat.setVisible(category);
		
		//assigning current view
		current_view = R.layout.modify_category;
		
		//loading contents for the window
		mod_cat.loadWindowContent();
	}
	
	public void moveToStatistics () {
		StatisticsWindow stats = new StatisticsWindow(this);
		
		//setting window visible
		stats.setVisible();
		
		current_view = R.layout.statistics;
				
		//loading window contents
		stats.loadWindowcontent();
	}
		
	//****click functions****
	//-----------------------
	//***********************
	
	//when view all is clicked we move to all intervals window
	//TODO
	//probably going to items anyhow
	public void menuAllIntervalsClick (View v) {
		moveToView(R.layout.items);
	}
	
	//when categories is clicked on menubar we move to categories
	public void menuCategoryClick (View v) {
		moveToView(R.layout.categories);
	}
	
	//when add item in visible menu is clicked we move to add item
	public void menuAddItemClick (View v) {
		moveToView(R.layout.additem);
	}
	
	//when statistics button is pressed
	public void menuStatisticsClick (View v) {
		moveToView(R.layout.statistics);
	}
	
	//when menu button is pressed again
	public void menuReturnClick(View v) {
		moveToView(R.layout.daysintervals);
	}
			
	/*public void itemDeleteClick(View v) {
		//first deletes the item
		add_item.itemDelete(v);
		//it is necessary to call adapters again
		add_item.loadWindowContent();
	}*/

	public void dateDialogClick (View v) {
		
		//finding minute interval checkbox
		CheckBox micb = (CheckBox) findViewById(R.id.is_minute_interval);
		
		//comparing if it's checked to see if we popup dialog of not
		if (!micb.isChecked()) {	
			if (current_view == R.layout.additem) {
				date_dialog.setVisible(R.id.add_item_iterval_starting_date);
			} else if (current_view == R.layout.modifyitem) {
				date_dialog.setVisible(R.id.modify_item_iterval_starting_date);
			} else {
				//showing the dialog
				date_dialog.setVisible(R.id.date_edit_text);
			}
		}
	}
	
	public void dateDialogModifyIntervalClick(View v) {
		date_dialog.setVisible(R.id.modify_interval_date);
	}
							
	//****Override functions*****
	//---------------------------
	//***************************
	
	//we have to override dialog on creation here
	//seems mandatory
	@Override
	protected Dialog onCreateDialog(int id) {
		// get the current date to date field(s)
        Calendar c = Calendar.getInstance();
		
	    switch (id) {
	    case 0:
	        return new DatePickerDialog(this,
	                    date_dialog.getDatePicker(),
	                    c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
	    }
	    return null;
	}
	
	//when pressing back button we do stuff
	@Override
	public void onBackPressed () {
		//making hierarchy of views
		switch (current_view) {
			//if we're on main window we just exit the program
			case R.layout.daysintervals: {
				this.moveTaskToBack(true);
				break;
			}
			//if we're in any of the lists we return to main window
			//also item creation, interval creation and settings return to main window
			case R.layout.additem:
			case R.layout.addinterval:
			case R.layout.intervals:
			case R.layout.items:
			case R.layout.settings:
			case R.layout.categories :
			case R.layout.thingtoremember: 
			case R.layout.statistics: {
				moveToView(R.layout.daysintervals);
				break;
			}
						
			//if we're modifying an item we move to items window
			case R.layout.modifyitem: {
				moveToView(R.layout.items);
				break;
			}
			
			//if  we're modifying an interval we move to intervals window
			case R.layout.modify_interval: {
				moveToView(R.layout.intervals);
				break;
			}
			
			//if we're modifying category we move back to category list
			case R.layout.modify_category: {
				moveToView(R.layout.categories);
				break;
			}
				
		}
		
		return;
	}
	
	//overriding activity result
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {     
	  super.onActivityResult(requestCode, resultCode, data); 
	  switch(requestCode) {
	  
	  //handling file from the file browser
	  case file_browser_result: { 
	      if ((resultCode == Activity.RESULT_OK) && 
	    		  (current_view == R.layout.additem || 
	    		  current_view == R.layout.modifyitem)) { 
	    	  ListView listview;
	    	  
	    	  if (current_view == R.layout.additem) {
	    		  //finding the list view we want to change
	    		  listview = 
	    				  (ListView)findViewById(R.id.item_list_creation);
	    	  } else {
	    		  //finding the list view we want to change
	    		  listview = 
	    				  (ListView)findViewById(R.id.modify_item_listview);
	    	  }
	    		  
	    	  //getting the the text we got from the activity
	    	  String file = data.getStringExtra(FileBrowserActivity.FILE_IDENTIFIER);
	    		  
	    	  int position = this.getIntent()
	    			  .getExtras()
	    			  .getInt(FileBrowserActivity.INDEX_IDENTIFIER);
	    		   
	    	  //also setting up file type
	    	  ((Data)listview.getItemAtPosition(position)).setContent(
	    			  file);
	    	  ((Data)listview.getItemAtPosition(position)).setType(Datatype.FILE);
	    		  
	    	  //then notifying that soemthing has changed
	    	  ((AddItemAdapter)listview.getAdapter()).notifyDataSetChanged();

	      } 
	      break; 
	    }
	  
	  	//handling image from the camera
		case camera_result: {
			 if ((resultCode == Activity.RESULT_OK) && 
		    		  (current_view == R.layout.additem || 
		    		  current_view == R.layout.modifyitem)) { 
				 ListView listview;
				 if (current_view == R.layout.additem) {
					 //finding the list view we want to change
					 listview = 
							 (ListView)findViewById(R.id.item_list_creation);
				 } else {
					 //finding the list view we want to change
					 listview = 
							 (ListView)findViewById(R.id.modify_item_listview);
				 }
				 
	    		  //getting the the text we got from the activity
	    		  String file;
	    		  file = this.getIntent()
	    					 .getExtras().getString(MediaStore.EXTRA_OUTPUT);
	    		 
	    		  //String file = data.getStringExtra(FileBrowserActivity.FILE_IDENTIFIER);
	    		  
	    		  int position = this.getIntent()
	    				  .getExtras()
	    				  .getInt(FileBrowserActivity.INDEX_IDENTIFIER);
	    		   
	    		  //also setting up file type
	    		  ((Data)listview.getItemAtPosition(position)).setContent(
	    				  file);
	    		  ((Data)listview.getItemAtPosition(position)).setType(Datatype.FILE);
	    		  
	    		  //then notifying that soemthing has changed
	    		  ((AddItemAdapter)listview.getAdapter()).notifyDataSetChanged();
	    		  
	    	  }
	    	  break;
		}
	  } 
	}
	
	//*****other functions*****
	//-------------------------
	//*************************
	
	
	//*****variables*****
	//-------------------
	//*******************
	
	//views for back button
	//List<Integer> views = new ArrayList<Integer>();
	
	int current_view = 0;
	
	//ui instances
	//ItemViewWindow review;

	//ModifyItemWindow modify_item;
	DateDialog date_dialog;
	
	Notification minute_interval_notification;
	
	TodaysIntervals todays_intervals;
	
	List<Handler> minute_intervals;

	/*MainWindow main_win;
	TodaysIntervals today_interval;
	AddIntervalWindow add_interval;
	

	ListAllIntervalsWindow intervals;
	ListAllItemsWindow items;
	ModifyIntervalWindow modify_interval;
	
	AddItemWindow add_item;
	*/
}	

