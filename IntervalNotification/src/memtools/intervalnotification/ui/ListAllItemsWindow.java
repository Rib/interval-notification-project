package memtools.intervalnotification.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import memtools.intervalnotification.IntervalNotification;
import memtools.intervalnotification.R;
import memtools.intervalnotification.Interface.InterfaceImplementation;
import memtools.intervalnotification.Libs.Category;
import memtools.intervalnotification.Libs.CategoryAdapter;
import memtools.intervalnotification.Libs.ListItemAdapter;
import memtools.intervalnotification.Libs.ThingToRemember;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class ListAllItemsWindow {
	public final static String category_static = "CATEGORY";
	
	//context to which this is attached
	Context context;
	
	//items list view
	ListView item_listview;
	//category search field
	AutoCompleteTextView actv;
	//search button
	ImageView search_button;
	//non intervals checkbox
	CheckBox non_intervals;
	
	//value if adapter needs to be updated
	boolean update_adapter = false;
	
	//lists
	//category list
	List<Category> categories;
	//items list
	List<ThingToRemember> items;
	//temporaty items for regexp
	List<ThingToRemember> temp_items;
	
	//adapters
	//category adapter
	ArrayAdapter<Category> category_adapter;
	//items adapter
	BaseAdapter items_adapter;
	
	//interface declaration
	InterfaceImplementation iface;
	
	public ListAllItemsWindow (IntervalNotification context) {
		this.context = context;
		
		//interface init
		iface = new InterfaceImplementation(this.context);
	}
	
	//sets this window visible
	public void setVisible() {
		((IntervalNotification) context).setContentView(R.layout.items);
	}
	
	public void loadWindowContent() {
		long mikros = System.currentTimeMillis();
		findFields();
		Log.v("List items: findffields", ""+(System.currentTimeMillis()-mikros));
		mikros = System.currentTimeMillis();
		
		//getting all categories
		categories = iface.listAllCategories();
		Log.v("List items: categories", ""+(System.currentTimeMillis()-mikros));
		mikros = System.currentTimeMillis();
		
		//gets all items
		items = iface.listItems("", true);
		Log.v("List items: listitems", ""+(System.currentTimeMillis()-mikros));
		mikros = System.currentTimeMillis();
	
		//updating most recent data to items
		/*for (int i = 0; i < items.size(); ++i) {
			iface.updateTTRInfo(items.get(i));
		}*/
		
		//finally loading all non db contents
		loadNonDBContent();
		Log.v("List items: loadnondbcontent", ""+(System.currentTimeMillis()-mikros));
		mikros = System.currentTimeMillis();
	}
	
	public void setSavedInstanceStates (Bundle states) {
		findFields();
		
		//then just adding it into onstate
		states.putString(category_static, actv.getText().toString());
	}
	
	public void loadSavedInstanceStates (Bundle states) {
		if (states.containsKey(category_static)) {
			findFields();
			actv.setText(states.getString(category_static));
		}
	}
		
	//loads contents that have nothing to do with interface
	private void loadNonDBContent () {
		findFields();
		
		if (item_listview == null) return;
		
		//setting categories adapter
		category_adapter = new CategoryAdapter (this.context, categories);
		
		//setting threshold to 1
		actv.setThreshold(1);
		//setting adapter
		actv.setAdapter(category_adapter);
		
		//cuts off stuff accorting to search field
		//temp_items = RegexpSelection(items, actv.getText().toString());
		if (items_adapter == null || update_adapter) {
			//setting items adapter
			items_adapter = new ListItemAdapter(this.context, items);
			//((ListItemAdapter) items_adapter).getFilter().filter(actv.getText().toString());
			item_listview.setAdapter(items_adapter);
		}
		
		//adding on item click listener
		actv.setOnItemClickListener(new OnItemClickListener() {  
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				((ListItemAdapter) items_adapter).getFilter()
				.filter(actv.getText().toString());
				
				InputMethodManager imm = (InputMethodManager)
						context.getSystemService(
					      Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(actv.getWindowToken(), 0);
			}
		});
		
		//setting listener to update regexp
		actv.setOnKeyListener(new OnKeyListener() {

			public boolean onKey(View arg0, int arg1, KeyEvent arg2) {
				((ListItemAdapter) items_adapter).getFilter()
				.filter(actv.getText().toString());
				return false;
			}		
		});
		
		search_button.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					((ImageView)v).setImageResource(R.drawable.search_button_down);
				} else if (event.getAction() == MotionEvent.ACTION_UP) {
					((ImageView)v).setImageResource(R.drawable.search_button_up);
				}
				return false;
			}
			
		});
		
		search_button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				((ListItemAdapter) items_adapter).getFilter()
				.filter(actv.getText().toString());
				
				InputMethodManager imm = (InputMethodManager)
						context.getSystemService(
					      Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(actv.getWindowToken(), 0);
			}
			
		});
				
		//setting done button action
		actv.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE || 
					actionId == EditorInfo.IME_ACTION_SEARCH) {
					((ListItemAdapter) items_adapter).getFilter()
					.filter(actv.getText().toString());
					
					InputMethodManager imm = (InputMethodManager)
							context.getSystemService(
						      Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(actv.getWindowToken(), 0);
						
					return true;
				}

				return false;
			}
			
		});
		
		non_intervals.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				((ListItemAdapter) items_adapter).getFilter()
				.filter(actv.getText().toString());
				
				InputMethodManager imm = (InputMethodManager)
						context.getSystemService(
					      Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(actv.getWindowToken(), 0);
			}
			
		});
	}
	
	private void findFields() {
		//item list view
		item_listview = (ListView) 
				((IntervalNotification) 
						context).findViewById(R.id.item_list_view);
		
		//category search field
		actv = (AutoCompleteTextView) 
						((IntervalNotification) 
								context).findViewById(R.id.item_restriction_field);
		
		//search button
		search_button = (ImageView)
				((IntervalNotification)
						context).findViewById(R.id.items_search_button);
		
		//non intervals checkbox
		non_intervals = (CheckBox)
				((IntervalNotification)
						context).findViewById(R.id.items_non_intervals);
	}
	
	private List<ThingToRemember> RegexpSelection 
	(List<ThingToRemember> text, String keyword) {
		//list that is to be returned
		List<ThingToRemember> return_list =  new ArrayList<ThingToRemember>();
		
		//if we have backslash we remove it from the string (ignore!)
		keyword = keyword.replace('\\', ' ');
		
		//if there is some keyword
		if (keyword.length() > 0) {
			//creating regexp of the form
			// \bkeyword
			StringBuilder sb = new StringBuilder("\\b").append(keyword);
			
			//going through the list
			for (int i = 0; i < text.size(); ++i) {
				String temp_info = text.get(i).getInfob().getCategory() + " " + 
						text.get(i).getInfob().getInfo();
								
				Matcher m = 
						Pattern.compile(sb.toString()).matcher(temp_info);
				
				if (m.find()) {
					return_list.add(text.get(i));
				}
			}
		}
		//if we dont have search word we return original list
		else {
			return text;
		}
		
		return return_list;
	}
}
