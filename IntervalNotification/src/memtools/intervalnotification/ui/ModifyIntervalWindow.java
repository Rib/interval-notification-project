package memtools.intervalnotification.ui;

import java.text.DateFormat;
import java.util.Date;

import memtools.intervalnotification.IntervalNotification;
import memtools.intervalnotification.R;
import memtools.intervalnotification.Interface.InterfaceImplementation;
import memtools.intervalnotification.Libs.Interval;
import memtools.intervalnotification.Libs.Intervaltype;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ModifyIntervalWindow {
	Context context;
	
	//fields
	TextView category_text;
	TextView info_text;
	EditText date;
	TextView length_text;
	Button save_button;
	Button delete_button;
	
	//interval this window is associated with
	Interval interval;
	
	//interface
	InterfaceImplementation iface;
	
	public ModifyIntervalWindow (IntervalNotification context) {
		this.context = context;
		//making instance of interface
		iface = new InterfaceImplementation(this.context);
	}
	
	public void setVisible (Interval interval) {
		((IntervalNotification) context).setContentView(R.layout.modify_interval);
		
		if (interval == null) return;
		
		this.interval = interval;
		
		if (interval.getTTR().getInfob() == null) {
			iface.removeInterval(interval);
			((IntervalNotification)context).moveToAllIntervals();
		}
		
		iface.updateTTRInfo(interval.getTTR());
	}
	
	public void loadWindowContent () {
		//setting interval id to hidden field
		((TextView)((IntervalNotification)context)
		.findViewById(R.id.modify_interval_interval_id))
		.setText(((Long)interval.getId()).toString());

		//finding all fields
		findFields();
		
		//loading category
		category_text.setText(this.interval.getTTR().getInfob().getCategory());
		
		//loading info
		info_text.setText(this.interval.getTTR().getInfob().getInfo());
		
		DateFormat df = DateFormat.getDateInstance();
		date.setText(df.format(this.interval.getNextDate().getTime()));
		
		//loading interval length
		length_text.setText(R.string.previous_interval_lenght);
		String temp = length_text.getText().toString() + " " + String.valueOf(this.interval.getLength());
		if (this.interval.getType() == Intervaltype.DAY) {
		length_text.setText(R.string.days);
		} else {
			length_text.setText(R.string.minutes);
		}
		
		temp += " " + length_text.getText();
		length_text.setText(temp);
		
		
		//setting save buttons onclick listener
		save_button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				//at the moment we know only next date can be changed
				getInterval().setNextDate((new Date(date.getText().toString())).getTime());
				
				//using 1 as grade nothing is automatically changed
				iface.updateInterval(new Double(0), getInterval(), false);
				((IntervalNotification)context).moveToAllIntervals();
				
				//setting up message when this is called
				String cs = getInterval().getTTR().getInfob().getInfo();
				cs += " " + context.getString(R.string.was_modified);
				
				//setting up time this notification is displayed
				int duration = Toast.LENGTH_SHORT;
				
				Toast toast = Toast.makeText(context, cs, duration);
				toast.show();
			}
			
		});
		
		//setting delete button onclick action
		
		delete_button.setOnClickListener(new OnClickListener () {

			@Override
			public void onClick(View arg0) {
				//setting up message when this is called
				final String cs = getInterval().getTTR().getInfob().getInfo()+ 
						" " + context.getString(R.string.was_deleted);
				
				AlertDialog.Builder builder = new AlertDialog.Builder(context);
				builder.setMessage(context.getString(R.string.delete) + " " + 
						getInterval().getTTR().getInfob().getInfo())
						.setCancelable(false)
						.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				           public void onClick(DialogInterface dialog, int id) {
								//setting up time this notification is displayed
								int duration = Toast.LENGTH_SHORT;
												
								iface.removeInterval(getInterval());
								
								//moving back to intervals
								((IntervalNotification) context).moveToAllIntervals();
								
								Toast toast = Toast.makeText(context, cs, duration);
								toast.show();
				                dialog.cancel();
				           }
				       })
				       .setNegativeButton("No", new DialogInterface.OnClickListener() {
				           public void onClick(DialogInterface dialog, int id) {
				                dialog.cancel();
				           }
				       });
					
				AlertDialog alert = builder.create();
				
				alert.show();
			}
		});
	}
	
	private Interval getInterval() { return interval; }
	
	//finds all thefields this window needs
	private void findFields() {
		//category text
		category_text = (TextView) ((IntervalNotification)context).findViewById(
				R.id.modify_interval_category_text);
		
		//info text
		info_text = (TextView) ((IntervalNotification)context).findViewById(
				R.id.modify_interval_item_text);
		
		//date
		date = (EditText) ((IntervalNotification)context).findViewById(
				R.id.modify_interval_date);
		
		//length of interval
		length_text = (TextView) ((IntervalNotification)context).findViewById(
				R.id.modify_interval_previous_length);
		
		//save button
		save_button = (Button)((IntervalNotification)context).findViewById(
				R.id.modify_interval_save_button);
		
		//delete button
		delete_button = (Button)((IntervalNotification)context).findViewById(
				R.id.modify_interval_delete_button);
		
	}
}
