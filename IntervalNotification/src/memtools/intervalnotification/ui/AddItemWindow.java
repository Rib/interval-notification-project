package memtools.intervalnotification.ui;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import memtools.intervalnotification.IntervalNotification;
import memtools.intervalnotification.R;
import memtools.intervalnotification.Interface.InterfaceImplementation;
import memtools.intervalnotification.Libs.AddItemAdapter;
import memtools.intervalnotification.Libs.Category;
import memtools.intervalnotification.Libs.Data;
import memtools.intervalnotification.Libs.Datatype;
import memtools.intervalnotification.Libs.Interval;
import memtools.intervalnotification.Libs.Intervaltype;
import memtools.intervalnotification.Libs.MinuteInterval;
import memtools.intervalnotification.Libs.ThingToRemember;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

public class AddItemWindow {
	//constant strings
	public static final String category_static = "CATEGORY";
	public static final String item_list_static = "ITEM_LIST";
	public static final String item_typelist_static = "ITEM_TYPELIST";
	public static final String add_as_interval_static = "ADD_AS_INTERVAL";
	public static final String is_minute_interval_static = "IS_MINUTE_INTERVAL";
	public static final String date_static = "DATE";
	
	Context context;
	
	//using interface operations
	InterfaceImplementation iface;
	
	//items that are visible in the window atm
	List<Data> items = new ArrayList<Data>();
	
	String category_text = "";
	boolean adding_interval = true;
	boolean is_minute_interval = false;
	String date = "";
	
	//adapter
	AddItemAdapter item_adapter;
		
	//listview stuff
	ListView listview;
	
	//save button
	ImageView save_button;
			
	//constructor
	public AddItemWindow (IntervalNotification context) {
		this.context = context;
		
		iface = new InterfaceImplementation(this.context);
	}
	
	//sets add item window visible
	public void setVisible () {
		((IntervalNotification) context).setContentView(R.layout.additem);
		
		//loading defaults when we show this view again
		defaults();
	}
	
	//load content for this window
	public void loadWindowContent () {
		if (listview == null || save_button == null)
		findFields();
		
		// First parameter - Context
		// Second parameter - All items in list
		// Third parameter - this window "id"
		if (item_adapter == null) {
			item_adapter = new AddItemAdapter (this.context, items, 0, category_text, 
				adding_interval, is_minute_interval, date);

			//assigning adapter to listview
			listview.setAdapter(item_adapter);
		}
				
		save_button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				String cs = listview.getAdapter().getCount()-1 > 0 ? 
						((Data)listview.getAdapter().getItem(0)).getContent() + " " : 
						context.getString(R.string.empty_item) + " ";
				cs += context.getString(R.string.was_added);
				
				//saving the item
				itemSave();
				//loading the window contents again
				loadWindowContent();
				
				//setting up time this notification is displayed
				int duration = Toast.LENGTH_SHORT;
											
				Toast toast = Toast.makeText(context, cs, duration);
				toast.show();
			}
			
		});
		
		save_button.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					((ImageView)v).setImageResource(R.drawable.save_button_down);
				} else if (event.getAction() == MotionEvent.ACTION_UP ||
						event.getAction() == MotionEvent.ACTION_CANCEL ||
						event.getAction() == MotionEvent.ACTION_OUTSIDE) {
					((ImageView)v).setImageResource(R.drawable.save_button_up);
				}
				return false;
			}
			
		});
		
		listview.setSelection(listview.getAdapter().getCount()-1);
	}
	
	//loads items from lists
	public void loadItems(List<String> content, List<String> type) {
		if (content.size() == type.size()) {
			int count = content.size();
			items.clear();
			for (int i = 0; i < count; ++i) {
				Data data = new Data();
				data.setContent(content.get(i));
				data.setType(Datatype.valueOf(type.get(i)));
				items.add(data);
			}
		}
	}
	
	public void itemSave () {	
		updateItems();
		
		//finally adding it through interface
		//making new thing to remember
		ThingToRemember tempdata = iface.addItem(items, iface.newTTR());
		
		//checking if we have new category
		//Recognizing stuff from category
		String category_text = ((AddItemAdapter)listview.getAdapter()).getCategory();
		
		Category category = new Category();
		//just making sure we have some category
		if (!category_text.isEmpty()) {
			category = iface.addCategory(category_text);
		}
				
		//here we make sure we have the same text (should always be so)
		//and linking category to thing to remember
		if (!category_text.isEmpty() && category.getName().equals(category_text)) {
			//keeping the old category intact
			//joining category to ttr
			iface.setCategoryTTR((int)category.getId(), 
					(int)tempdata.getId());
		}
		
		//checking we also add interval with it
		if (((AddItemAdapter)listview.getAdapter()).addingInterval()) {
			saveAsInterval(tempdata);
			//if we have a chance of change in todays intervals we update
			//content of todays intervals
			((IntervalNotification)context)
			.getTodaysIntervals().updateContent();
		}
		
		//loading defaults
		defaults();
		//and finally loading window contents again
		loadWindowContent();
	}
	
	public void updateItems() {
		//updating items :)
		items = ((AddItemAdapter)listview.getAdapter()).getItems();
		
		category_text = ((AddItemAdapter)listview.getAdapter()).getCategory();
		adding_interval = ((AddItemAdapter)listview.getAdapter()).addingInterval();
		is_minute_interval = ((AddItemAdapter)listview.getAdapter()).isMinuteInterval();
		date = ((AddItemAdapter)listview.getAdapter()).getDate();
	}
	
	public void setStates(String category_text, boolean adding_interval, 
			boolean is_minute_interval, String date) {
		this.category_text = category_text;
		this.adding_interval = adding_interval;
		this.is_minute_interval = is_minute_interval;
		this.date = date;
	}
	
	public void setSavedInstanceStates (Bundle states) {
		findFields();
		
		//finding necessary information
		String category = ((AddItemAdapter)listview.getAdapter()).getCategory();
		
		boolean add_as_interval = ((AddItemAdapter)listview.getAdapter()).addingInterval();
		
		//creating the list
		ArrayList<String> items_content = new ArrayList<String>();
		ArrayList<String> items_type = new ArrayList<String>();
		for(int i = 0; i < listview.getAdapter().getCount()-2; ++i) {
			items_content.add(((Data)listview.getItemAtPosition(i)).getContent());
			items_type.add(((Data)listview.getItemAtPosition(i)).getType().toString());
		}
		
		//then put them into states
		states.putString(category_static, category);
		states.putStringArrayList(item_list_static, items_content);
		states.putStringArrayList(item_typelist_static, items_type);
		states.putBoolean(add_as_interval_static, add_as_interval);
		//if we have checked add as interval box we send some extra
		if (add_as_interval) {
			boolean is_minute_interval = ((AddItemAdapter)listview.getAdapter()).isMinuteInterval();
			String date = ((AddItemAdapter)listview.getAdapter()).getDate();
			
			states.putBoolean(is_minute_interval_static, is_minute_interval);
			states.putString(date_static, date);
		}
	}
	
	public void loadSavedInstanceStates (Bundle states) {
		if (states.containsKey(category_static) &&
				states.containsKey(item_list_static) &&
				states.containsKey(item_typelist_static) &&
				states.containsKey(add_as_interval_static)) {
			
			//finds all necessary fields (and more ^^)
			findFields();
			
			category_text = states.getString(category_static);

			loadItems(states.getStringArrayList(item_list_static),
					states.getStringArrayList(item_typelist_static));
			
			if (states.getBoolean(add_as_interval_static)) {
				adding_interval = true;
				//checking if we hold all necessary keys we need to reconstruct view
				if (states.containsKey(is_minute_interval_static) &&
						states.containsKey(date_static)) {
					is_minute_interval = states.getBoolean(is_minute_interval_static);
					date = states.getString(date_static);
				}
			} else {
				adding_interval = false;
			}
		}
		
		loadWindowContent();
	}
	
	private void saveAsInterval (ThingToRemember ttr) {
		//first finding fields (probably not necessary but doesn't cost much)
		findFields();
		//getting date
		Date temp_date;
		Intervaltype  temp_type;
		int temp_length = 1;
		
		//checking if we have minute interval or not
		if (!item_adapter.isMinuteInterval()) {
			DateFormat df = DateFormat.getDateInstance();
			//interpreting date
			try {
				temp_date = df.parse(item_adapter.getDate());
			} catch (ParseException e) {
				Calendar cal = Calendar.getInstance();
				temp_date = cal.getTime();
			}
			//type is normal day interval
			temp_type = Intervaltype.DAY;
			//setting length to 1
			temp_length = 1;
		} else {
			//getting today
			Calendar calendar = Calendar.getInstance();
			//getting current date
			temp_date = calendar.getTime();
			//making type minute interval
			temp_type = Intervaltype.MINUTE;
			//we have length too
			try {
				temp_length = Integer.parseInt(item_adapter.getDate());
			} catch (NumberFormatException e) {
				temp_length = 1;
			}
		}
		
		//making sure we have correct value in selected thing to remember
		//interval id (hence it should be -1)
		ttr.setIntervalId(-1);
		
		//finally calling interface to create our interval
		Interval temp_interval = iface.addInterval(temp_length, 
				temp_date, ttr, temp_type);
		
		//if we have minute interval we make new handler to make sure 
		//we get notifications on time
		if (temp_interval.getType().equals(Intervaltype.MINUTE)) {
			//we put handler up for this minute interval
			//TODO check that ttr is ok
			MinuteInterval mi = new MinuteInterval(this.context, ttr);
			/*AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			mi.makePendingIntent();
			am.set(AlarmManager.ELAPSED_REALTIME, 
					SystemClock.elapsedRealtime()+mi.getNextDelay(), mi.pendingIntent());*/
			//setting handler
			Handler handler = new Handler();
			//setting delay
			handler.postDelayed(mi, mi.getNextDelay());
		}
		
		//updating interval activities hence we just added interval
		iface.updateIntervalActivities();
	}
	
	//finding all necessary fields
	private void findFields() {
		//finding the list view we want to show
		listview = (ListView) 
				((IntervalNotification) 
						context).findViewById(R.id.item_list_creation);
				
		//finding save button
		save_button = (ImageView)
				((IntervalNotification) context).findViewById(
						R.id.save_item_button);
	}
	
	//loads some default stuff
	private void defaults () {		
		//then clearing unnecessary stuff
		items.clear();
		
		Data tmp = new Data();
		Data tmp2 = new Data();
		tmp.setContent("");
		tmp2.setContent("");
		tmp.setType(Datatype.TEXT);
		tmp2.setType(Datatype.TEXT);
		//add few empty items
		items.add(tmp);
		items.add(tmp2);
		
		//nullifying adapter so it get updated too
		item_adapter = null;
	}
}
