package memtools.intervalnotification.ui;

import memtools.intervalnotification.IntervalNotification;
import memtools.intervalnotification.R;
import memtools.intervalnotification.Interface.InterfaceImplementation;
import memtools.intervalnotification.Libs.Category;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ModifyCategoryWindow {
	Context context;
	
	//category we're modifying
	Category category;
	
	//fields
	EditText category_field;
	Button save_button;
	
	InterfaceImplementation iface;
	
	public ModifyCategoryWindow(Context context) {
		this.context = context;
		
		iface = new InterfaceImplementation(this.context);
	}
	
	public void setVisible(Category category) {
		((IntervalNotification)context).setContentView(R.layout.modify_category);
		
		this.category = category;
		//setting id field
		((TextView)(((IntervalNotification)context)
				.findViewById(R.id.modify_category_category_id)))
				.setText(String.valueOf(category.getId()));
	}
	
	public void loadWindowContent() {
		//first finding fields
		findFields();
		
		//setting edit texts text
		category_field.setText(category.getName());
		
		//setting onclicklisterner for save button
		save_button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				category.setName(category_field.getText().toString());
				
				//just saving this into database and loading window contents again
				iface.updateCategory(category);
				loadWindowContent();
			}
			
		});
	}
	
	private void findFields() {
		//finding category field
		category_field = (EditText)
				((IntervalNotification)context).findViewById(R.id.modify_category_name);
		
		//finding save button
		save_button = (Button)
				((IntervalNotification)context).findViewById(R.id.modify_category_save_button);
	}
}
