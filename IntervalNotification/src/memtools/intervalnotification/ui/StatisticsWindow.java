package memtools.intervalnotification.ui;

import memtools.intervalnotification.IntervalNotification;
import memtools.intervalnotification.R;
import memtools.intervalnotification.Graphs.TotalGraph;
import memtools.intervalnotification.Interface.InterfaceImplementation;
import memtools.intervalnotification.Libs.CompoundStatistics;

import org.achartengine.ChartFactory;

import android.content.Context;
import android.content.Intent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.TextView;

public class StatisticsWindow {
	Context context;
	
	//necessary text fields
	TextView success_rate_field;
	TextView success_rate_100;
	TextView reviews;
	TextView week_reviews;
	
	ImageView total_graph_button;
	
	InterfaceImplementation iface;
	
	public StatisticsWindow (Context context) {
		this.context = context;
		
		iface = new InterfaceImplementation(this.context);
	}
	
	public void setVisible() {
		((IntervalNotification) context).setContentView(R.layout.statistics);
	}
	
	public void loadWindowcontent() {
		//first finding fields
		findFields();
		
		final CompoundStatistics stats = iface.getStatistics();
	    
	    long l = (int)Math.round(stats.success_rate * 100); // truncates  
	    
		success_rate_field.setText(String.valueOf(l)+"%");
		
		l = (int)Math.round(stats.success_rate_100 * 100);
		
		success_rate_100.setText(String.valueOf(l)+"%");
		
		reviews.setText(((Integer)stats.reviews).toString());
		
		week_reviews.setText(((Integer)stats.days7_reviews).toString());
		

		
		total_graph_button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				TotalGraph tg = new TotalGraph(stats);
				
				Intent intent = ChartFactory.getBarChartIntent(context, 
						tg.getDataset(), tg.getRenderer(), null);
				
				context.startActivity(intent);
				
				/*ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(
						LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				
				GraphicalView time_view = ChartFactory.getTimeChartView(context, 
						getDateDemoDataset(), getDemoRenderer(), null);
				
				time_view.setLayoutParams(params);
				
				((LinearLayout)success_rate_field.getParent().getParent())
				.addView(time_view);*/
			}
			
		});
		
		total_graph_button.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					((ImageView)v).setImageResource(R.drawable.total_graph_button_down);
				} else if (event.getAction() == MotionEvent.ACTION_UP ||
						event.getAction() == MotionEvent.ACTION_OUTSIDE ||
						event.getAction() == MotionEvent.ACTION_CANCEL) {
					((ImageView)v).setImageResource(R.drawable.total_graph_button_up);
				}
				return false;
			}
			
		});
	}
	
	private void findFields() {
		//success rate
		success_rate_field = (TextView)
				((IntervalNotification) context)
				.findViewById(R.id.statistics_success_rate);
				
		success_rate_100 = (TextView)
				((IntervalNotification) context)
				.findViewById(R.id.statistics_success_rate_100);
		
		reviews = (TextView)
				((IntervalNotification)context)
				.findViewById(R.id.statistics_total_reviews);
		
		week_reviews = (TextView)
				((IntervalNotification)context)
				.findViewById(R.id.statistics_last_7_days_reviews);
		
		total_graph_button = (ImageView)
				((IntervalNotification)context)
				.findViewById(R.id.statistics_total_graph_button);
	}
}
