package memtools.intervalnotification.ui;

import java.util.Calendar;
import java.util.List;

import memtools.intervalnotification.IntervalNotification;
import memtools.intervalnotification.R;
import memtools.intervalnotification.Interface.InterfaceImplementation;
import memtools.intervalnotification.Libs.CompoundStatistics;
import memtools.intervalnotification.Libs.Data;
import memtools.intervalnotification.Libs.Interval;
import memtools.intervalnotification.Libs.Preferences;
import memtools.intervalnotification.Libs.Repetition;
import memtools.intervalnotification.Libs.ReviewContent;
import memtools.intervalnotification.Libs.ThingToRemember;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

public class ItemViewWindow {
	//constants needed to restore this window
	public static final String ttr_id_static = "INTERVAL_ID";
	public static final String data_index_static = "DATA_INDEX";
	public static final String test_static = "IS_TEST";
	public static final String orientation_static = "ORIENTATION";
	
	Context context;
	
	TodaysTestWindow ttw;
	boolean no_review;
	
	//interval we currently go through
	ThingToRemember this_ttr;
	//Interval this_interval;
	//stores all items from this_item to this list
	List<Data> item_data;
	//is the current index of the window
	int data_index;
		
	//views
	TextView header_text;
	ViewFlipper flipper;
	RelativeLayout rating_content;
	TextView ttr_id;
	TextView data_index_field;
	TextView category_text_field;
	ImageView next;
	ImageView back;
	ImageView no_review_button;
	ImageView review_failed_button;
	ImageView review_success_button;
	RelativeLayout review_field;
	
	//interface
	InterfaceImplementation iface;
	
	float lastX;
		
	public ItemViewWindow (Context context, TodaysTestWindow ttw, boolean no_review) {
		this.context = context;
		
		iface = new InterfaceImplementation(this.context);
		
		data_index = 1;
		
		this.ttw = ttw;
		this.no_review = no_review;
	}
	
	public void setVisible () {
		((IntervalNotification) context).setContentView(R.layout.thingtoremember);
	}
	
	public void loadWindowContent (ThingToRemember ttr) {
		//first finding views
		findViews();
		
		ttr_id.setText(String.valueOf(ttr.getId()));
		iface.updateTTRInfo(ttr);
		//assigning item and interval
		//this_interval = iface.getInterval(interval.getId(),false);
		this_ttr = ttr;
		
		//updating ttr info
		//iface.updateTTRInfo(this_interval.getTTR());
		
		//getting the data list from the interface
		item_data = iface.getTTRData(ttr);
		//item_data = iface.getTTRData(this_interval.getTTR());
		
		//initializing data_index, because we come from somewhere else to this everytime
		data_index = Integer.valueOf(data_index_field.getText().toString());
			
		loadNonDBContent();
			
	}
	
	public void downAction(MotionEvent touchevent) {
		lastX = touchevent.getX();
	}
	
	public void upAction(MotionEvent touchevent) {
		float currentX = touchevent.getX();

		//if we're going to right direction
		if (lastX < currentX) {
			Back();

		} else if (lastX > currentX) {
			Next();
		}
	}
	
	public void Next () {
		findViews();
		//if we're not in last slot we move screen to next
		if (item_data != null && data_index < item_data.size()) {
			flipper.setInAnimation(AnimationUtils.loadAnimation(this.context, R.anim.push_left_in));
			flipper.setOutAnimation(AnimationUtils.loadAnimation(this.context, R.anim.push_right_out));
			flipper.showNext();
			++data_index;
			loadNonDBContent();
			data_index_field.setText(String.valueOf(data_index));
		}
		
	}
	
	public void Back() {
		findViews();
		//if we're not in first slot we move screen to previous
		if (data_index > 1) {			
			flipper.setInAnimation(AnimationUtils.loadAnimation(this.context, R.anim.push_right_in));
			flipper.setOutAnimation(AnimationUtils.loadAnimation(this.context, R.anim.push_left_out));
			flipper.showPrevious();
			--data_index;
			loadNonDBContent();
			data_index_field.setText(String.valueOf(data_index));
		}
	}
	
	//this sets all the necessary parameters to saved instance state
	public void setSavedInstanceStates (Bundle states) {
		//finding necessary fields
		findViews();

		//finding the item we're reviewing
		long ttr_id_int = Long.valueOf(ttr_id.getText().toString());
		
		int data_index = Integer.valueOf(data_index_field.getText().toString());
		
		//then putting it into onstate
		states.putLong(ttr_id_static, ttr_id_int);
		states.putInt(data_index_static, data_index);
		states.putSerializable(test_static, ttw);
		states.putInt(orientation_static, ((IntervalNotification) context).getRequestedOrientation());
		
	}
	
	//this loads all necessary stuff when on state is called
	public void loadSavedInstanceStates (Bundle states) {
		if (states.containsKey(ttr_id_static) &&
				states.containsKey(data_index_static)) {
			long ttr_id = states.getLong(ttr_id_static);
			
			setVisible();
			findViews();
			
			data_index_field.setText(
					String.valueOf(states.getInt(data_index_static)));
			//making sure we don't load anything weird
			if (ttr_id >= 0)
			loadWindowContent(iface.getTTR(ttr_id));
			
			ttw = (TodaysTestWindow) states.getSerializable(test_static);
		}
	}
	
	//views/sets the contents of the window
	private void loadData() {
		
		findViews();
		
		ReviewContent review_content = 
				new ReviewContent(context, item_data.get(data_index-1), this_ttr, ttw, no_review);
		
		//flipper.removeAllViews();
		if (flipper.getChildCount() == 0) {
			review_field = (RelativeLayout)review_content.getView(review_field, true);
			RelativeLayout review_field2 = null;
			review_field2 = (RelativeLayout)review_content.getView(review_field2, true);
			flipper.addView(review_field);
			flipper.addView(review_field2);
		}
		else {
			review_content.getView(flipper.getChildAt((data_index-1)%2), true);	
		}
		
	}
	
	public void setReviewed(boolean rate) {
		//TODO
		//make sure ratings make sense
		SharedPreferences prefs = context.getSharedPreferences(IntervalNotification.SETTINGS_FILE,
				0);
		
		double success = prefs.getFloat(IntervalNotification.SUCESS_FACTOR, 
				(float) Math.exp(1));
		double failure = prefs.getFloat(IntervalNotification.FAILURE_FACTOR, 
				(float) (1/Math.sqrt(2)));
		
		//TODO
		//have to think about efficiency...
		//doing continuous success rate calculations
		CompoundStatistics stats = iface.getStatistics();
		Editor edit = prefs.edit();
		if (stats.success_rate_100 < 
				prefs.getFloat(Preferences.MINIMUM_SUCCESS_RATE.toString(), 0.4f)) {
			edit.putFloat(IntervalNotification.SUCESS_FACTOR, 
					(float) (1.1+((success-1.1)*0.99)));
			success = (float) (1.1+((success-1.1)*0.99));
		} else if (stats.success_rate_100 > prefs.getFloat(
				Preferences.TARGET_SUCCESS_RATE.toString(),
				0.8f)) {
			edit.putFloat(IntervalNotification.SUCESS_FACTOR, 
					(float) (1.1+((success-1.1)*1.1)));
			success = (float) (1.1+((success-1.1)*1.1));
		}
		
		edit.commit();
		
		double grade = rate ? success : failure;
		
		Log.v("Grade...", ""+grade);
		
		if (grade > 0) {
			//first getting timestamp (in milliseconds ;))
			long timestamp = ((Calendar)Calendar.getInstance()).getTimeInMillis();
						
			//finally creating new repetition
			Repetition rep = new Repetition();
			
			//assigning repetition values
			rep.setTimestamp(timestamp);
			rep.setSuccessful(rate);
			Interval temp_interval = iface.getInterval(this_ttr.getIntervalId(), false);
			
			if (temp_interval.getId() == -1) {
				this_ttr = iface.getTTR(this_ttr.getId());
				temp_interval = iface.getInterval(this_ttr.getIntervalId(), false);
			}
			
			rep.setInterval(temp_interval);
			//TODO research interval existence
			iface.updateStatistics(rep);
			
			//then finally updating the interval
			iface.updateInterval(grade, temp_interval,true);
		}
		
		iface.updateTTRInfo(this_ttr);
		//setting up message when this is called
		String cs = this_ttr.getInfob().getInfo();
		cs += " " + context.getString(R.string.was_reviewed);
		
		//setting up time this notification is displayed
		int duration = Toast.LENGTH_SHORT;
		
		Toast toast = Toast.makeText(context, cs, duration);
		toast.show();
		
		((IntervalNotification)context).getTodaysIntervals().updateContent();
	}
	
	private void loadNonDBContent() {
		if (item_data != null && item_data.size() > 0) {
			
	//making sure we don't over- or underflow
			if (data_index < 1) data_index = 1;
			data_index = ((data_index-1) % item_data.size()) + 1;
			//first getting the header
			header_text.setText(data_index + "/" + item_data.size());
			
			category_text_field.setText(this_ttr.getInfob().getCategory());
			
			//setting button visibility
			if (data_index == 1) {
				back.setVisibility(View.INVISIBLE);
			} else {
				back.setVisibility(View.VISIBLE);
			}
			
			if (data_index == item_data.size()) {
				next.setVisibility(View.INVISIBLE);
			} else {
				next.setVisibility(View.VISIBLE);
			}
			
			loadData();	

			no_review_button.setVisibility(View.VISIBLE);
			
			//also if we're not in last view we hide rating and ok stuff
			if (data_index < item_data.size() && 
					item_data.size() > 1) {
				review_failed_button.setVisibility(View.INVISIBLE);
				review_success_button.setVisibility(View.INVISIBLE);
			} else if (data_index == item_data.size() && !no_review){
				review_failed_button.setVisibility(View.VISIBLE);
				review_success_button.setVisibility(View.VISIBLE);
			} else if (no_review) {
				review_failed_button.setVisibility(View.INVISIBLE);
				review_success_button.setVisibility(View.INVISIBLE);
			}
			
		} else {
			header_text.setText("0/0");
		}
		
		
		next.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				//just excuting next command
				Next();
			}
			
		});
		
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//just executing back command
				Back();
			}	
		});
		
		no_review_button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (!no_review && ttw == null) {
					//only moving back to todays intervals
					((IntervalNotification)context).moveToTodaysIntervals();
				} else if (ttw == null) {
					((IntervalNotification)context).moveToAllItems();
				} else {
					ttw.Next();
				}
			}
			
		});
		
		review_failed_button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//executing commands for failed review
				setReviewed(false);
				
				if (ttw == null) {
					//then moving back to todays intervals
					((IntervalNotification)context).moveToTodaysIntervals();
				} else {
					ttw.Next();
				}
				
			}
			
		});
		
		review_success_button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//executing commands for successful review
				setReviewed(true);
				
				if (ttw == null) {
					//then moving back to todays intervals
					((IntervalNotification)context).moveToTodaysIntervals();
				} else {
					ttw.Next();
				}
			}
			
		});
		
		flipper.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View arg0, MotionEvent touchevent) {
				//first checking is we have correct view atm
				if (((IntervalNotification)context)
						.getWindow().getDecorView()
						.findViewById(R.id.thingtoremember_layout)
						!= null) {
					
					switch (touchevent.getAction()) {
						//basically back sweep
						case MotionEvent.ACTION_DOWN: {
							downAction(touchevent);
							return true;
						}
						//forward sweep
						case MotionEvent.ACTION_UP: {
							upAction(touchevent);
							return true;
						}
					}
				}
				
				return false;
			}
			
		});
	}
	
	private void findViews () {
		//finding header field
		header_text = (TextView)
				((IntervalNotification)
						context).findViewById(R.id.thing_header);
		
		//finding flipper
		flipper = (ViewFlipper)
				((IntervalNotification) 
					context).findViewById(R.id.thing_view_flipper);
				
		rating_content = (RelativeLayout)
				((IntervalNotification)
						context).findViewById(R.id.rating_content);
				
		ttr_id = (TextView)
				((IntervalNotification)
						context).findViewById(R.id.review_interval_id);
		
		data_index_field = (TextView)
				((IntervalNotification)
						context).findViewById(R.id.review_data_index);
		
		category_text_field = (TextView)
				((IntervalNotification)
						context).findViewById(R.id.thing_category_text);
		
		next = (ImageView)
				((IntervalNotification)
						context).findViewById(R.id.thing_next_button);
		
		back = (ImageView)
				((IntervalNotification)
						context).findViewById(R.id.thing_back_button);
	
		no_review_button = (ImageView)
				((IntervalNotification)
						context).findViewById(R.id.no_rating_button);
		
		review_failed_button = (ImageView)
				((IntervalNotification)
						context).findViewById(R.id.review_failed_button);
		
		review_success_button = (ImageView)
				((IntervalNotification)
						context).findViewById(R.id.review_success_button);
		
		review_field = (RelativeLayout) 
				((IntervalNotification)
						context).findViewById(R.id.thing_view_text_layout);
	}
		
	public void setLastX (float lastX) { this.lastX = lastX; }
	public float getLastX () { return lastX; }
	
	
	
}
