package memtools.intervalnotification.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import memtools.intervalnotification.IntervalNotification;
import memtools.intervalnotification.R;
import memtools.intervalnotification.Interface.InterfaceImplementation;
import memtools.intervalnotification.Libs.Interval;
import memtools.intervalnotification.Libs.Intervaltype;
import memtools.intervalnotification.Libs.ThingToRemember;
import android.content.Context;

public class TodaysTestWindow implements Serializable {

	/**
	 * 
	 */
	public static final long serialVersionUID = 1812434216967924387L;

	Context context;
	
	List<Interval> todays_test_list;
	int current = 0;
	
	public TodaysTestWindow(Context context, List<Interval> todays_test_list) {
		this.context = context;
		this.todays_test_list = 
				new ArrayList<Interval>(todays_test_list);
	}
	
	public void show() {
		InterfaceImplementation iface = new InterfaceImplementation(context);
		
		ItemViewWindow ivw = new ItemViewWindow(context, this, false);
		ivw.setVisible();
		todays_test_list.set(current, 
			iface.getInterval(todays_test_list.get(current).getId(), false));
		ThingToRemember ttr = todays_test_list.get(current).getTTR();
		ivw.loadWindowContent(ttr);
	}
	
	public void Next() {
		++current;
		
		if (current >= todays_test_list.size()) {
			((IntervalNotification)context).moveToView(R.layout.daysintervals);
		} else if (todays_test_list.get(current).getType()
				.equals(Intervaltype.MINUTE)) {
			Next();
		} else {
			show();
		}
	}
	
}
