package memtools.intervalnotification.ui;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import memtools.intervalnotification.IntervalNotification;
import memtools.intervalnotification.R;
import memtools.intervalnotification.Interface.InterfaceImplementation;
import memtools.intervalnotification.Libs.AddIntervalAdapter;
import memtools.intervalnotification.Libs.Category;
import memtools.intervalnotification.Libs.CategoryAdapter;
import memtools.intervalnotification.Libs.Interval;
import memtools.intervalnotification.Libs.Intervaltype;
import memtools.intervalnotification.Libs.MinuteInterval;
import memtools.intervalnotification.Libs.ThingToRemember;
import android.content.Context;
import android.os.Handler;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

public class AddIntervalWindow {
	Context context;
	//taking interface operations to use
	InterfaceImplementation iface;
	
	//listviews
	ListView ttr_info_listview;
	
	//list of things to remember
	List<ThingToRemember> ttrs;
	//list of categories
	List<Category> categories;
	//item that is currently selected
	ThingToRemember selected_ttr;
	
	//fields
	//auto completion field we want to show
	AutoCompleteTextView actv;
	//spinner to view the items
	Spinner spin_items;
	//select all items checkbox
	CheckBox cb;
	//minute interval checkbox
	CheckBox micb;
	//datefield
	EditText date;
	//interval add button
	Button interval_add_button;
	
	//adapters
	//category adapter
	//CategoryAdapter cat_adapter;
	//thing to remember info adapter
	ArrayAdapter<ThingToRemember> ttr_info_adapter;
	
	//constructor
	public AddIntervalWindow (IntervalNotification context) {
		this.context = context;
		iface = new InterfaceImplementation(this.context);
		
		//loading defaults
		//defaults();
	}
	
	public void setVisible() {
		((IntervalNotification)context).setContentView(R.layout.addinterval);
		//we can't load defaults before we have all the components visible
		defaults();
	}
	
	public void loadWindowContent() {
		//first finding all the fields
		findFields();
		
		//getting categories from the interface
		categories = iface.listAllCategories();
		
		//getting items from interface and items don't already have interval
		ttrs = iface.listItems(actv.getText().toString(), false);
		
		//we're updating information about ttrs
		for (int i = 0; i < ttrs.size(); ++i) {
			iface.updateTTRInfo(ttrs.get(i));
		}
		
		loadNonDBWindowContent();		
	}

	public void intervalSave () {
		findFields();
	
		//this should fix the problem of no info showing after adding new interval
		//(later) this probably isn't necessary because the problem was elsewhere
		//but still good to take care of
		if (selected_ttr == null && spin_items.getAdapter().getCount() > 0) {
			selected_ttr = (ThingToRemember)spin_items.getAdapter().getItem(0);
		} else if (selected_ttr == null) {
			defaults();
			return;
		}
		
		//first getting date
		Date temp_date;
		Intervaltype  temp_type;
		int temp_length = 1;
		
		//first checking is it minute interval or not
		if (!micb.isChecked()) {
			DateFormat df = DateFormat.getDateInstance();
			//interpreting date
			try {
				temp_date = df.parse(date.getText().toString());
			} catch (ParseException e) {
				Calendar cal = Calendar.getInstance();
				temp_date = cal.getTime();
			}
			//type is normal day interval
			temp_type = Intervaltype.DAY;
			//setting length to 1
			temp_length = 1;
		} else {
			//getting today
			Calendar calendar = Calendar.getInstance();
			//getting current date
			temp_date = calendar.getTime();
			//making type minute interval
			temp_type = Intervaltype.MINUTE;
			//we have length too
			try {
				temp_length = Integer.parseInt(date.getText().toString());
			} catch (NumberFormatException e) {
				temp_length = 1;
			}
		}
		
		//making sure we have correct value in selected thing to remember
		//interval id (hence it should be -1)
		selected_ttr.setIntervalId(-1);
		
		//finally calling interface to create our interval
		Interval temp_interval = iface.addInterval(temp_length, 
				temp_date, selected_ttr, temp_type);
		
		//if we have minute interval we make new handler to make sure 
		//we get notifications on time
		if (temp_interval.getType().equals(Intervaltype.MINUTE)) {
			//we put handler up for this minute interval
			//TODO check that ttr really gets here
			MinuteInterval mi = new MinuteInterval(this.context, selected_ttr);
			//setting handler
			Handler handler = new Handler();
			//setting delay
			handler.postDelayed(mi, mi.getNextDelay());
		}
		
		//updating interval activities hence we just added interval
		iface.updateIntervalActivities();
				
		//loading defaults
		defaults();
	}
	
	private void loadNonDBWindowContent() {
		//setting threshold how many letters we have to put when we start to see
		//suggestions
		actv.setThreshold(1);
		//cat_adapter.getFilter().filter(actv.getText().toString().toLowerCase());
		//creating adapter for this
		//CategoryListAdapter cat_adapter = new CategoryListAdapter(this, categories);
		CategoryAdapter cat_adapter = new CategoryAdapter(this.context, categories);
		actv.setAdapter(cat_adapter);
						
		//making new spinner adapter each time
		SpinnerAdapter ttr_adapter = new AddIntervalAdapter(this.context,ttrs);
				
		//setting adapter for spinner items
		spin_items.setAdapter(ttr_adapter);
				
				        		
		//adding on item click listener
		actv.setOnItemClickListener(new OnItemClickListener()
		{  
			public void onItemClick(AdapterView<?> parent, View v, int pos, long id) {
				cb.setChecked(false);
				loadWindowContent();
			}
		});
				
		cb.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				//only setting text to null (same as all categories)
				if (cb.isChecked()) {
					actv.setText("");
					loadWindowContent();
				}
			}
		});
				
		micb.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				//if minute interval check box is checked
				if (micb.isChecked()) {
					//getting header and setting proper text
					TextView tv = (TextView) 
							((IntervalNotification) 
									context).findViewById(R.id.starting_date_text);
					tv.setText(R.string.interval_length_text);
					
					//getting edit field and setting good type and hint
					EditText et = (EditText) 
							((IntervalNotification) 
									context).findViewById(R.id.date_edit_text);
							
					et.setInputType(InputType.TYPE_CLASS_NUMBER);
					et.setHint("");
					et.setText("1");
				}
				
				else {
					//getting header and setting proper text
					TextView tv = (TextView) 
							((IntervalNotification) 
									context).findViewById(R.id.starting_date_text);
							
					tv.setText(R.string.starting_date);
							
					//getting edit field and setting good type and hint
					EditText et = (EditText) 
							((IntervalNotification) 
									context).findViewById(R.id.date_edit_text);
							
					et.setInputType(InputType.TYPE_DATETIME_VARIATION_NORMAL);
					et.setHint(R.string.date_hint);
							
					//getting this day
					Calendar now = Calendar.getInstance();
					
					DateFormat df = DateFormat.getDateInstance();
					date.setText(df.format(now.getTime()));
				}
			}
		});
				
		//setting listener for spinner (so we know which item is selected at all times)
		spin_items.setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
				selected_ttr = (ThingToRemember)parent.getItemAtPosition(pos);
			}

			@SuppressWarnings("rawtypes")
			public void onNothingSelected(AdapterView parent) {
				
			}
		});
				
		interval_add_button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				//we might make this slightly more true to order of things 
				//but seems this works just fine
				if (selected_ttr != null) {
					String cs = context.getString(R.string.interval) + " " +
								selected_ttr.getInfob().getInfo();
					cs += " " + context.getString(R.string.was_added);
					
					//setting up time this notification is displayed
					int duration = Toast.LENGTH_SHORT;
					
					Toast toast = Toast.makeText(context, cs, duration);
					toast.show();
				}
				
				//saving interval
				intervalSave();
				//loading window contents again
				loadWindowContent();
			}
			
		});
		
		date.setOnEditorActionListener(new OnEditorActionListener(){

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE || 
						actionId == EditorInfo.IME_ACTION_GO ||
						actionId == EditorInfo.IME_ACTION_SEND) {
					interval_add_button.requestFocus();
					return true;
				} else if (actionId == EditorInfo.IME_ACTION_NEXT) {
					interval_add_button.requestFocus();
					return true;
				}

				return false;
			}
			
		});
	}
	
	//loads default values to all fields
	private void defaults () {
		findFields();
		
		//all selected is default
		cb.setChecked(true);
		
		//getting this day
		Calendar now = Calendar.getInstance();
		
		DateFormat df = DateFormat.getDateInstance();
		date.setText(df.format(now.getTime()));
				
		//not minute interval by default
		micb.setChecked(false);
	}
	
	//finds all the fields for use
	private void findFields() {
		//finding all the fields
		actv =  (AutoCompleteTextView) 
				((IntervalNotification) 
						context).findViewById(R.id.category_autocomplete_view);
		
		//finding the item spinner field we want to show
		spin_items = (Spinner) 
				((IntervalNotification) 
						context).findViewById(R.id.item_chooser);
		
		//finding select all items checkbox
		cb = (CheckBox)
				((IntervalNotification) 
						context).findViewById(R.id.choose_all_categories);
		
		//finding minute interval checkbox
		micb = (CheckBox)
				((IntervalNotification) 
						context).findViewById(R.id.is_minute_interval);
		
		date = (EditText)
				((IntervalNotification)
						context).findViewById(R.id.date_edit_text);
		
		//add button
		interval_add_button = (Button)
				((IntervalNotification)
						context).findViewById(R.id.interval_add_button);
	}
}

