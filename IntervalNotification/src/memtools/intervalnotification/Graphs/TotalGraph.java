package memtools.intervalnotification.Graphs;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import memtools.intervalnotification.Libs.CompoundStatistics;
import memtools.intervalnotification.Libs.Repetition;

import org.achartengine.chart.PointStyle;
import org.achartengine.chart.TimeChart;
import org.achartengine.model.TimeSeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.graphics.Color;
import android.util.Log;


public class TotalGraph {
	final int[] x = { 1,2,3,4,5,6,7,8,9,10,11,12 };
	
	CompoundStatistics stats;
	
	public TotalGraph (CompoundStatistics stats) {
		this.stats = stats;
		this.stats.repetitions = parseThisYear(this.stats.repetitions);
	}
	
	public XYMultipleSeriesRenderer getRenderer() {
		XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();
		XYSeriesRenderer r = new XYSeriesRenderer();
		//first graph
		r.setColor(Color.BLUE);
		r.setPointStyle(PointStyle.SQUARE);
		r.setFillPoints(true);
				
		renderer.addSeriesRenderer(r);
		return renderer;
	}
	
	public XYMultipleSeriesDataset getDataset() {
		//dataset
	    XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
	    //amount of months
	    final int months = 12;
	    
	    //getting values for each month
	    List<Integer> values = getValues(stats.repetitions);
	    
	    //getting current time
	    long value = new Date().getTime()-12 * 30 * TimeChart.DAY;
	    
	    TimeSeries series = new TimeSeries("Last 12 months reviews");
	    
	    for (int i = 0; i < months; i++) {
	    	 
	    	series.add((extractMonth(value)+ i)%12, 
	    			(values.get((extractMonth(value)+ i)%12)));
	    	Log.v("value of "+ ((extractMonth(value)+ i)%12 ), 
	    			""+values.get((extractMonth(value)+ i)%12));
	    }

	    dataset.addSeries(series);
	    
	    return dataset;
	}
	
	//extracts month in which this time stamp is...
	//@param Calendar time in milliseconds
	//@return 0..11
	private int extractMonth(long timestamp) {
		Calendar date = Calendar.getInstance();
		date.setTimeInMillis(timestamp);
		return date.get(Calendar.MONTH);
	}
	
	//reduces the list to contain only values behind one year
	private List<Repetition> parseThisYear (List<Repetition> stats) {
		List<Repetition> return_values = new ArrayList<Repetition>();
		Calendar now = Calendar.getInstance();
		now.add(Calendar.YEAR, -1);
		for(int i = 0; i < stats.size(); ++i) {
			if (stats.get(i).getTimestamp() > now.getTimeInMillis()) {
				return_values.add(stats.get(i));
			}
		}
		
		return return_values;
	}
	
	//@return correct amount of stuff into correct months
	private List<Integer> getValues(List<Repetition> stats) {
		List<Integer> values = new ArrayList<Integer>();
		values.add(0);
		values.add(0);
		values.add(0);
		values.add(0);
		values.add(0);
		values.add(0);
		values.add(0);
		values.add(0);
		values.add(0);
		values.add(0);
		values.add(0);
		values.add(0);
		
		for (int i = 0; i < stats.size(); ++i) {
			int month = extractMonth(stats.get(i).getTimestamp());
			values.set(month, values.get(month)+1);
		}
		return values;
	}
	
	class ChartValue { public Date date; public int value; };
	
	enum Month {
		JANUARY, FEBUARY, MARCH, APRIL, MAY, JUNE, JULY, AUGUST, SEPTEMBER,
		OCTOBER, NOVEMBER, DECEMBER
	};
}
