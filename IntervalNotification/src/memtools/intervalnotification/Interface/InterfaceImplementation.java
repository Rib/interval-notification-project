package memtools.intervalnotification.Interface;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import memtools.intervalnotification.IntervalNotification;
import memtools.intervalnotification.Libs.Category;
import memtools.intervalnotification.Libs.CompoundStatistics;
import memtools.intervalnotification.Libs.Data;
import memtools.intervalnotification.Libs.DataSmallInfo;
import memtools.intervalnotification.Libs.DatabaseDataSource;
import memtools.intervalnotification.Libs.Interval;
import memtools.intervalnotification.Libs.Intervaltype;
import memtools.intervalnotification.Libs.MinuteInterval;
import memtools.intervalnotification.Libs.Preferences;
import memtools.intervalnotification.Libs.Repetition;
import memtools.intervalnotification.Libs.ThingToRemember;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteException;
import android.os.Handler;
import android.util.Log;

public class InterfaceImplementation implements Interface {
	Context context;
	//constructor
	public InterfaceImplementation (Context context) {
		this.context = context;
		//creating the database and taking all its properties to use
		DatabaseDataSource tempdbds = new DatabaseDataSource(context.getApplicationContext());
		dbds = tempdbds;
	}
	
	public Category addCategory(String category) {
		return dbds.createCategory(category);
	}
	
	public ThingToRemember newTTR () {
		//creates and returns thing to remember
		return dbds.createThingToRemember();
	}
	
	public void deleteTTRDataContent(ThingToRemember ttr) {
		//getting the data of this ttr
		List<Data> datas = this.getTTRData(ttr);
		
		//deleting all the data
		for (int i = 0; i < datas.size(); ++i) {
			dbds.deleteData(datas.get(i));
		}
	}
	
	public void deleteTTR (ThingToRemember ttr) {
		//updating ttr just in case
		ttr = dbds.getTTR(ttr.getId());
		//deleting ttr
		dbds.deleteThingToRemember(ttr);
	}
	
	public DataSmallInfo getTTRInfo (ThingToRemember ttr) {
		return dbds.getTTRInfo(ttr);
	}
		
	public void updateTTRInfo (ThingToRemember ttr) {
		//just gets ttr info... but at the same time it updates it
		dbds.getTTRInfo(ttr);
	}
	
	public ThingToRemember addItem (List<Data> items, ThingToRemember ttr) {
		//if we have 0 sized list for some reason we just return ttr...
		if (items.size() == 0) return ttr;
		
		//visits whole arrays and adds them to database
		for (int i = 0; i < items.size(); ++i) {
			//PROGRAMMER NOTE: if performance becomes a problem just
			//move this whole data creation inside database connection
			//(it's WAY faster)
			items.get(i).setId(-1);
			dbds.createData(items.get(i), (int)ttr.getId());
		}
		
		//now we update recent info to ttr
		ttr = dbds.getTTR(ttr.getId());
		
		//setting info for this ttr
		if (items.size() > 0) {
			DataSmallInfo dsi = new DataSmallInfo();
			dsi.setInfo(items.get(0).getContent());
			ttr.setInfo(dsi);
		}
		
		return ttr;
	}
	
	public List<ThingToRemember> addItems (List<List<Data>> datas, List<ThingToRemember> ttr) {
		//going through all ttrs
		for (int i = 0; i < ttr.size(); ++i) {
			addItem(datas.get(i), ttr.get(i));
		}
		
		//creates and returns data
		return ttr;
	}
	
	public List<ThingToRemember> listItems (String category, boolean include_interval) {
		return dbds.getAllTTR(category, include_interval);
	}
	
	public void setCategoryTTR(Integer category_id, Integer ttr_id) {
		dbds.createCategoryTTR(category_id, ttr_id);
	}
	
	public List<Category> listAllCategories () {		
		return dbds.getAllCategories();
	}
	
	public List<Data> getTTRData (ThingToRemember ttr) {
		return dbds.getTTRData(ttr);
	}
	
	public boolean addFromFile (String filename) {
		File file = new File(filename);
		
		//PROGRAMMER NOTE:
		//this requires more knowledge about the format of the file
		
		return true;
	}
	
	public boolean updateIntervalActivities() {
		boolean gupdate = false;
		
		//first getting ALL intervals
		List<Interval> intervals = dbds.getAllIntervals(Interval.ALL);
		//getting current time
		Calendar now = Calendar.getInstance();
		//getting preferences
		SharedPreferences prefs = context.getSharedPreferences(
				IntervalNotification.SETTINGS_FILE, 0);
		
		int active_amount = 0;
		
		try {
			//then updating each interval
			//because we're accessing database multiple times we open connection now
			dbds.open();
			for (int i = 0; i < intervals.size(); ++i) {
				//boolean for if we have to update anything
				boolean update = false;
				
				boolean is_day_active = true;
				//storing this day
				int year = now.get(Calendar.YEAR);
				int month = now.get(Calendar.MONTH);
				int day = now.get(Calendar.DAY_OF_MONTH);
				long millis = now.getTimeInMillis();
				//storing interval day
				int int_year = intervals.get(i).getNextDate().get(Calendar.YEAR);
				int int_month = intervals.get(i).getNextDate().get(Calendar.MONTH);
				int int_day = intervals.get(i).getNextDate().get(Calendar.DAY_OF_MONTH);
				
				//first getting absolute truth value
				is_day_active &= (int_year <= year);
				
				is_day_active &= (int_month <= month);
				
				is_day_active &= (int_day <= day);
				
				//if we have smaller month
				is_day_active |= ((int_year <= year) && (int_month < month));
				
				//if we have smaller year
				is_day_active |= (int_year < year);
							
				//compensating current day interval update time
				now.add(Calendar.MILLISECOND, -(int) (prefs.getLong(Preferences.DAY_TICK.toString(), 
						(new Long(4*60*60*1000)))));
				
				if (((intervals.get(i).getType().equals(Intervaltype.DAY)) &&
						is_day_active) ||
					((int_day == now.get(Calendar.DAY_OF_MONTH)) &&
					(int_month == now.get(Calendar.MONTH)) &&
					(int_year == now.get(Calendar.YEAR)) &&
					(intervals.get(i).getType().equals(Intervaltype.DAY)))) {
					active_amount++;
				}
				
				//compensating current day interval update time
				now.add(Calendar.MILLISECOND, (int) (prefs.getLong(Preferences.DAY_TICK.toString(), 
						(new Long(4*60*60*1000)))));
							
				//then comparing absolute time
				if ((intervals.get(i).getNextDate().getTimeInMillis() < millis &&
						intervals.get(i).getType().equals(Intervaltype.MINUTE)) ||
						(is_day_active && 
						intervals.get(i).getType().equals(Intervaltype.DAY) &&
						(active_amount <= prefs.getInt(
								Preferences.DAYS_INTERVALS.toString(), 100)))) {
					//checking if we have to update
					if (!intervals.get(i).isActive()){ update = true;if (!gupdate) gupdate = true;}
					intervals.get(i).setActive(true);
					//then finally updating interval
				} else {	
					//checking if we have to update
					if (intervals.get(i).isActive()){ update = true; if (!gupdate) gupdate = true;}
					intervals.get(i).setActive(false);
				}
					
				if (update) {
					dbds.updateInterval(intervals.get(i),true);
				}
			}
			//closing database connection as we're done
			dbds.close();
		} catch (SQLiteException e) {
			e.printStackTrace();
		}
		
		return gupdate;
	}
	
	public List<Interval> nextActiveIntervals (int start, int amount) {		
		List<Interval> tempintervals = new ArrayList<Interval>();
		
		amount = amount-start;
		
		tempintervals = dbds.getAllIntervals(Interval.ACTIVE);
		
		List<Interval> returnintervals = new ArrayList<Interval>();
				
		//making sure we don't underflow
		if (start < 0 || amount < 1) {
			//just getting minute intervals
			for (int i = 0; i < tempintervals.size(); ++i) {
				if (tempintervals.get(i).isActive() && 
						tempintervals.get(i).getType().equals(Intervaltype.MINUTE)) {
					returnintervals.add(tempintervals.get(i));
				}
			}
			
			return returnintervals;
		}
				
		
		
		for (int i = 0; i < tempintervals.size(); ++i) {
			if (tempintervals.get(i).isActive() && amount > 0 && 
					tempintervals.get(i).getType().equals(Intervaltype.DAY)) {
				--amount;
				returnintervals.add(tempintervals.get(i));
			} else if (tempintervals.get(i).isActive() && 
					tempintervals.get(i).getType().equals(Intervaltype.MINUTE)) {
				returnintervals.add(tempintervals.get(i));
			}
		}
				
		//it is likely we don't need that code that's below so just sending 
		//active intervals
		return returnintervals;
	}
	
	public List<Interval> nextInactiveIntervals (int start, int amount) {
		List<Interval> tempintervals = new ArrayList<Interval>();
		
		amount = amount-start;
		
		//making sure we don't underflow and  at the same time we check amounts
		//underflow
		if (start < 0 || amount <= 0) {
			return tempintervals;
		}
				
		tempintervals = dbds.getAllIntervals(Interval.INACTIVE);
				
		//creating new list for pruning the list of inactive intervals
		List<Interval> return_intervals = new ArrayList<Interval>();
		
		//creating current time
		Calendar now = Calendar.getInstance();
		//checking if day has changed (or rather displacing the time)
		SharedPreferences prefs = context.getSharedPreferences(
				IntervalNotification.SETTINGS_FILE,0);
				
		now.add(Calendar.MILLISECOND, -(int) prefs.getLong(
				Preferences.DAY_TICK.toString(), 4*60*1000*60));
				
		for(int i=0; i < tempintervals.size(); ++i) {
			if (tempintervals.get(i).getPrevDate().get(Calendar.YEAR) ==
					now.get(Calendar.YEAR) &&
				tempintervals.get(i).getPrevDate().get(Calendar.MONTH) ==
					now.get(Calendar.MONTH) &&
				tempintervals.get(i).getPrevDate().get(Calendar.DAY_OF_MONTH) ==
					now.get(Calendar.DAY_OF_MONTH) && amount > 0 &&
					tempintervals.get(i).getType().equals(Intervaltype.DAY)) {
				return_intervals.add(tempintervals.get(i));
				--amount;
			}
		}
	
		return return_intervals;
				
		//should be tested if sublist doesn't do anything to make sure
		//that we don't overflow or underflow (though this shouldn't happen anyhow)
		//return tempintervals.subList(start, start+amount);
	}
	
	public int amountOfActiveIntervals () {		
		//yes, this is slow... but very clear anyhow
		return dbds.getAllIntervals(Interval.ACTIVE).size();
	}
	
	public int amountOfInactiveIntervals () {		
		//yes, this is slow... but very clear anyhow
		return dbds.getAllIntervals(Interval.INACTIVE).size();
	}
	
	public boolean updateInterval (double grade, Interval interval, boolean gradeable) {
		//if for some reason interval length is less than 1 we put it to 1
		if (interval.getLength() < 1) {
			interval.setLength(1);
		}
		
		//checking if gradeable and 
		if (gradeable && interval.getType() == Intervaltype.DAY && interval.isActive()) {
			dayIntervalUpdate(grade, interval);
		} else if (gradeable && interval.getType() == Intervaltype.MINUTE) {
			//if we doing minute interval
			minuteIntervalUpdate(grade, interval);
		} else {
			dbds.updateInterval(interval,false);
		}
		
		//at least for now no error should become unless database failed to update
		return true;
	}
	
	private void dayIntervalUpdate(double grade, Interval interval) {
		Double tempadd = (grade*interval.getLength());
		int tempint = tempadd.intValue();
		Calendar now = Calendar.getInstance();
		
		//setting previous date (now)
		interval.setPrevDate(now.getTimeInMillis());
		
		//because default grades are in days we start with that
		now.add(Calendar.DATE, tempint);
		tempadd -= tempadd.intValue();
		tempadd *= 24;
		
		//hours
		tempint = tempadd.intValue();
		now.add(Calendar.HOUR_OF_DAY, tempint);
		tempadd -= tempadd.intValue();
		tempadd *= 60;
		
		//finally minutes
		tempint = tempadd.intValue();
		now.add(Calendar.MINUTE, tempint);
		
		//here we have the formula for interval length
		if (grade < 1 && ((int) (grade*interval.getLength())) < 7 )
		interval.setLength((int) (grade*interval.getLength()));
		else {
			interval.setLength(7);
			now = Calendar.getInstance();
			now.add(Calendar.DAY_OF_MONTH, 7);
		}
		//setting next date
		interval.setNextDate(now.getTimeInMillis());
		interval.setActive(false);
		
		dbds.updateInterval(interval,false);
	}
	
	private void minuteIntervalUpdate(double grade, Interval interval) {
		//we add 1 minute because we need to speed up the process anyways
		//and our interval add length is always at least 2
		Double tempadd = (grade*interval.getLength()+1);
		
		//if we go over a day we switch to normal day interval
		if ((tempadd/(60*24)) > 1) {
			interval.setType(Intervaltype.DAY);
			interval.setLength(1);
			dayIntervalUpdate(1,interval);
		} else {
			//taking integer minute value
			int tempint = tempadd.intValue();
			Calendar now = Calendar.getInstance();
						
			//setting previous date (now)
			interval.setPrevDate(now.getTimeInMillis());
			
			//adding minutes
			now.add(Calendar.MINUTE, tempint);
			//setting previous date (now)
			//setting next date
			interval.setNextDate(now.getTimeInMillis());		
			//setting length of increment
			interval.setLength(tempadd.intValue());
			
			//setting activity to false
			interval.setActive(false);
			
			//updating the interval
			dbds.updateInterval(interval,false);
								
			//setting up the handler for setting minute interval active again
			MinuteInterval mi = new MinuteInterval(this.context,
					this.getInterval(interval.getId(), false).getTTR());
			Handler handler = new Handler();
			handler.postDelayed(mi, mi.getNextDelay());
		}
	}
	
	public Interval addInterval (int length, Date date, ThingToRemember ttr, Intervaltype type) {
		return dbds.createInterval(length, date, ttr, type);
	}
	
	public boolean removeInterval (Interval interval) {
		return dbds.deleteInterval(interval);
	}
	
	public List<Interval> listAllIntervals () {
		//gets all intervals
		return dbds.getAllIntervals(Interval.ALL);
	}
	
	public Interval getInterval(long id, boolean multiple) {
		return dbds.getInterval(id, multiple);
	}
	
	public boolean updateData (Data data, String newcategory, String newdata) {
		return true;
	}
	
	public ThingToRemember getTTR(long ttr_id) {
		return dbds.getTTR(ttr_id);
	}
	
	public boolean updateCategory(Category category) {
		
		return dbds.updateCategory(category);
	}
	
	public void updateStatistics (Repetition rep) {
		dbds.updateStatistics(rep);
	}
	
	public CompoundStatistics getStatistics() {
		return dbds.getStatistics();
	}
	
	//private stuff
	//database access stuff
	private DatabaseDataSource dbds;

}
