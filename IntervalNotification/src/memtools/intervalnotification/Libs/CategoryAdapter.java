package memtools.intervalnotification.Libs;

import java.util.ArrayList;
import java.util.List;

import memtools.intervalnotification.IntervalNotification;
import memtools.intervalnotification.R;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.TextView;

public class CategoryAdapter extends ArrayAdapter<Category> {
	Context context;
	
	List<Category> original;
	List<Category> categories;
	List<Category> filtered;
	
	Filter filter;
	
	public CategoryAdapter (Context context, List<Category> categories) {
		super(context,R.layout.category_list_item,categories);
		
		this.context = context;
		this.categories = new ArrayList<Category>(categories);
		this.original = new ArrayList<Category>(categories);
		
	
		filtered = new ArrayList<Category>();
	}

	@Override
	public int getCount() {
		return categories.size();
	}

	@Override
	public Category getItem(int position) {
		return categories.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			//getting inflater from context (IntervalNotification)
			LayoutInflater mInflater = LayoutInflater.from(context);
		
			//inflating convertview
			convertView = mInflater.inflate(R.layout.category_list_item, parent, false);
		}
		
		Category temp_category = categories.get(position);
		
		if (temp_category != null) {
			TextView category = (TextView)convertView
					.findViewById(R.id.category_list_field);
			
			if (category != null) {
				category.setText(categories.get(position).getName());
			}
			
			//then finally if we are in category list we change the color of the text
			if (((IntervalNotification)context).getCurrentView() == R.layout.categories &&
					!parent.getClass().getSuperclass().equals(ListView.class)) {
				category.setTextColor(Color.parseColor("#eeeeee"));
			}
		}
		
		return convertView;
	}
	
	@Override
	public Filter getFilter() {
		
		if (filter == null)
			filter = new CategoryFilter();
				
		return filter;
	}
	
	//making custom filter for category
	private class CategoryFilter extends Filter {

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			
			//making new filter results
			FilterResults result = new FilterResults();
			String prefix = "";
			if (constraint != null)
				prefix = constraint.toString().toLowerCase();

			if(constraint != null && prefix.length() > 0) {
				
				final ArrayList<Category> filt = new ArrayList<Category>();
                final ArrayList<Category> lItems = new ArrayList<Category>(original);
                
                int count = lItems.size();
                
                //manually filtering results
                for(int i = 0; i < count; ++i) {
                    final Category cat = lItems.get(i);
                    final String cat_string = cat.getName().toLowerCase();
                    
                    if(cat_string.startsWith(prefix))
                        filt.add(cat);
                }

                //then adding into filtered stuff
                result.count = filt.size();
                result.values = filt;

			} else {
				result.values = new ArrayList<Category>(original);
				result.count = original.size();
            }

			return result;
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void publishResults(CharSequence constraint, FilterResults results) {
			filtered = (ArrayList<Category>)results.values;
			notifyDataSetChanged();
			//setting categories
			categories = filtered;
			notifyDataSetInvalidated();
		}
		
	}

}
