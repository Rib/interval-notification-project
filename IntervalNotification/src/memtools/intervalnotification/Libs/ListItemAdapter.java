package memtools.intervalnotification.Libs;

import java.util.ArrayList;
import java.util.List;

import memtools.intervalnotification.IntervalNotification;
import memtools.intervalnotification.R;
import memtools.intervalnotification.Interface.InterfaceImplementation;
import memtools.intervalnotification.ui.ItemViewWindow;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ListItemAdapter extends ArrayAdapter<ThingToRemember> {
	Context context;
	
	List<ThingToRemember> items;
	List<ThingToRemember> original;
	List<ThingToRemember> filtered;
	
	Filter filter;
	
	public ListItemAdapter (Context context, List<ThingToRemember> items) {
		super(context, R.layout.items_single_item, items);
		this.context = context;
		this.items = new ArrayList<ThingToRemember>(items);
		this.original = new ArrayList<ThingToRemember>(items);
		
		filtered = new ArrayList<ThingToRemember>();
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public ThingToRemember getItem(int arg0) {
		return items.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(final int position, View convertView, final ViewGroup parent) {
		//gettin the inflater
		LayoutInflater mInflater = LayoutInflater.from(context);
		
		//creating holder
		final ViewHolder holder;
		
		//if there is nothing in convert view we just create it
		if (convertView == null) 
			//inflating the item
			convertView = mInflater.inflate(R.layout.items_single_item, parent, false);
		
		if (convertView.getTag() == null) {
			//tagging ttr
			items.get(position).setTag(convertView);
			
			//making new viewholder
			holder = new ViewHolder();
			
			//assigning values
			holder.category = (TextView)
					convertView.findViewById(R.id.single_item_category);
			holder.info = (TextView)
					convertView.findViewById(R.id.single_item_info);
			holder.edit = (ImageView)
					convertView.findViewById(R.id.single_item_edit);
			holder.full_screen = (ImageView)
					convertView.findViewById(R.id.single_item_full_review);
			holder.review = (RelativeLayout)
					convertView.findViewById(R.id.single_item_preview);
			holder.full_visible = false;
			holder.edit.setVisibility(View.GONE);
			holder.full_screen.setVisibility(View.GONE);
			holder.review.setVisibility(View.GONE);
			
			//setting tag to holder
			convertView.setTag(holder);
		} else {
			//if we already have the item we just get the tag
			holder = (ViewHolder) convertView.getTag();
			
			holder.full_visible = false;
			holder.edit.setVisibility(View.GONE);
			holder.full_screen.setVisibility(View.GONE);
			holder.review.setVisibility(View.GONE);
			((View)holder.info.getParent())
			.setBackgroundResource(R.drawable.button_thing_to_remember_up);
		}
		
		if (items.get(position).getInfob().getInfo().length() == 0) {
			InterfaceImplementation iface = new InterfaceImplementation(context);
			items.set(position, iface.getTTR(items.get(position).getId()));
			iface.updateTTRInfo(items.get(position));
		}
		
		//getting lengths 
		int cat_length = items.get(position).getInfob().getCategory().length() < 20 ?
				items.get(position).getInfob().getCategory().length() : 20;
		
		int info_length = items.get(position).getInfob().getInfo().length() < 20 ? 
				items.get(position).getInfob().getInfo().length() : 20;
		
		//category assignment
		if (items.get(position).getInfob().getCategory().length() > 0) {
			holder.category.setText
			(items.get(position).getInfob().getCategory().subSequence(0, cat_length));
		} else {
			holder.category.setText("");
		}
		
		//info assignment
		if (items.get(position).getInfob().getInfo().length() > 0) {
			holder.info.setText
			(items.get(position).getInfob().getInfo().subSequence(0, info_length));
		} else {
			holder.info.setText("");
		}

		LinearLayout ll = (LinearLayout) convertView
				.findViewById(R.id.single_item_info_layout);
		//setting onclick button for this view
		ll.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (holder.full_visible) {
					holder.full_visible = false;
					holder.edit.setVisibility(View.GONE);
					holder.full_screen.setVisibility(View.GONE);
					holder.review.setVisibility(View.GONE);
					((View)holder.info.getParent())
					.setBackgroundResource(R.drawable.button_thing_to_remember_up);
				} else {
					((View)holder.info.getParent())
					.setBackgroundResource(R.drawable.button_thing_to_remember_down);
					holder.full_visible = true;
					holder.edit.setVisibility(View.VISIBLE);
					holder.edit.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							((IntervalNotification) 
									context).moveToModifyItem(items.get(position));
						}
						
					});
					
					holder.edit.setOnTouchListener(new OnTouchListener() {

						@Override
						public boolean onTouch(View v, MotionEvent event) {
							
							if (event.getAction() == MotionEvent.ACTION_DOWN) {
								((ImageView)v).setImageResource(R.drawable.edit_button_down);
							} else if (event.getAction() == MotionEvent.ACTION_UP ||
									event.getAction() == MotionEvent.ACTION_CANCEL ||
									event.getAction() == MotionEvent.ACTION_OUTSIDE) {
								((ImageView)v).setImageResource(R.drawable.edit_button_up);
							}
							return false;
						}
						
					});
					
					holder.full_screen.setVisibility(View.VISIBLE);
					holder.full_screen.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							//making orientation orientation sensor based
							((IntervalNotification)context)
							.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
							
							((IntervalNotification)context).setContentView(R.layout.thingtoremember);
							((IntervalNotification)context).setCurrentView(R.layout.thingtoremember);
							ItemViewWindow ivw = new ItemViewWindow(context,null,true);
							ivw.setVisible();
							if (items.get(position).getInfob().getInfo().length() == 0) {
								InterfaceImplementation iface = new InterfaceImplementation(context);
								iface.updateTTRInfo(items.get(position));
							}
								
							ivw.loadWindowContent(items.get(position));
							
						}
						
					});
					
					holder.full_screen.setOnTouchListener(new OnTouchListener() {

						@Override
						public boolean onTouch(View v, MotionEvent event) {
							if (event.getAction() == MotionEvent.ACTION_DOWN) {
								((ImageView)v).setImageResource(R.drawable.full_screen_button_down);
							} else if (event.getAction() == MotionEvent.ACTION_UP ||
									event.getAction() == MotionEvent.ACTION_CANCEL ||
									event.getAction() == MotionEvent.ACTION_OUTSIDE) {
								((ImageView)v).setImageResource(R.drawable.full_screen_button_up);
							}
							return false;
						}
						
					});
										
					InterfaceImplementation iface = new InterfaceImplementation(context);
					List<Data> datas = iface.getTTRData(items.get(position));
					if (datas.size() > 0) {
						holder.review.setVisibility(View.VISIBLE);
						ReviewContent rev = 
								new ReviewContent(context, datas.get(0),
										null, null, true);
						RelativeLayout temp_l = null;
						if (holder.review.getChildCount() == 0)
						holder.review.addView(rev.getView(temp_l, true));
						else rev.getView(holder.review.getChildAt(0), true);
					}
				}
				
				((ListView)holder.category
						.getParent().getParent().getParent()).setSelection(position);
			}
			
		});
		
		ll.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent motion) {
				//TODO
				if (motion.getAction() == MotionEvent.ACTION_DOWN) {
					v.setBackgroundResource(R.drawable.button_thing_to_remember_down);
				} else if ((motion.getAction() == MotionEvent.ACTION_CANCEL ||
						motion.getAction() == MotionEvent.ACTION_OUTSIDE) &&
						holder.review.getVisibility() == View.GONE) {
					v.setBackgroundResource(R.drawable.button_thing_to_remember_up);
				}
				return false;
			}
			
		});
		
		return convertView;
	}
		
	//class for the views
	class ViewHolder {
		TextView category;
		TextView info;
		ImageView edit;
		ImageView full_screen;
		RelativeLayout review;
		boolean full_visible;
	}
	
	@Override
	public Filter getFilter() {
		
		if (filter == null)
			filter = new TTRFilter();
				
		return filter;
	}
	
	private class TTRFilter extends Filter {

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			//making new filter results
			FilterResults result = new FilterResults();
			
			String prefix = "";
			
			if (constraint != null)
				prefix = constraint.toString().toLowerCase();
			
            //seeing if we only list non intervalled intervals
            boolean non_intervals = 
            		((CheckBox)
            		((IntervalNotification)context)
            		.findViewById(R.id.items_non_intervals)).isChecked();
            
			
			if(constraint != null && prefix.length() > 0 || non_intervals) {
				synchronized(this) {
					final ArrayList<ThingToRemember> filt = new ArrayList<ThingToRemember>();
	                final ArrayList<ThingToRemember> lItems = new ArrayList<ThingToRemember>(original);
	                
	                //getting the size of items
	                int count  = lItems.size();
	                                
	                //manually filtering results
	                for(int i = 0; i < count; ++i) {
	                	InterfaceImplementation iface = new InterfaceImplementation(context);
	                	
	                	
	                    if (lItems.get(i).getInfob().getInfo().length() == 0) {
	                    	if (lItems.get(i).getIntervalId() == 0)
	                    	lItems.set(i, iface.getTTR(lItems.get(i).getId()));
	                    	iface.updateTTRInfo(lItems.get(i));
	                    	//TODO
	                    	//There is problem with syncing with database when there is only 1 item in the list
	                    }
	                    ThingToRemember ttr = lItems.get(i);
	                    //ThingToRemember ttr = iface.getTTR(lItems.get(i).getId());
	                    //iface.updateTTRInfo(ttr);
	                    
	    				String category = ttr.getInfob().getCategory();
	    				String info = ttr.getInfob().getInfo();
	                    String ttr_string = (category + " " + info).toLowerCase();
	                    if(ttr_string.contains(prefix) || prefix == "") {
	                    	if (!non_intervals) filt.add(ttr);
	                    	else if (non_intervals && ttr.getIntervalId() < 1) filt.add(ttr);
	                    }
	      
	                }
	                
	                //then adding into filtered stuff
	                result.count = filt.size();
	                result.values = filt;
				}
			} else {
				synchronized(this) {
					result.values = new ArrayList<ThingToRemember>(original);
					result.count = original.size();
				}
			}
			
			return result;
		}
		
		@SuppressWarnings("unchecked")
		@Override
		protected void publishResults(CharSequence constraint, FilterResults results) {
			synchronized(this) {
				filtered = (ArrayList<ThingToRemember>)results.values;
				//setting new items
				items = filtered;
			}
			notifyDataSetChanged();
		}
		
	}

}
