package memtools.intervalnotification.Libs;

import java.util.ArrayList;
import java.util.List;

import memtools.intervalnotification.R;
import memtools.intervalnotification.ui.TodaysIntervals;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DayIntervalAdapter extends ArrayAdapter<Interval> {
	Context context;
	TodaysIntervals window_context;
	
	List<Interval> intervals;
	List<Interval> original;
	List<Interval> filtered;
	
	Filter filter;
	
	public DayIntervalAdapter (Context context,
			TodaysIntervals window_context, List<Interval> intervals) {
		super(context, R.layout.daysintervals, intervals);
		this.context = context;
		this.intervals = new ArrayList<Interval>(intervals);
		this.original = new ArrayList<Interval>(intervals);
		this.window_context = window_context;
		
		this.filtered = new ArrayList<Interval>();
	}

	//gets list size 
	@Override
	public int getCount() {
		return intervals.size();
	}

	//gets the item from the list
	@Override
	public Interval getItem(int arg0) {

		return intervals.get(arg0);
	}

	//gets the id of the item
	@Override
	public long getItemId(int arg0) {
		return intervals.get(arg0).getId();
	}
	
	@Override
	public Filter getFilter() {
		
		if (filter == null)
			filter = new IntervalFilter();
				
		return filter;
	}

	//get view override
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		//getting the inflater
		LayoutInflater mInflater = LayoutInflater.from(context);
		
		//creating holder
		ViewHolder holder;
		
		//inflating the item		
		/*convertView = intervals.get(position).isActive() ? 
				mInflater.inflate(R.layout.days_intervals_item, parent, false) :
				mInflater.inflate(R.layout.days_intervals_inactive_item, parent, false)	;*/
		if (convertView == null || convertView.getTag() == null) {
			
			if (convertView == null)
			convertView = mInflater.inflate(R.layout.days_intervals_item, parent, false);
				
			//setting tag of current view for further use 
			intervals.get(position).setTag(convertView);		
						
			//making new viewholder
			holder = new ViewHolder();
				
			//assigning views
			holder.category = (TextView) 
					convertView.findViewById(R.id.days_intervals_active_category);
			holder.info = (TextView)
					convertView.findViewById(R.id.days_intervals_active_info);
			holder.type = (ImageView)
					convertView.findViewById(R.id.days_intervals_image_type);
			holder.bind = (LinearLayout)
					convertView.findViewById(R.id.days_intervals_active_single_item);
			
			convertView.setTag(holder);
		
		} else {
			holder = (ViewHolder) convertView.getTag();
			convertView.setBackgroundResource(R.drawable.ttr_button);
		}
		
		//choosing view either is active or not
		if (intervals.get(position).isActive()) { 
			/*((TextView) 
			convertView.findViewById(R.id.days_intervals_active_static_category))
			.setTextColor(Color.parseColor("#eeeeee"));
			((TextView) 
			convertView.findViewById(R.id.days_intervals_active_static_info))
			.setTextColor(Color.parseColor("#eeeeee"));
			holder.category.setTextColor(Color.parseColor("#eeeeee"));
			holder.info.setTextColor(Color.parseColor("#eeeeee"));*/
			holder.type.setImageResource(R.drawable.normal_interval);
			
		} else { 
			/*((TextView) 
			convertView.findViewById(R.id.days_intervals_active_static_category))
			.setTextColor(Color.parseColor("#444444"));
			((TextView) 
			convertView.findViewById(R.id.days_intervals_active_static_info))
			.setTextColor(Color.parseColor("#444444"));
			holder.category.setTextColor(Color.parseColor("#444444"));
			holder.info.setTextColor(Color.parseColor("#444444"));*/
			holder.type.setImageResource(R.drawable.old_interval);
		}
		
		if (intervals.get(position).getTTR() != null) {
			//finding maximum lengths
			int cat_length = intervals.get(position).getTTR().getInfob().getCategory().length();
			cat_length = cat_length < 20 ? cat_length : 20;
			
			int info_length = intervals.get(position).getTTR().getInfob().getInfo().length();
			info_length = info_length < 20 ? info_length : 20;
			
			//category assignment
			if (cat_length > 0) {
				holder.category.setText(
						intervals.get(position).getTTR().getInfob().getCategory().subSequence(0, cat_length));
			} else {
				holder.category.setText("");
			}
			
			//info assignment
			if (info_length > 0) {
				holder.info.setText(
						intervals.get(position).getTTR().getInfob().getInfo().subSequence(0, info_length));
			} else {
				holder.info.setText("");
			}
		
		}
		
		//then finally see if we have active minute interval
		if (intervals.get(position).getType() == Intervaltype.MINUTE) {
			/*((TextView) 
			convertView.findViewById(R.id.days_intervals_active_static_category))
			.setTextColor(Color.parseColor("#ee0000"));
			((TextView) 
			convertView.findViewById(R.id.days_intervals_active_static_info))
			.setTextColor(Color.parseColor("#ee0000"));
			holder.category.setTextColor(Color.parseColor("#ee0000"));
			holder.info.setTextColor(Color.parseColor("#ee0000"));*/
			holder.type.setImageResource(R.drawable.minute_interval);
		}
		
		//setting click action to bindview
		holder.bind.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				window_context.moveToReview(position);
			}
			
		});
		
		((LinearLayout)convertView).setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View convertView, MotionEvent action) {
				if (action.getAction() == MotionEvent.ACTION_DOWN) {
					convertView.setBackgroundResource(R.drawable.ttr_button_down);
				} else if (action.getAction() == MotionEvent.ACTION_UP ||
						action.getAction() == MotionEvent.ACTION_CANCEL ||
						action.getAction() == MotionEvent.ACTION_OUTSIDE) {
					convertView.setBackgroundResource(R.drawable.ttr_button);
				}
				return false;
			}
			
		});
						
		return convertView;
	}
	
	class ViewHolder {
		TextView category;
		TextView info;
		ImageView type;
		LinearLayout bind;
	}
	
	private class IntervalFilter extends Filter{
		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			//making new filter results
			FilterResults result = new FilterResults();
			
			String prefix = "";
			
			if (constraint != null)
				prefix = constraint.toString().toLowerCase();
			
			if(constraint != null && prefix.length() > 0) {
				synchronized(this){
					final ArrayList<Interval> filt = new ArrayList<Interval>();
	                final ArrayList<Interval> lItems = new ArrayList<Interval>(original);
	                
	                //getting the size of items
	                int count  = lItems.size();
	                
	                //manually filtering results
	                for(int i = 0; i < count; ++i) {
	                    final Interval interval = lItems.get(i);
	    				String category = interval.getTTR().getInfob().getCategory();
	    				String info = interval.getTTR().getInfob().getInfo();
	                    final String interval_string = (category + " " + info).toLowerCase();	
	                    
	                    if(interval_string.contains(prefix) && 
	                    	(interval.isActive() || 
	                    	(!interval.isActive() &&
	                            	!interval.getType().equals(Intervaltype.MINUTE))))
	                        filt.add(interval);
	                }
	                
	                //then adding into filtered stuff
	                result.count = filt.size();
	                result.values = filt;
				}
			} else {
				synchronized(this){
					final ArrayList<Interval> filt = new ArrayList<Interval>();
	                final ArrayList<Interval> lItems = new ArrayList<Interval>(original);
	                
	                //getting the size of items
	                int count  = lItems.size();
	                
	                //manually filtering results
	                for(int i = 0; i < count; ++i) {
	                    final Interval interval = lItems.get(i);
	                    if (interval.isActive() || (!interval.isActive() &&
	                        	!interval.getType().equals(Intervaltype.MINUTE)))
	                    	filt.add(interval);
	                }
	                
	                //then adding into filtered stuff
	                result.count = filt.size();
	                result.values = filt;
				}
            }
			
			return result;
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void publishResults(CharSequence constraint,
				FilterResults results) {
			filtered = (ArrayList<Interval>)results.values;
			notifyDataSetChanged();
				
			//setting categories
			intervals = filtered;
			notifyDataSetInvalidated();
		}
	}
	
}
