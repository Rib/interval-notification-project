package memtools.intervalnotification.Libs;

import memtools.intervalnotification.ImageViewerActivity;
import memtools.intervalnotification.IntervalNotification;
import memtools.intervalnotification.R;
import memtools.intervalnotification.VideoPlayerActivity;
import memtools.intervalnotification.ui.ItemViewWindow;
import memtools.intervalnotification.ui.TodaysTestWindow;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ReviewContent {
	//context in which this adapter is
	Context context;
	
	//data to be shown
	Data data;

	ThingToRemember ttr;
	
	TodaysTestWindow ttw;
	boolean no_review;
	
	AudioPlayer control_audio;
	
	
	public ReviewContent(Context context, Data data, ThingToRemember ttr, TodaysTestWindow ttw, boolean no_review) {
		this.context = context;
		
		this.data = data;
		
		this.ttr = ttr;
		
		this.ttw = ttw;
		this.no_review = no_review;
		
		//getting the type of this data
		interpretType();
	}

	public int getCount() {
		//count is always one hence we have only one item :)
		return 1;
	}

	public Object getItem(int arg0) {
		//item clearly is data
		return data;
	}

	public long getItemId(int arg0) {
		//id is the id of this data
		return data.getId();
	}

	public View getView(View convertView, boolean include_heavy) {
		//getting the inflater
		LayoutInflater mInflater = LayoutInflater.from(context);
		
		//setting viewholder
		ViewHolder holder;
		holder = new ViewHolder();
		
		if (convertView == null || convertView.getTag() == null) {		
			//we inflate the view field
			convertView = mInflater.inflate(R.layout.master_content_view, null);
						
			//inflating all views
			holder.video_field = (VideoPlayer)
			convertView.findViewById(R.id.video_view_id);
			holder.image_field = (ImageView)
					convertView.findViewById(R.id.image_view_id);
			holder.audio_text = (TextView)
					convertView.findViewById(R.id.audio_now_playing);
			holder.now_playing_text = (TextView)
					convertView.findViewById(R.id.audio_now_playing_text);
			holder.audio_field = (AudioPlayer) 
					convertView.findViewById(R.id.audio_media_controller);
			holder.text_field = (TextView)
					convertView.findViewById(R.id.text_view_id);
			holder.action_button = (Button)
					convertView.findViewById(R.id.action_button);
			
			convertView.setTag(holder);	
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
			
			//finding conversions
			if (data.getType().equals(Datatype.VIDEO)) {
				/*holder.video_field = (VideoPlayer)
						convertView.findViewById(R.id.video_view_id);
				holder.image_field = (ImageView)
						convertView.findViewById(R.id.thumbnail_in_video_view);*/
				holder.video_field.setVisibility(View.INVISIBLE);
				holder.image_field .setVisibility(View.VISIBLE);
				holder.audio_text.setVisibility(View.GONE);
				holder.now_playing_text.setVisibility(View.GONE);
				holder.audio_field.setVisibility(View.GONE);
				holder.text_field.setVisibility(View.GONE);
				holder.action_button.setVisibility(View.VISIBLE);
				
			} else if (data.getType().equals(Datatype.IMAGE)) {
				/*holder.image_field = (ImageView)
						convertView.findViewById(R.id.image_view_id);*/
				holder.video_field.setVisibility(View.GONE);
				holder.image_field .setVisibility(View.VISIBLE);
				holder.audio_text.setVisibility(View.GONE);
				holder.now_playing_text.setVisibility(View.GONE);
				holder.audio_field.setVisibility(View.GONE);
				holder.text_field.setVisibility(View.GONE);
				holder.action_button.setVisibility(View.GONE);
			} else if (data.getType().equals(Datatype.AUDIO)) {
				/*holder.text_field = (TextView)
						convertView.findViewById(R.id.audio_now_playing);
				holder.audio_field = (AudioPlayer) 
						convertView.findViewById(R.id.audio_media_controller);*/
				holder.video_field.setVisibility(View.GONE);
				holder.image_field .setVisibility(View.GONE);
				holder.audio_text.setVisibility(View.VISIBLE);
				holder.now_playing_text.setVisibility(View.VISIBLE);
				holder.audio_field.setVisibility(View.VISIBLE);
				holder.text_field.setVisibility(View.GONE);
				holder.action_button.setVisibility(View.GONE);
				
				control_audio = holder.audio_field;
			} else {
				/*holder.text_field = (TextView)
						convertView.findViewById(R.id.text_view_id);*/
				holder.video_field.setVisibility(View.GONE);
				holder.image_field .setVisibility(View.GONE);
				holder.audio_text.setVisibility(View.GONE);
				holder.now_playing_text.setVisibility(View.GONE);
				holder.audio_field.setVisibility(View.GONE);
				holder.text_field.setVisibility(View.VISIBLE);
				holder.action_button.setVisibility(View.GONE);
			}
		
		//assigning contents
		if (data.getType().equals(Datatype.VIDEO) && include_heavy) {
			Bitmap curThumb = ThumbnailUtils.createVideoThumbnail(data.getContent(),
				MediaStore.Video.Thumbnails.MICRO_KIND);
			
			final GestureDetector gd = new GestureDetector(new CustomVideoGestureDetector(holder.video_field));
			
			holder.image_field.setImageBitmap(curThumb);
			
			View.OnTouchListener ivotl = new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					return gd.onTouchEvent(event);
				}
				
			};
			
			//setting null onclick listener that performs onclick just for fun :)
			holder.action_button.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					//this makes everything work... don't ask me how
				}
				
			});
						
			holder.action_button.setOnTouchListener(ivotl);
			
			holder.video_field.setContent(holder.image_field, data.getContent(), 
					holder.video_field);
						
		} else if (data.getType().equals(Datatype.IMAGE) && include_heavy) {
			//desampling file first
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds  = true;
			BitmapFactory.decodeFile(data.getContent(), options);
			
			//setting sample size
			int height = 
					((IntervalNotification)context)
					.getWindowManager().getDefaultDisplay().getHeight();
			int width = 
					((IntervalNotification)context)
					.getWindowManager().getDefaultDisplay().getWidth();
			options.inSampleSize = 
					ImageViewerActivity.calculateInSampleSize(options, height/2, width/2);
			
			//then really decoding image
			options.inJustDecodeBounds = false;
			Bitmap temp_bm = BitmapFactory.decodeFile(data.getContent(), options);
			holder.image_field.setImageBitmap(temp_bm);
						
			final GestureDetector gd = new GestureDetector(new CustomImageGestureDetector());

			ImageView.OnTouchListener ivotl = new ImageView.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					return gd.onTouchEvent(event);
				}
				
			};
			
			//setting null onclick listener that performs onclick just for fun :)
			holder.image_field.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					//this makes everything work... don't ask me how
				}
				
			});
			
			holder.image_field.setOnTouchListener(ivotl);
							
		}  else if (data.getType().equals(Datatype.AUDIO) && include_heavy) {
			holder.audio_text.setText(data.getContent());
			holder.audio_field.setContent(data, (View) holder.audio_field.getParent());
			
			final GestureDetector gd = new GestureDetector(new CustomAudioGestureDetector());

			AudioPlayer.OnTouchListener rotl = new AudioPlayer.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					return gd.onTouchEvent(event);
				}
				
			};
			
			//setting null onclick listener that performs onclick just for fun :)
			((AudioPlayer)holder.audio_field).setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					//this makes everything work... don't ask me how
				}
				
			});
			
			((AudioPlayer)holder.audio_field).setOnTouchListener(rotl);
			
		} else {
			holder.text_field.setText(data.getContent());
		}

		return convertView;
	}
		
	//returns current type or the file
	public Datatype getType() {
		return data.getType();
	}
	
	//this function recognizes the type of this file/data
	private void interpretType () {
		if (data.getContent().endsWith(".mp4") || 
				data.getContent().endsWith(".3gp") ||
				data.getContent().endsWith(".webm")) {
			data.setType(Datatype.VIDEO);
		} else if (data.getContent().endsWith(".jpg") ||
				data.getContent().endsWith(".gif") ||
				data.getContent().endsWith(".png") ||
				data.getContent().endsWith(".bmp") ||
				data.getContent().endsWith(".webp")) {
			data.setType(Datatype.IMAGE);
		} else if (data.getContent().endsWith(".flac") ||
				data.getContent().endsWith(".mp3") ||
				data.getContent().endsWith(".mid") ||
				data.getContent().endsWith(".xmf") ||
				data.getContent().endsWith(".mxmf") ||
				data.getContent().endsWith(".rtttl") ||
				data.getContent().endsWith(".rtx") ||
				data.getContent().endsWith(".ota") ||
				data.getContent().endsWith(".imy") ||
				data.getContent().endsWith(".ogg") ||
				data.getContent().endsWith(".wav") ||
				data.getContent().endsWith(".amr")) {
			data.setType(Datatype.AUDIO);
		}
	}
	
	//we have holder that can hold everything possible
	//but doesn't do this
	private class ViewHolder {
		TextView text_field;
		TextView audio_text;
		TextView now_playing_text;
		VideoPlayer video_field;
		ImageView image_field;
		AudioPlayer audio_field;
		Button action_button;
	}
	
	private class CustomVideoGestureDetector extends SimpleOnGestureListener
	implements GestureDetector.OnDoubleTapListener {
	    private static final int SWIPE_MIN_DISTANCE = 15;
	    private static final int SWIPE_MAX_OFF_PATH = 250;
	    private static final int SWIPE_THRESHOLD_VELOCITY = 100;
	    
	    VideoPlayer video_player;
	    
	    public CustomVideoGestureDetector(VideoPlayer video_player) {
	    	super();
	    	this.video_player = video_player;
	    }
	    
	    //setting on fling which is overrides "normal" moving movements in this view
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        	
        	if (ttr != null) {
	            try {
	                if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
	                    return false;
	
	                ItemViewWindow ivw = new ItemViewWindow((IntervalNotification)context, ttw, no_review);
	                ivw.loadWindowContent(ttr);
	                
	                if(e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
	                	ivw.Next();
	                }  else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
	                	ivw.Back();
	                }
	
	            } catch (Exception e) {
	                // nothing
	            }
        	}
            return false;
        }
        
        //on double tap we go to full screen
 		@Override
 		public boolean onDoubleTap(MotionEvent e) { 			
 			if (video_player.isPlaying()) video_player.stopPlayback();
 			Intent intent = new Intent(context, VideoPlayerActivity.class);
 			intent.putExtra(VideoPlayerActivity.FILE, video_player.getPath());
 			intent.putExtra(VideoPlayerActivity.VIDEO_CONTINUE, 
 					video_player.getCurrentPosition());
 			((IntervalNotification)context).startActivity(intent);
 			return true;
 		}
 		
 		//on confirmed single tap we go to full screen
 		@Override
 		public boolean onSingleTapConfirmed(MotionEvent e) {
 	    	if (!video_player.isPlaying()) {
 	    		video_player.start();
 	    	}
 			return true;
 		}
	}
	
	//this gesture listener handles swipe and all the tap gestures in correct way
	private class CustomImageGestureDetector extends SimpleOnGestureListener 
	implements GestureDetector.OnDoubleTapListener {
		
	    private static final int SWIPE_MIN_DISTANCE = 15;
	    private static final int SWIPE_MAX_OFF_PATH = 250;
	    private static final int SWIPE_THRESHOLD_VELOCITY = 100;
	    
	    //setting on fling which is overrides "normal" moving movements in this view
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        	
        	if (ttr != null) {
	            try {	
	                if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
	                    return false;
	                	
	                ItemViewWindow ivw = new ItemViewWindow((IntervalNotification)context, ttw, no_review);
	                ivw.loadWindowContent(ttr);
	                
	                if(e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
	                	ivw.Next();
	                }  else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
	                	ivw.Back();
	                }
	
	            } catch (Exception e) {
	                // nothing
	            }
        	}
        	
            return false;
        }

        //on double tap we go to full screen
		@Override
		public boolean onDoubleTap(MotionEvent e) {
	    	Intent intent = new Intent(context, ImageViewerActivity.class);
			intent.putExtra(ImageViewerActivity.FILE, data.getContent());
			((IntervalNotification)context).startActivity(intent);
			return true;
		}

		//on confirmed single tap we go to full screen
		@Override
		public boolean onSingleTapConfirmed(MotionEvent e) {
	    	Intent intent = new Intent(context, ImageViewerActivity.class);
			intent.putExtra(ImageViewerActivity.FILE, data.getContent());
			((IntervalNotification)context).startActivity(intent);
			return true;
		}
	}
	
	//audio gesture listener (mainly for moving forward)
	private class CustomAudioGestureDetector extends SimpleOnGestureListener{
		
	    private static final int SWIPE_MIN_DISTANCE = 15;
	    private static final int SWIPE_MAX_OFF_PATH = 250;
	    private static final int SWIPE_THRESHOLD_VELOCITY = 100;
	    
	    //setting on fling which is overrides "normal" moving movements in this view
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        	
        	if (ttr != null) {
	            try {
	                if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
	                    return false;
	               
	                if (control_audio != null) {
	                	control_audio.stop();
	                }
	                
	                ItemViewWindow ivw = new ItemViewWindow((IntervalNotification)context, ttw, no_review);
	                ivw.loadWindowContent(ttr);
	                
	                if(e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
	                	ivw.Next();
	                }  else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
	                	ivw.Back();
	                }
	
	            } catch (Exception e) {
	                // nothing
	            }
        	}
        	
            return false;
        }
	}
}
