package memtools.intervalnotification.Libs;

import java.io.File;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import memtools.intervalnotification.FileBrowserActivity;
import memtools.intervalnotification.IntervalNotification;
import memtools.intervalnotification.R;
import memtools.intervalnotification.Interface.InterfaceImplementation;
import memtools.intervalnotification.ui.AddItemWindow;
import memtools.intervalnotification.ui.ModifyItemWindow;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

public class AddItemAdapter extends BaseAdapter {
	//adapters context
	Context context;
	//data values
	List<Data> values;
	//some layout items
	List<Data> litems;
	
	String category_text = "";
	boolean adding_interval = true;
	boolean is_minute_interval = false;
	String date = "";
	
	boolean drop_down = true;
	
	//window in which this adapter was created
	int window;
	
	//this is here if we're at modifying item
	ThingToRemember ttr;
		
	//constuctor
	public AddItemAdapter(Context context, List<Data> values, int window, String category_text,
			boolean adding_interval, boolean is_minute_interval, String date) {
		this.context = context;
		
		this.values = values;
		
		this.window = window;
		
		this.category_text = category_text;
		this.adding_interval = adding_interval;
		this.is_minute_interval = is_minute_interval;
		this.date = date;
		
		Data dat = null;
		litems = new ArrayList<Data>();
		litems.add(dat);
		//litems.add(dat);
		//litems.add(dat);
		//litems.add(dat);
	}
	
	//returning list size as count
	@Override
	public int getCount() {
		return litems.size()+values.size()+1;
	}

	//returning item at arg0
	@Override
	public Object getItem(int arg0) {
		return values.get(arg0);
	}

	//returning same value back :)
	@Override
	public long getItemId(int arg0) {
		return arg0;
	}
	
	public void setTTR(ThingToRemember ttr) {
		this.ttr = ttr;
	}
	
	//gets whole list
	public List<Data> getItems() { return values; }
	
	//gets the category text
	public String getCategory() { return category_text; }
	
	//gets if we're adding interval
	public boolean addingInterval() { return adding_interval; }
	
	//gets if we're doing minute interval
	public boolean isMinuteInterval() { return is_minute_interval; }
	
	//gets the datefields current value
	public String getDate() { return date; }
	
	public void setCategory(String category) {
		category_text = category;
	}
	
	public void setAddingInterval (boolean adding_interval) {
		this.adding_interval = adding_interval;
	}
	
	public void setMinuteInterval (boolean is_minute_interval) {
		this.is_minute_interval = is_minute_interval;
	}
	
	public void setDate (String date) {
		this.date = date;
	}
	
	@Override
	public View getView(final int position, View convertView, final ViewGroup parent) {
		//getting the inflater
		LayoutInflater mInflater = LayoutInflater.from(context);
		
		//creating holder
		ViewHolder holder;
		
		//if there is nothing in convert view we just create it
		//if (convertView == null) {
			//inflating the item
			if (window == 0 && (position <= values.size()) && position >= litems.size()) {
				convertView = mInflater.inflate(R.layout.additem_item, parent, false);
			} else if (window == 1 && (position <= values.size()) && position >= litems.size()) {
				convertView = mInflater.inflate(R.layout.modifyitem_item, parent, false);
			} else if ( position == 0 ) { 
				convertView = (window == 0 ? 
						mInflater.inflate(R.layout.add_item_header, parent, false) : 
						mInflater.inflate(R.layout.modify_item_header, parent, false));
			} else {
				convertView = mInflater.inflate(R.layout.add_button_layout, parent,false);
			}
									
			//making new viewholder
			holder = new ViewHolder();
			
			if (window == 0 && (position <= values.size()) && position >= litems.size()) {
				//finding the fields
				holder.delete_button = (ImageView)
						convertView.findViewById(R.id.this_item_delete_button);
				holder.data = (EditText)
						convertView.findViewById(R.id.this_item_edit_text);
				holder.file_button = (ImageView)
						convertView.findViewById(R.id.this_item_from_file_button);
				holder.camera_button = (ImageView)
						convertView.findViewById(R.id.this_item_take_picture_button);
			} else if (window == 1 && (position <= values.size()) && position >= litems.size()) {
				//finding the fields
				holder.delete_button = (ImageView)
						convertView.findViewById(R.id.modify_single_item_delete_button);
				holder.data = (EditText)
						convertView.findViewById(R.id.modify_item_edit_text);
				holder.file_button = (ImageView)
						convertView.findViewById(R.id.modify_item_from_file_button);
				holder.camera_button = (ImageView)
						convertView.findViewById(R.id.modify_item_take_picture_button);
			}
			
			if (position < values.size()&& position >= litems.size()) {
				//setting tag to recognize this view later
				values.get(position).setTag(holder);
			}
			
			//setting tag to holder
		/*	convertView.setTag(holder);
		} else {
			//if we already have the item we just get the tag
			holder = (ViewHolder) convertView.getTag();
		}*/
		if (position <= values.size() && position >= litems.size()) {
			if (values.get(position-litems.size()).getContent() != null && 
					values.get(position-litems.size()).getContent().length() > 0) {
				holder.data.setText(values.get(position-litems.size()).getContent());
			} else if (position == litems.size()) {
				holder.data.setHint(R.string.question_hint);
			} else if (position > litems.size()) {
				holder.data.setHint(R.string.andwer_hint);
			}
		
			//finally adding text change listener to make sure we have updated info at all times
			//nothing needs to be done as long as we don't do ANYTHING extra :)
			
			//setting type of data if we're at the last
			if (position == values.size()) {
				holder.data.setImeOptions(EditorInfo.IME_ACTION_DONE);
			}
			
			holder.data.addTextChangedListener(new TextWatcher() {
	
				@Override
				public void afterTextChanged(Editable s) {
					//Log.v("Teksti muuttui " + position, s.toString());
					values.get(position-litems.size()).setContent(s.toString());
				}
	
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
				}
	
				@Override
				public void onTextChanged(CharSequence s, int start, int before,
						int count) {
					//Log.v("Teksti muuttui " + position, s.toString());
				}
				
			});
			
			holder.data.setOnEditorActionListener(new OnEditorActionListener(){

				@Override
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
					if (actionId == EditorInfo.IME_ACTION_NEXT) {
						if (position < values.size()-1) {
							Log.v("Position", ""+position);
							(((ViewHolder)values.get(position+1).getTag()).data).requestFocus();
							((ListView)parent).setSelection(position+1);
							//TODO
							return true;
						}
						
					} else if (actionId == EditorInfo.IME_ACTION_DONE) {
						InputMethodManager imm = (InputMethodManager)
								context.getSystemService(
							      Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
					}
					
					return false;
				}
			});
			
			//adding onclick listener for delete
			holder.delete_button.setOnClickListener(new OnClickListener () {
	
				@Override
				public void onClick(View v) {
					//removing item from the list
					values.remove(position-litems.size());
					
					//creating two string lists of current items
					List<String> content = new ArrayList<String>();
					List<String> type = new ArrayList<String>();
					
					//then making contents to those
					for (int i = 0; i < values.size(); ++i) {
						content.add(values.get(i).getContent());
						type.add(values.get(i).getType().toString());
					}
					
					//if we are modifying
					if (window == 1) {
						//creating new modify item window
						ModifyItemWindow mod_item = 
								new ModifyItemWindow((IntervalNotification)context);
						
						mod_item.setVisible(ttr);
						//loading items into data structures
						mod_item.loadItems(content,type);
						
						mod_item.setStates(category_text, adding_interval, 
								is_minute_interval, date);
						
						//updating window contents
						mod_item.loadWindowContent();
						
						
					} else if (window == 0) {
						//and if we're creating window
						
						//creating new add item window
						AddItemWindow add_item = new AddItemWindow((IntervalNotification)context);
	
						
						//finally creating new lists
						add_item.loadItems(content, type);
						
						add_item.setStates(category_text, adding_interval, 
								is_minute_interval, date);
						//and loading window
						add_item.loadWindowContent();
					}
				}
			});
			
			holder.delete_button.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (event.getAction() == MotionEvent.ACTION_DOWN) {
						((ImageView)v).setImageResource(R.drawable.delete_button_down);
					} else if (event.getAction() == MotionEvent.ACTION_UP ||
							event.getAction() == MotionEvent.ACTION_CANCEL ||
							event.getAction() == MotionEvent.ACTION_OUTSIDE) {
						((ImageView)v).setImageResource(R.drawable.delete_button_up);
					}
					return false;
				}
				
			});
			
			holder.file_button.setOnClickListener(new OnClickListener() {
	
				@Override
				public void onClick(View v) {
					((IntervalNotification)context).getIntent()
					.putExtra(FileBrowserActivity.INDEX_IDENTIFIER, position-litems.size());
					
					Intent intent = new Intent(context,FileBrowserActivity.class);
					((IntervalNotification)context)
					.startActivityForResult(intent, IntervalNotification.file_browser_result);
				}
				
			});
			
			holder.file_button.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent motion) {
					if (motion.getAction() == MotionEvent.ACTION_DOWN) {
						((ImageView)v).setImageResource(R.drawable.from_file_button_down);
					} else if (motion.getAction() == MotionEvent.ACTION_UP ||
							motion.getAction() == MotionEvent.ACTION_CANCEL ||
							motion.getAction() == MotionEvent.ACTION_OUTSIDE) {
						((ImageView)v).setImageResource(R.drawable.from_file_button_up);
					}
					return false;
				}
				
			});
			
			holder.camera_button.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick (View v) {
					((IntervalNotification)context).getIntent()
					.putExtra(FileBrowserActivity.INDEX_IDENTIFIER, position-litems.size());
					
					Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
					
					File photo = new File( Environment.getExternalStoragePublicDirectory(
							Environment.DIRECTORY_DCIM), 
							"InNo"+System.currentTimeMillis()+".jpg");
					intent.putExtra(MediaStore.EXTRA_OUTPUT,
							Uri.fromFile(photo));
					
					((IntervalNotification)context).getIntent()
					.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo).getPath());
					
					((IntervalNotification)context)
					.startActivityForResult(intent, IntervalNotification.camera_result);
	
				}
			});
			
			holder.camera_button.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (event.getAction() == MotionEvent.ACTION_DOWN) {
						((ImageView)v)
						.setImageResource(R.drawable.camera_button_down);
					} else if (event.getAction() == MotionEvent.ACTION_UP ||
							event.getAction() == MotionEvent.ACTION_CANCEL ||
							event.getAction() == MotionEvent.ACTION_OUTSIDE) {
						((ImageView)v)
						.setImageResource(R.drawable.camera_button_up);
					}
					return false;
				}
				
			});
		
		} else if (position >= litems.size()) {
			//*****------------*****
			//this is the add button
			//*****------------*****
			convertView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					//creating two string lists of current items
					List<String> content = new ArrayList<String>();
					List<String> type = new ArrayList<String>();
					
					//then making contents to those
					for (int i = 0; i < values.size(); ++i) {
						content.add(values.get(i).getContent());
						type.add(values.get(i).getType().toString());
					}
					content.add("");
					type.add(Datatype.TEXT.toString());
					
					//if we are modifying
					if (window == 1) {
						//creating new modify item window
						ModifyItemWindow mod_item = 
								new ModifyItemWindow((IntervalNotification)context);
						
						mod_item.setVisible(ttr);
						//loading items into data structures
						mod_item.loadItems(content,type);
						
						mod_item.setStates(category_text, adding_interval, 
								is_minute_interval, date);
						
						//updating window contents
						mod_item.loadWindowContent();
						
						
					} else if (window == 0) {
						//and if we're creating window
						
						//creating new add item window
						AddItemWindow add_item = new AddItemWindow((IntervalNotification)context);
	
						
						//finally creating new lists
						add_item.loadItems(content, type);
						
						add_item.setStates(category_text, adding_interval, 
								is_minute_interval, date);
						
						//and loading window
						add_item.loadWindowContent();
					}									
				}
				
			});
			
			convertView.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (event.getAction() == MotionEvent.ACTION_DOWN) {
						((ImageView)v).setImageResource(R.drawable.add_button_down);
					} else if (event.getAction() == MotionEvent.ACTION_UP ||
							event.getAction() == MotionEvent.ACTION_CANCEL ||
							event.getAction() == MotionEvent.ACTION_OUTSIDE) {
						((ImageView)v).setImageResource(R.drawable.add_button_up);
					}
					return false;
				}
				
			});
			
		} else {
			//*****--------*****
			//this is the header
			//*****--------*****
			InterfaceImplementation iface = new InterfaceImplementation(context);
			
			//category field
			final AutoCompleteTextView category = (AutoCompleteTextView)
					(window == 0 ? 
					convertView.findViewById(R.id.category_input_text):
					convertView.findViewById(R.id.modify_item_category_input_text));
			
			if (category.getText().toString() != category_text)
			category.setText(category_text);
			
			category.setOnEditorActionListener(new OnEditorActionListener(){

				@Override
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
					if (actionId == EditorInfo.IME_ACTION_NEXT) {
						if (position < getCount()-1 && position == 0) {
							(((ViewHolder)values.get(1).getTag()).data).requestFocus();
							((ListView)parent).setSelection(position+1);
							//TODO
							//category.requestFocus(View.FOCUS_DOWN);
						}
						return true;
					}
					
					return false;
				}
			});
			
			category.addTextChangedListener(new TextWatcher() {

				@Override
				public void afterTextChanged(Editable s) {
					category_text = s.toString();
				}

				@Override
				public void beforeTextChanged(CharSequence arg0, int arg1,
						int arg2, int arg3) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onTextChanged(CharSequence arg0, int arg1,
						int arg2, int arg3) {
					// TODO Auto-generated method stub
					
				}
				
			});

			//checkbox for add interval
			CheckBox add_interval = (CheckBox)
					(window == 0 ?
					convertView.findViewById(R.id.add_as_interval) :
					convertView.findViewById(R.id.modify_item_add_as_interval) 	);
			
			if (add_interval.isChecked() != adding_interval)
			add_interval.setChecked(adding_interval);
						
			//checkbox for minute interval
			//TODO check for errors
			final CheckBox minute_interval = (CheckBox) 
					convertView.findViewById(R.id.is_minute_interval);
			
			if (minute_interval.isChecked() != is_minute_interval)
			minute_interval.setChecked(is_minute_interval);
						
			//date
			final EditText date_field = (EditText)
					(window == 0 ?
					convertView.findViewById(R.id.add_item_iterval_starting_date):
					convertView.findViewById(R.id.modify_item_iterval_starting_date));
			
			date_field.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent arg1) {
					if (!minute_interval.isChecked()) {
						((IntervalNotification)context).dateDialogClick(v);
						return true;
					} 
					
					return false;
									
				}
				
			});
			
			date_field.setText(date);
			
			date_field.addTextChangedListener(new TextWatcher () {

				@Override
				public void afterTextChanged(Editable s) {
					date = s.toString();
					
				}

				@Override
				public void beforeTextChanged(CharSequence arg0, int arg1,
						int arg2, int arg3) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onTextChanged(CharSequence arg0, int arg1,
						int arg2, int arg3) {
					// TODO Auto-generated method stub
					
				}
				
			});
			
			//shows all categories in category textfield
			ImageView show_categories_button = (ImageView)
					(window == 0 ? 
					convertView.findViewById(R.id.add_item_threshold_trigger_button):
					convertView.findViewById(R.id.modify_item_threshold_trigger_button));
	
			//list of all categories
			List<Category> categories = iface.listAllCategories();
			
			//adapter for category text field
			ArrayAdapter<Category> category_adapter = new CategoryAdapter(context, categories);
			
			//setting threshold
			category.setThreshold(1);
			
			//setting categorys adapter
			category.setAdapter(category_adapter);
			
			//initializing minuteinterval just in case
			if (minute_interval.isChecked() && date == "") {
				date_field.setHint("");
				date_field.setText("1");
			} else if (date == "") {
				//getting this day
				Calendar now = Calendar.getInstance();
				DateFormat df = DateFormat.getDateInstance();
				date_field.setText(df.format(now.getTime()));
			}
			
			if (add_interval.isChecked()) {
				//first setting necessary fields visible
				minute_interval.setVisibility(View.VISIBLE);
				date_field.setVisibility(View.VISIBLE);
				
				//adding listener to minuteinterval change
				minute_interval.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						is_minute_interval = isChecked;
						
						if (isChecked) {
							date_field.setHint("");
							date_field.setText("1");
						} else {
							//getting this day
							Calendar now = Calendar.getInstance();
							DateFormat df = DateFormat.getDateInstance();
							date_field.setText(df.format(now.getTime()));
						}
					}
					
				});
				
			} else {
				//making all unnecessary field gone from the view
				minute_interval.setVisibility(View.GONE);
				date_field.setVisibility(View.GONE);
			}
			
			//setting litener for interval checking
			add_interval.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton checkbox, boolean value) {
					adding_interval = value;
					
					if (value) {
						//first setting necessary fields visible
						minute_interval.setVisibility(View.VISIBLE);
						date_field.setVisibility(View.VISIBLE);
					} else {
						minute_interval.setVisibility(View.GONE);
						date_field.setVisibility(View.GONE);
					}
				}
				
			});
			
			show_categories_button.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent motion) {
					switch(motion.getAction()) {
						case MotionEvent.ACTION_DOWN: {
							if (dropDownIsShowing()) {
								category.showDropDown();
								((ImageView)v)
								.setImageResource(R.drawable.extend_categories_button_down);
							} else {
								((ImageView)v)
								.setImageResource(R.drawable.extend_categories_button_up);
							}
	
							return true;
						}
						
						case MotionEvent.ACTION_UP: {
							return true;
						}
					}
					return false;
				}
				
			});
			
			//then checking for interval that's already there
			if (window == 1) {
				ImageView interval_delete_button = (ImageView)
				convertView.findViewById(R.id.modify_item_delete_interval_button);
				
				ttr = iface.getTTR(ttr.getId());
				if (ttr.getIntervalId() > 0) {
					//if we have some interval we show the delete button
					interval_delete_button.setVisibility(View.VISIBLE);
					add_interval.setChecked(false);
					add_interval.setVisibility(View.GONE);
					
					interval_delete_button.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View arg0) {
							InterfaceImplementation iface = new InterfaceImplementation(context);
							final Interval temp_interval = 
									iface.getInterval(ttr.getIntervalId(), false);
							//iface.removeInterval(temp_interval);
							
							//setting up message when this is called
							final String cs = context.getString(R.string.interval_from) + " " 
									+ ttr.getInfob().getInfo() + 
									" " + context.getString(R.string.was_deleted);
							
							AlertDialog.Builder builder = new AlertDialog.Builder(context);
							builder.setMessage(context.getString(R.string.delete) + " " +
									context.getString(R.string.interval) + " " +
									ttr.getInfob().getInfo())
									.setCancelable(false)
									.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
							           public void onClick(DialogInterface dialog, int id) {
											//setting up time this notification is displayed
											int duration = Toast.LENGTH_SHORT;
											InterfaceImplementation iface = new InterfaceImplementation(context);		
											iface.removeInterval(temp_interval);
											
											//moving back to this item
											ttr = iface.getTTR(ttr.getId());
											((IntervalNotification) context).moveToModifyItem(ttr);
											
											Toast toast = Toast.makeText(context, cs, duration);
											toast.show();
							                dialog.cancel();
							           }
							       })
							       .setNegativeButton("No", new DialogInterface.OnClickListener() {
							           public void onClick(DialogInterface dialog, int id) {
							                dialog.cancel();
							           }
							       });
								
							AlertDialog alert = builder.create();
							
							alert.show();
						}
						
					});
					
					interval_delete_button.setOnTouchListener(new OnTouchListener() {

						@Override
						public boolean onTouch(View v, MotionEvent event) {
							if (event.getAction() == MotionEvent.ACTION_DOWN) {
								((ImageView)v)
								.setImageResource(R.drawable.delete_interval_button_down);
							} else if (event.getAction() == MotionEvent.ACTION_UP ||
									event.getAction() == MotionEvent.ACTION_CANCEL ||
									event.getAction() == MotionEvent.ACTION_OUTSIDE) {
								((ImageView)v)
								.setImageResource(R.drawable.delete_interval_button_up);
							}
							return false;
						}
						
					});
					
				} else {
					interval_delete_button.setVisibility(View.GONE);
					add_interval.setVisibility(View.VISIBLE);
					add_interval.setChecked(true);	
				}
			}
		}
				
		return convertView;	
	}
	
	private boolean dropDownIsShowing() {
		drop_down = !drop_down;
		return !drop_down;
	}
	
	class ViewHolder {
		ImageView delete_button;
		EditText data;
		ImageView file_button;
		ImageView camera_button;
	}
}
