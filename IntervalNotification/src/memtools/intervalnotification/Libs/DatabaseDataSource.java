//this class is for managing database connection (and creating it)
//it provides all necessary services needed to add information to 
//database and retrieving it

package memtools.intervalnotification.Libs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DatabaseDataSource {
	//public stuff
	boolean lock = false;
	
	//constructor
	//takes database context as parameter
	public DatabaseDataSource (Context context) {
		this.context = context;
		mydbhelper = new MySQLiteHelper(context);
	}
	
	//opens db connection
	//throws exception if opening didn't succeed
	public void open() throws SQLException {
		mydbhelper = new MySQLiteHelper(context);
		database = mydbhelper.getWritableDatabase();
	}
	
	//closes db connection
	public void close() {
		database.close();
	}
	
	//private stuff
	private SQLiteDatabase database;
	private MySQLiteHelper mydbhelper;
	private Context context;
		
	//creates category in database and returns category class
	public Category createCategory (String name) {
		Category newcategory = new Category();
		
		try {
			//opening database connection
			open();
			//creating the category
			newcategory = Category.create(database, name);
			//closing database connection
			close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return newcategory;
	}

	//deletes category from database
	//TODO
	//make this better when deleting category is possible
	public void deleteCategory (Category category) {
		long id = category.getId();
		database.delete(MySQLiteHelper.TABLE_CATEGORY, 
				MySQLiteHelper.COLUMN_ID + " = " + id, null);
	}
		
	//creates link between category and thing to remember
	public CategoryTTR createCategoryTTR (Integer category_id, Integer ttr_id) {
		CategoryTTR new_cat_ttr = new CategoryTTR(0,0);
		
		try {
			//opening database connection
			open();
			//creating the link
			new_cat_ttr = CategoryTTR.create(database, category_id, ttr_id);
			//closing the database
			close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return new_cat_ttr;
	}
	
	//deletes link between category and ttr from the database
	//TODO
	//when destroying links between category and thingtoremember becomes
	//possible we recode this
	public void deleteCategoryTTR (CategoryTTR categoryttr) {
		long category_id = categoryttr.getCategoryId();
		long ttr_id = categoryttr.getTTRId();
		
		database.delete(MySQLiteHelper.TABLE_CATEGORY_THING_TO_REMEMBER,
				MySQLiteHelper.CATEGORY_ID + " = " + category_id + " and " +
				MySQLiteHelper.TTR_ID + " = " + ttr_id, null);
	}
	
	//lists all categories from the database
	//TODO
	//move this to categories
	public List<Category> getAllCategories () {
		//making new category list
		List<Category> categories = new ArrayList<Category>();
		
		try {
			//opening database connection
			open();
			//starting the transaction
			database.beginTransaction();
			try {
				//making the query
				Cursor cursor = database.query(MySQLiteHelper.TABLE_CATEGORY,
						Category.allColumns, null,
						null,null,null,null);
													
				if (cursor.getCount() > 0) {
				
					//moving to first position
					cursor.moveToFirst();
				
					categories.add(Category.cursorTransform(cursor));
				
					while (!cursor.isLast()) {
						cursor.moveToNext();
						//adding rest
						categories.add(Category.cursorTransform(cursor));
					}
				}
				
				cursor.close();
				database.setTransactionSuccessful();
			} finally {
				database.endTransaction();
			}
			
			//closing database
			close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
			
		return categories;
	}
	
	public List<Data> getTTRData (ThingToRemember ttr) {
		//initializing the list
		List<Data> data_list = new ArrayList<Data>();
		
		try {
			//opening database connection
			open();
			if (ttr != null)
			data_list = Data.findData(database, ttr);
			//closing database connection
			close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return data_list;
		
	}
	
	//just creates and returns thing to remember
 	public ThingToRemember createThingToRemember () {
 		ThingToRemember newttr = new ThingToRemember();
 		
 		try {
	 		//opening database connection
	 		open();
	 		//creating new thing to remember
	 		newttr = ThingToRemember.create(database);		
			//closing database connection
			close();
 		} catch (SQLException e) {
 			e.printStackTrace();
 		}
		
		return newttr;
	}
 	
	//deletes thing to remember from the database
	public void deleteThingToRemember (ThingToRemember thingtoremember) {
		
		try {
			//first opening database connection
			open();
			
			//then we have to make sure we delete interval first 
			//(drawback of easier interval access)
			if (thingtoremember.getIntervalId() != -1) {
				if (Interval.delete(database, thingtoremember.getIntervalId())) {
					//then we can freely delete ttr, 
					//because we have cascade to data on deletion
					ThingToRemember.delete(database, thingtoremember);
				}
			} else {
				//then we can freely delete ttr, 
				//because we have cascade to data on deletion
				ThingToRemember.delete(database, thingtoremember);
			}
			
			//closing database connection
			close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	//gets most recent info about this thingtoremember
	public ThingToRemember getTTR(long ttr_id) {
		ThingToRemember temp_ttr = new ThingToRemember ();
		try {
			//opening database connection
			open();
			temp_ttr = ThingToRemember.getTTR(database, ttr_id);
			//closing database connection
			close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return temp_ttr;
	}
	
	//gets info from the class, might require access to database
	public DataSmallInfo getTTRInfo (ThingToRemember ttr) {
		DataSmallInfo tempstring = new DataSmallInfo();
		try {
			//opening the database
			open();
			if (ttr != null)
			tempstring = ttr.getInfo(database);		
			//closing the database
			close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tempstring;
	}
	
	public List<ThingToRemember> getAllTTR(String category, boolean include_interval){
		//initialising list of ttrs
		List<ThingToRemember> ttrs = new ArrayList<ThingToRemember>();
		
		try {
			//opening database connection
			open();
			//getting all ttrs
			ttrs = ThingToRemember.getAll(database, category, include_interval);				
			//closing database connection
			close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return ttrs;
	}
	
	//helper function to create single interval
	//parameters: type is type, ttr_id is the id of "ThingToRemember", 
	//lenght first interval lenght
	//returns just created interval
	public Interval createInterval (int length, Date date, 
			ThingToRemember ttr, Intervaltype type) {
		Interval new_interval = new Interval();
		try {
			//opening database connection
			open();
			//creating new interval
			new_interval = Interval.create(database, length, date, ttr, type);
			//closing database connection
			close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return new_interval;
	}
	
	//deletes interval from the database
	public boolean deleteInterval (Interval interval) {
		boolean success = false;
		try {
			//opening database connection
			open();
			success = Interval.delete(database, interval.getId());
			//closing database connection
			close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return success;
	}
	
	//gets all intervals with sql clause
	public List<Interval> getAllIntervals (int active) {
		List<Interval> tempintervals = new ArrayList<Interval>();
		
		//opening database connection
		try {
				open();
				//getting all intervals
				tempintervals = Interval.getAll(database, active);
				//closing database connection
				close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return tempintervals;
	}
	
	//updates interval as it is given in class
	//nothing else is done...
	//has multiple parameter which set true disables opening new database 
	//connection and assumes it has been handled somewhere else
	public void updateInterval (Interval interval, boolean multiple) {
		
		try {
			//opening database connection
			if (!multiple)
			open();
			//updating interval
			Interval.update(database, interval);
			//closing database connection
			if (!multiple)
			close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	//helper function to create single data
	//parameters: data is data, type is type, ttr_id is the id of 
	//"ThingToRemember"
	//returns data that has just been input
	public Data createData (Data data, Integer ttr_id) {
		Data tempdata = new Data();
		
		try {
			//opening database connection
			open();
			//creating new data
			tempdata = Data.create(database, data, ttr_id);		
			//closing database connection
			close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return tempdata;
	}
	
	//deletes data from the database
	public void deleteData (Data data) {
		try {
			//opening database connection
			open();
			Data.delete(database, data);
			//closing database connection
			close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	//has multiple parameter which set true will NOT open database connection
	//here and assumes it has been opened somewhere else
	public Interval getInterval(long id, boolean multiple) {
		Interval temp_interval = new Interval();
		
		try {
			//first opening database connection
			if (!multiple)
			open();
			//then constructing interval from the database
			temp_interval = Interval.construct(database, id);
			//closing database connection
			if (!multiple)
			close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return temp_interval;
	}

	public boolean updateCategory(Category category) {
		boolean result = false;
		try {
			//opening database connection
			open();
			result = category.update(database);
			//closing database connection
			close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public void updateStatistics(Repetition rep) {
		try {
			//opening database connection
			open();
			//updating
			rep.update(database);
			//closing database connection
			close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public CompoundStatistics getStatistics() {
		CompoundStatistics stats = new CompoundStatistics();
		
		try {
			//opening database connection
			open();
			stats = Repetition.getStatistics(database);
			//closing database connection
			close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return stats;
	}
}
