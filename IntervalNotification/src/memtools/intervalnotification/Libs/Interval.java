package memtools.intervalnotification.Libs;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import memtools.intervalnotification.IntervalNotification;

import android.content.ContentValues;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class Interval implements Comparable<Interval>{
	//public stuff
	
	public static int INACTIVE = 0;
	public static int ACTIVE = 1;
	public static int ALL = 2;
	
	//static columns
	public static String allColumns[] = { MySQLiteHelper.TABLE_INTERVAL + 
		"." + MySQLiteHelper.COLUMN_ID,
		MySQLiteHelper.TABLE_INTERVAL + 
		"." + MySQLiteHelper.COLUMN_TYPE,
		MySQLiteHelper.TABLE_INTERVAL + 
		"." + MySQLiteHelper.COLUMN_ACTIVE,
		MySQLiteHelper.COLUMN_PREVIOUS_DATE,
		MySQLiteHelper.TABLE_INTERVAL + 
		"." + MySQLiteHelper.COLUMN_NEXT_DATE,
		MySQLiteHelper.TABLE_INTERVAL + 
		"." + MySQLiteHelper.COLUMN_LENGTH };
	
	//constructor
	public Interval () {
		is_active = true;
		id = -1;
	}
	
	//id operations
	public long getId() { return id; }
	public void setId(Integer id) { this.id = id; }
	
	//type operations
	public Intervaltype getType() { return type; }
	public void setType(Intervaltype type) { this.type = type; }
	
	//activity operations
	public boolean isActive() { return is_active; }
	public void setActive(boolean activity) { is_active = activity; }
	
	//previous date operations
	public Calendar getPrevDate() { 
		Calendar prev = Calendar.getInstance();
		prev.setTimeInMillis(prev_date);
		return prev; 
	}
	public void setPrevDate(long prev_date) {
		this.prev_date = prev_date;
	}
	
	//next date operations
	public Calendar getNextDate() {
		Calendar next = Calendar.getInstance();
		next.setTimeInMillis(next_date);
		return next; 
	}
	public void setNextDate(long next_date) { this.next_date = next_date; }
	
	//length operations
	public int getLength() { return prev_interval_length; }
	public void setLength(int length) { prev_interval_length = length; }
	
	//thing to remember operations
	public ThingToRemember getTTR () { return ttr; }
	public void setTTR (ThingToRemember ttr) { this.ttr = ttr; }
	
	//tag operations
	public void setTag(Object tag) { this.tag = tag; }
	public Object getTag() { return tag; }
	
	//overriding compare operator
	public int compareTo(Interval interval) {
		int cat_cmp = getTTR().getInfob().getCategory()
				.compareTo(interval.getTTR().getInfob().getCategory());
		if (isActive() && interval.isActive()) {
			return cat_cmp;
		} else if (isActive() && !interval.isActive()) {
			return -1;
		} else if (!isActive() && interval.isActive()) {
			return 1;
		} else {
			return cat_cmp;
		}
	}
	
	//returns all intervals active 0 means inactive, 1 active and 2 all intervals
	//we must remember this literally gets all certain class intervals
	//so no processing is done here (at least not yet...)
	public static List<Interval> getAll (SQLiteDatabase database, int active) {
		//creating new array
		List<Interval> tempintervals = new ArrayList<Interval>();
		
		//cursor
		Cursor cursor;
		
		//starting transaction
		database.beginTransaction();
		
		try {
			String[] columns = {Interval.allColumns[0],  Interval.allColumns[1],
					Interval.allColumns[2],
					Interval.allColumns[3],
					Interval.allColumns[4],
					Interval.allColumns[5],
					ThingToRemember.allColumns[0],
					ThingToRemember.allColumns[1]};
			
			//manually creating query for the database
			//select clause
			String select = "SELECT " + 
			columns[0] + ", " +
			columns[1] + ", " +
			columns[2] + ", " +
			columns[3] + ", " +
			columns[4] + ", " +
			columns[5] + ", " +
			columns[6] + ", " +
			columns[7] + " ";
			
			//from clause
			String from = "FROM " + 
			MySQLiteHelper.TABLE_INTERVAL + ", " +
			MySQLiteHelper.TABLE_THING_TO_REMEMBER + " ";
			
			//where clause
			String where = "WHERE " + 
			columns[0] + " = " + columns[7] + " ";
			
			//creating current time
			Calendar now = Calendar.getInstance();
			now.add(Calendar.DAY_OF_MONTH, 1);
			String future = ""+now.getTimeInMillis();
			now.add(Calendar.DAY_OF_MONTH, -2);
			String past = ""+now.getTimeInMillis();
			
			//TODO make sure this works...
			String date_cmp = columns[3] + " < " + future + " AND " +
					columns[3] + " > " + past;
			
			//TODO sometime we have to see if we can get fully updated stuff
			//so we can reduce some of the results
			if (active != Interval.ALL) 
				where += " AND " + columns[2] + " = " +
					(active == Interval.ACTIVE ? "1" : 
						"0 AND " + date_cmp ) +
					  " ";
						
			String order_by = "ORDER BY " + columns[0] + " ASC";
			
			String search = select + from + where + order_by + ";";
			
			cursor = database.rawQuery(search, null);
			
			//now we have certain class of intervals
			//and we want to update them
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				int index = 0;
				tempintervals.add(Interval.cursorTransform(cursor));
				
				//transforming thing to remember manually
				ThingToRemember tempttr = 
						ThingToRemember.getTTR(database, cursor.getInt(6));
				//updating ttr info
				tempttr.getInfo(database);

				//finally setting ttr to interval 
				tempintervals.get(index).setTTR(tempttr);
				
				while (!cursor.isLast()) {
					cursor.moveToNext();
					++index;
					tempintervals.add(Interval.cursorTransform(cursor));
					
					//transforming thing to remember manually
					ThingToRemember tempttr1 = 
							ThingToRemember.getTTR(database, cursor.getInt(6));
					//updating ttr info
					tempttr1.getInfo(database);
					//finally setting ttr to interval 
					tempintervals.get(index).setTTR(tempttr1);
				}
			}
			cursor.close();
			database.setTransactionSuccessful();
		} catch (SQLException e) {
			Log.e("INTERVAL", "Couldn't list intervals: " + e);
			e.printStackTrace();
			return new ArrayList<Interval>();
		} finally {
			database.endTransaction();
		}
		
		return tempintervals;
	}
	
	//creates new interval
	public static Interval create (SQLiteDatabase database, int length, 
			Date date, ThingToRemember ttr, Intervaltype type) {
		
		//Creating content values
		ContentValues content = new ContentValues();
		//putting stuff into content
		content.put(MySQLiteHelper.COLUMN_TYPE, type.toString());
		content.put(MySQLiteHelper.COLUMN_ACTIVE, 0);
		content.put(MySQLiteHelper.COLUMN_PREVIOUS_DATE, String.valueOf(date.getTime()));
		content.put(MySQLiteHelper.COLUMN_NEXT_DATE, String.valueOf(date.getTime()));
		content.put(MySQLiteHelper.COLUMN_LENGTH, length);
		
		//creating cursor
		Cursor cursor;
		
		//creating interval
		Interval new_interval = new Interval();
		
		//assigning ttr to this interval
		new_interval.setTTR(ttr);
		
		//starting transaction
		database.beginTransaction();
		
		try {
			//first we need to check wither we have already interval
			//for this item
			if (ttr.getIntervalId() == -1) {
				//trying to insert this new interval
				long insertId = database.insert(MySQLiteHelper.TABLE_INTERVAL, 
						null, content);
				
				//making sure we have it and making the interval
				cursor = database.query(MySQLiteHelper.TABLE_INTERVAL, 
						allColumns, MySQLiteHelper.COLUMN_ID + " = " + insertId,
						null,null,null,null);
				
				//making sure we have something
				if (cursor.getCount() > 0) {
					//getting the first (and only) item
					cursor.moveToFirst();
					
					//transforming to into interval
					new_interval = cursorTransform(cursor);
					
					//finally updating thing to remember
					ttr.update(database, (int)new_interval.getId());
				}
			}
			
			database.setTransactionSuccessful();
		} catch (SQLException e) { 
			Log.e("INTERVAL", "CREATION FAILED");
			database.endTransaction();
			return new Interval();
		} finally {
			database.endTransaction();
		}
		
		return new_interval;
	}
	
	//updates interval
	public static void update (SQLiteDatabase database, Interval interval) {
		//creating content values
		ContentValues content = new ContentValues();
		//adding right stuff into content
		content.put(MySQLiteHelper.COLUMN_TYPE, interval.getType().toString());
		if (interval.isActive()) {
		content.put(MySQLiteHelper.COLUMN_ACTIVE, 1);
		} else {
			content.put(MySQLiteHelper.COLUMN_ACTIVE, 0);
		}

		content.put(MySQLiteHelper.COLUMN_PREVIOUS_DATE, 
				String.valueOf(interval.getPrevDate().getTimeInMillis()));
		content.put(MySQLiteHelper.COLUMN_NEXT_DATE, 
				String.valueOf(interval.getNextDate().getTimeInMillis()));
		content.put(MySQLiteHelper.COLUMN_LENGTH, interval.getLength());
		
		//starting transaction
		database.beginTransaction();
		
		try {
			//updating...
			database.update(MySQLiteHelper.TABLE_INTERVAL, content, 
					MySQLiteHelper.COLUMN_ID + " = " + interval.getId(), null);
			
			database.setTransactionSuccessful();
		} finally {
			database.endTransaction();
		}
	}
	
	//deltetes interval
	public static boolean delete (SQLiteDatabase database, long id) {
		//starting transaction
		database.beginTransaction();
		int deleted_rows;
		
		try {
			deleted_rows = database.delete(MySQLiteHelper.TABLE_INTERVAL, 
					MySQLiteHelper.COLUMN_ID + " = " + id, null);
			
			ContentValues content = new ContentValues();
			content.putNull(MySQLiteHelper.INTERVAL_ID);
			//updating ttr table
			database.update(MySQLiteHelper.TABLE_THING_TO_REMEMBER,
					content, MySQLiteHelper.INTERVAL_ID + " = " + id , null);
			
			database.setTransactionSuccessful();
		} finally {
			database.endTransaction();
		}
		
		if (deleted_rows > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	//transforms interval cursor to interval
	public static Interval cursorTransform (Cursor cursor) {
		Interval tempinterval = new Interval();
		Intervaltype tempintervaltype;

		//assigning minute or day interval
		if (cursor.getString(1).contentEquals(Intervaltype.MINUTE.toString())) {
			tempintervaltype = Intervaltype.MINUTE;
		} else {
			tempintervaltype = Intervaltype.DAY;
		}
		
		//setting id
		tempinterval.setId(cursor.getInt(0));
		//setting type
		tempinterval.setType(tempintervaltype);
		//setting activity
		
		if (cursor.getInt(2) != 0) { tempinterval.setActive(true); }
		else { tempinterval.setActive(false); }

		//setting previous date
		
		tempinterval.setPrevDate(Long.valueOf(cursor.getString(3)));
		tempinterval.setNextDate(Long.valueOf(cursor.getString(4)));
		
		//setting interval length
		tempinterval.setLength(cursor.getInt(5));
		
		return tempinterval;
	}
	
	//constructs new interval from the id
	public static Interval construct(SQLiteDatabase database, long id) {
		
		Interval interval = new Interval();
		//starting transaction
		database.beginTransaction();
		
		try {
			String[] columns = {Interval.allColumns[0],  Interval.allColumns[1],
					Interval.allColumns[2],
					Interval.allColumns[3],
					Interval.allColumns[4],
					Interval.allColumns[5],
					ThingToRemember.allColumns[0],
					ThingToRemember.allColumns[1]};
			
			//manually creating query for the database
			//select clause
			String select = "SELECT " + 
			columns[0] + ", " +
			columns[1] + ", " +
			columns[2] + ", " +
			columns[3] + ", " +
			columns[4] + ", " +
			columns[5] + ", " +
			columns[6] + ", " +
			columns[7] + " ";
			
			//from clause
			String from = "FROM " + 
			MySQLiteHelper.TABLE_INTERVAL + ", " +
			MySQLiteHelper.TABLE_THING_TO_REMEMBER + " ";
			
			//where clause
			String where = "WHERE " + 
			columns[0] + " = " + id + " AND " + 
			columns[7] + " = " + id;
			
			String sql = select + from + where + ";";
			
			Cursor cursor = database.rawQuery(sql, null);
			
			//making the query from interval
			/*Cursor cursor = database.query(MySQLiteHelper.TABLE_INTERVAL + ", " +
					MySQLiteHelper.TABLE_THING_TO_REMEMBER,
					Interval.allColumns, columns[0] + " = " + id + 
					" AND " + columns[7] + " = " + id,
					null,null,null,null);*/
			
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				//creating interval
				interval = Interval.cursorTransform(cursor);
				
				//transforming thing to remember manually
				ThingToRemember tempttr = new ThingToRemember();
				tempttr.setId(cursor.getInt(6));
				//setting interval id
				tempttr.setIntervalId(cursor.getInt(7));
				//finally setting ttr to interval 
				interval.setTTR(tempttr);
			}
			
			cursor.close();
			
			database.setTransactionSuccessful();
		} catch (SQLException e) {
			Log.e("INTERVAL", "Getting interval was unsuccessful");
			database.endTransaction();
			return null;
		} finally {
			database.endTransaction();
		}
				
		return interval;
	}
	
	//this will return information about this class in string format
	@Override
	public String toString() {
		//this shouldn't be this way... changed when needed something else
		return String.valueOf(id);
	}
			
	//private stuff
	
	//id of this data
	private Integer id;
	//interval type
	private Intervaltype type;
	//is interval active
	private boolean is_active;
	//previous date when interval was active
	//private Calendar prev_date;
	private long prev_date;
	//next date when interval becomes active
	private long next_date;
	//private Calendar next_date;
	//length of previous interval
	private Integer prev_interval_length;
	
	//thing to remember this interval is assigned to
	private ThingToRemember ttr;
	
	//we can bind or store some outside object here
	private Object tag;
}
