package memtools.intervalnotification.Libs;

import java.util.ArrayList;
import java.util.List;

import memtools.intervalnotification.R;
import memtools.intervalnotification.Interface.InterfaceImplementation;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class ListIntervalAdapter extends ArrayAdapter<Interval> {
	Context context;
	
	List<Interval> intervals;
	List<Interval> original;
	List<Interval> filtered;
		
	Filter filter;
	
	public ListIntervalAdapter (Context context, List<Interval> intervals) {
		super(context, R.layout.interval, intervals);
		this.context = context;
		this.intervals = new ArrayList<Interval>(intervals);
		this.original = new ArrayList<Interval>(intervals);
		
		filtered = new ArrayList<Interval>();
	}

	//gets list size 
	@Override
	public int getCount() {
		return intervals.size();
	}

	//gets the item from the list
	@Override
	public Interval getItem(int arg0) {

		return intervals.get(arg0);
	}

	//gets the id of the item
	@Override
	public long getItemId(int arg0) {
		return intervals.get(arg0).getId();
	}

	@Override
	public Filter getFilter() {
		
		if (filter == null)
			filter = new IntervalFilter();
				
		return filter;
	}
	
	//get view override
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {	
		//getting the inflater
		LayoutInflater mInflater = LayoutInflater.from(context);
		
		//creating holder
		ViewHolder holder;
		
		//if there is nothing in convert view we just create it
		//if (convertView == null) {
			//inflating the item
			convertView = mInflater.inflate(R.layout.interval, parent, false);
			
			//binding this view into this position
			intervals.get(position).setTag(convertView);
			
			//making new viewholder
			holder = new ViewHolder();
			
			//assigning views
			holder.category = (TextView) 
					convertView.findViewById(R.id.single_interval_category);
			holder.info = (TextView)
					convertView.findViewById(R.id.single_interval_info);
			holder.date = (TextView)
					convertView.findViewById(R.id.single_interval_date);
			
			//setting tag
		/*	convertView.setTag(holder);		
		} else {
			//if we have soemthing we just get it from the tag
			holder = (ViewHolder) convertView.getTag();
		}*/
			
		/*InterfaceImplementation iface = new InterfaceImplementation(context);
		iface.getInterval(intervals.get(position).getId());
		iface.updateTTRInfo(intervals.get(position).getTTR());*/
		if (intervals.get(position).getTTR() != null) {
			//finding maximum lengths
			int cat_length = intervals.get(position).getTTR().getInfob().getCategory().length();
			cat_length = cat_length < 20 ? cat_length : 20;
			
			int info_length = intervals.get(position).getTTR().getInfob().getInfo().length();
			info_length = info_length < 20 ? info_length : 20;
			
			//category assignment
			if (cat_length > 0) {
				holder.category.setText(
						intervals.get(position).getTTR().getInfob().getCategory().subSequence(0, cat_length));
			} else {
				holder.category.setText("");
			}
			
			//info assignment
			if (info_length > 0) {
				holder.info.setText(
						intervals.get(position).getTTR().getInfob().getInfo().subSequence(0, info_length));
			} else {
				holder.info.setText("");
			}
		
		}
		
		//date assignment
		if (intervals.get(position).getNextDate().toString().length() > 0) {
			holder.date.setText(intervals.get(position).getNextDate().getTime().toLocaleString());
		} else {
			holder.date.setText("");
		}

		return convertView;
	}
	
	class ViewHolder {
		TextView category;
		TextView info;
		TextView date;
	}
	
	
	private class IntervalFilter extends Filter{

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			//making new filter results
			FilterResults result = new FilterResults();
			
			String prefix = "";
			
			if (constraint != null)
				prefix = constraint.toString().toLowerCase();
			
			if(constraint != null && prefix.length() > 0) {
				final ArrayList<Interval> filt = new ArrayList<Interval>();
                final ArrayList<Interval> lItems = new ArrayList<Interval>(original);
                
                //getting the size of items
                int count  = lItems.size();
                
                //manually filtering results
                for(int i = 0; i < count; ++i) {
                    final Interval interval = lItems.get(i);
    				String category = interval.getTTR().getInfob().getCategory();
    				String info = interval.getTTR().getInfob().getInfo();
                    final String interval_string = (category + " " + info).toLowerCase();
                    if(interval_string.contains(prefix))
                        filt.add(interval);
                }
                
                //then adding into filtered stuff
                result.count = filt.size();
                result.values = filt;
			} else {
				result.values = new ArrayList<Interval>(original);
				result.count = original.size();
            }
			
			return result;
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void publishResults(CharSequence constraint,
				FilterResults results) {	
			filtered = (ArrayList<Interval>)results.values;
			notifyDataSetChanged();
				
			//setting categories
			intervals = filtered;
			notifyDataSetInvalidated();
		}
		
	}
	
}
