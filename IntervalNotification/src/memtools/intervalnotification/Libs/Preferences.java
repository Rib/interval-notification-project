package memtools.intervalnotification.Libs;

public enum Preferences {
	DAY_TICK, DAYS_INTERVALS, 
	MINUTE_NOTIFICATION, TARGET_SUCCESS_RATE,
	MINIMUM_SUCCESS_RATE,
	ORIENTATION_LOCK
}
