package memtools.intervalnotification.Libs;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import memtools.intervalnotification.IntervalNotification;
import memtools.intervalnotification.R;
import memtools.intervalnotification.Interface.InterfaceImplementation;
import memtools.intervalnotification.ui.ItemViewWindow;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;

public class MinuteInterval implements Runnable {
	Context context;
	
	PendingIntent activity;
	
	ThingToRemember ttr;
	long ttr_id;
	
	public MinuteInterval (Context context, ThingToRemember ttr) {
		this.context = context;
		
		this.ttr = ttr;
		ttr_id = ttr.getId();
	}

	@Override
	public void run() {		
		InterfaceImplementation iface = new InterfaceImplementation(this.context);
				
		SharedPreferences prefs = context.getSharedPreferences(
				IntervalNotification.SETTINGS_FILE,0);
		
		//then we set up a notification if it is on
		if (prefs.getBoolean(Preferences.MINUTE_NOTIFICATION.toString(), true)) {			

			makePendingIntent();
					
			//making up the screen
			PowerManager pm = 
					(PowerManager)context.getSystemService(Context.POWER_SERVICE);
			
			if (!pm.isScreenOn()) {
		        WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK |
		        		PowerManager.ACQUIRE_CAUSES_WAKEUP |
		        		PowerManager.ON_AFTER_RELEASE,"MyLock");
		        wl.acquire(10000);
		        WakeLock wl_cpu = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,"MyCpuLock");
		        wl_cpu.acquire(10000);
			}
		}
		
		iface.updateIntervalActivities();
		
		//checking if we're in a view that requires immediate action to update
		if (((IntervalNotification) 
				context).getCurrentView() == R.layout.daysintervals) {
			//cheating screen to load it again..
			((IntervalNotification)context).setContentView(R.layout.loadscreen);
			((IntervalNotification)context).getTodaysIntervals().updateContent();
			((IntervalNotification)context).getTodaysIntervals().setVisible();
		}
			
	}
	
	//returns delay to next time we set interval active
	public long getNextDelay() {
		//getting current time
		Calendar now = Calendar.getInstance();
		
		InterfaceImplementation iface = new InterfaceImplementation(context);
		Interval interval = iface.getInterval(ttr.getIntervalId(), false);
		
		//first checking if we have already passed current time
		if (interval.getNextDate().getTimeInMillis() < now.getTimeInMillis()) {
			return 0;
		}
		
		//calculates current time in millis (max 24 hours)
		long current_time  = 
				interval.getNextDate().getTimeInMillis()-now.getTimeInMillis();
		
		return current_time;
		
	}
	
	public void makePendingIntent () {
		InterfaceImplementation iface = new InterfaceImplementation(this.context);
		
		//interval = iface.getInterval(interval_id,false);

		iface.updateTTRInfo(ttr);
		
		//notification manager
		NotificationManager not_mana = (NotificationManager)
				context.getSystemService(Context.NOTIFICATION_SERVICE);
		
		//we try to avoid system call as System.currentTimeMillis()
		//is that we try to round that with proper calendars current time
		//obviously there will be some delay (hopefully not too much)
		//Calendar now = Calendar.getInstance();
		
		Notification minute_notification;
		
		//creating notification
		if (((IntervalNotification)context).getNotification() == null) {
			minute_notification = new Notification(R.drawable.delete_button_up, 
				context.getText(R.string.new_minute_interval), System.currentTimeMillis());
			((IntervalNotification)context).setNotification(minute_notification);
			minute_notification.number = 1;
		} else {
			minute_notification = ((IntervalNotification)context).getNotification();
			minute_notification.number += 1;
		}
		
		//automatically disappear the notification when it's been clicked (or selected)
		minute_notification.flags |= Notification.FLAG_AUTO_CANCEL;
		
		//intent
		Intent notificationIntent = new Intent(context, IntervalNotification.class);
		//setting new action so that new activity recognizes and move to correct window
		//setting return stuff that is necessary to get to correct view
		notificationIntent.putExtra(IntervalNotification.CURRENT_VIEW, R.layout.thingtoremember);
		
		notificationIntent.putExtra(ItemViewWindow.ttr_id_static, ttr.getId());
		
		//TODO change casts
		notificationIntent.putExtra(ItemViewWindow.data_index_static, 1);
		notificationIntent.putExtra(IntervalNotification.from_notification, true);
		
		//this makes sure we don't create unnecessary activities
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		
		activity = PendingIntent.getActivity(context, 0, 
				notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
		
		minute_notification.setLatestEventInfo(context, 
				context.getString(R.string.minute_interval_header), 
				ttr.getInfob().getInfo(),
				activity);
		
		not_mana.notify(0, minute_notification);
	}
	
	public PendingIntent pendingIntent () { return activity; }
	
	//this reactivates all minute interval notifications (DANGER: not recommended to
	//use for safety only when starting program it's safe)
	public static List<Handler> reactivateMinuteIntervals (Context context) {
		List<Handler> temp_hand = new ArrayList<Handler>();
		
		InterfaceImplementation iface = new InterfaceImplementation(context);
		
		List<Interval> intervals = iface.listAllIntervals();
		
		for (int i = 0; i < intervals.size(); ++i) {
			if (intervals.get(i).getType().equals(Intervaltype.MINUTE)) {
				ThingToRemember ttr = iface.getTTR(intervals.get(i).getTTR().getId());
				//we put handler up for this minute interval
				MinuteInterval mi = new MinuteInterval(context, ttr);
				//settin ghandler
				Handler handler = new Handler();
				//setting delay
				if (mi.getNextDelay() > 0) {
					handler.postDelayed(mi, mi.getNextDelay());
					temp_hand.add(handler);
				}
			}
		}
		
		return temp_hand;
	}
}
