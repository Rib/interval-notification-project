package memtools.intervalnotification.Libs;

import java.util.List;

import memtools.intervalnotification.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class AddIntervalAdapter extends BaseAdapter {	
	List<ThingToRemember> values;
	Context context;
	
	//layout inflater
	private LayoutInflater mInflater;

	//constructor
	public AddIntervalAdapter (Context context, List<ThingToRemember> values) {
		//super(context, R.layout.item_chooser_item, R.id.item_chooser_data_field,values);
		this.values = values;
		this.context = context;
				
	}
	
	//overiding get count...
	@Override
	public int getCount() { return values.size(); }
	
	//overriding getting item
	@Override
    public Object getItem(int arg0) {
        return values.get(arg0);
    }
	
	//overrding getting the items id
    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

	//overrides getview function so we can assign several values to wanted fields
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		//getting inflater from context (IntervalNotification)
		mInflater = LayoutInflater.from(context);
		
		ViewHolder holder;
		
		convertView = mInflater.inflate(R.layout.item_chooser_item, parent, false);

		holder = new ViewHolder();
            
		holder.category = (TextView) 
				convertView.findViewById(R.id.item_chooser_category_field);
		holder.info = (TextView) 
				convertView.findViewById(R.id.item_chooser_data_field);

		
		//getting max lengths of the items
		int cat_length = values.get(position).getInfob().getCategory().length() < 20 ?
				values.get(position).getInfob().getCategory().length() : 20;
		
		int info_length = values.get(position).getInfob().getInfo().length() < 20 ?
				values.get(position).getInfob().getInfo().length() : 20;
		
		//cutting of certain amount of chars from category	
		if (values.get(position).getInfob().getCategory().length() > 0) {
			holder.category.setText(
				values.get(position).getInfob().getCategory().subSequence(0, cat_length));
		} else {
			holder.category.setText("");
		}
		
		//cutting certain amount of chars from info
		if (values.get(position).getInfob().getInfo().length() > 0) {
		 holder.info.setText(
				 values.get(position).getInfob().getInfo().subSequence(0, info_length));
		}else {
			holder.info.setText("");
		}
		
		return convertView;
	}
		
	class ViewHolder {
		TextView category;
		TextView info;
	}

}
