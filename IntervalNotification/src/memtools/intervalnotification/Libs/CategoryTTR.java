//this is object link between category and thing to remember

package memtools.intervalnotification.Libs;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class CategoryTTR {
	//public stuff
	
	public static String allColumns[] = { MySQLiteHelper.CATEGORY_ID, 
		MySQLiteHelper.TTR_ID
	};
	
	//category id operations
	public long getCategoryId () { return category_id; }
	public void setCategoryid (Integer category_id) { this.category_id = category_id; }
	
	//ttr id operations
	public long getTTRId () { return ttr_id; }
	public void setTTRId (Integer ttr_id) { this.ttr_id = ttr_id; }
	
	//constructor
	public CategoryTTR (Integer category_id, Integer ttr_id) {
		this.category_id = category_id;
		this.ttr_id = ttr_id;
	}
	
	//string representation of this class
	@Override
	public String toString() {
		return category_id.toString() + ttr_id.toString();
	}
	
	//creating new link between category and ttr into the database
	public static CategoryTTR create (SQLiteDatabase database, Integer category_id, Integer ttr_id) {
		CategoryTTR new_cat_ttr = new CategoryTTR(category_id, ttr_id);
		
		//creating content values
		ContentValues content = new ContentValues();
		//adding categoryttr into content
		content.put(MySQLiteHelper.CATEGORY_ID, category_id);
		content.put(MySQLiteHelper.TTR_ID, ttr_id);
		
		//starting the transaction
		database.beginTransaction();
		
		try {
			Cursor cursor = database.query(MySQLiteHelper.TABLE_CATEGORY_THING_TO_REMEMBER,
					allColumns, MySQLiteHelper.CATEGORY_ID + " = " + category_id +
					" AND " + MySQLiteHelper.TTR_ID + " = " + ttr_id, 
					null,null,null,null);
			Log.v("New link attempt", ttr_id + " " + category_id + " " + cursor.getCount());
			if (cursor.getCount() == 0) {
				//trying to insert new link
				database.insert(MySQLiteHelper.TABLE_CATEGORY_THING_TO_REMEMBER,
					null, content);
				Log.v("New link", "Inserting link");
				Log.v("New link info", ttr_id + " " + category_id);
			}
			
			database.setTransactionSuccessful();
		} catch (Exception e) {
			Log.e("CategoryTTR","Link already exists");
		} finally {
			database.endTransaction();
		}
		
		return new_cat_ttr;
	}
	
	//private stuff
	private Integer category_id;
	private Integer ttr_id;
}
