package memtools.intervalnotification;

import java.io.IOException;

import android.app.Activity;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.MediaController;
import android.widget.MediaController.MediaPlayerControl;

public class AudioPlayerActivity extends Activity implements 
MediaPlayerControl {
	public static String FILE = "FILE";
	
	private MediaController mMediaController;
	private MediaPlayer mMediaPlayer;
	private Handler mHandler = new Handler();
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.thingtoremember);
		
		//making new media player
		mMediaPlayer = new MediaPlayer();
		//setting media controllers
        mMediaController = new MediaController(this);
        //setting media player for this activity
        mMediaController.setMediaPlayer(AudioPlayerActivity.this);
        //setting anchor
        mMediaController.setAnchorView(findViewById(R.id.thing_view_flipper));
        
        String audio_file = this.getIntent().getStringExtra(FILE);
        
        try {
        	//setting data source
            mMediaPlayer.setDataSource(audio_file);
            //preparing (might take a while if slow)
            mMediaPlayer.prepare();
        } catch (IOException e) {
            Log.e("AudioPlayerActivity", "Could not open file " 
            		+ audio_file + " for playback.", e);
            this.finish();
        }
        
        //setting prepared listener so we know when to start playing
        mMediaPlayer.setOnPreparedListener(new OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
            		//when audio is prepared we start playing
                    mHandler.post(new Runnable() {
                            public void run() {
                                    mMediaController.show(10000);
                                    mMediaPlayer.start();
                            }
                    });
            }
        });
        
	}
	
    @Override
    //handling activity destroy event
    protected void onDestroy() {
        super.onDestroy();
        mMediaPlayer.stop();
        mMediaPlayer.release();
    }

    //methods controller
	@Override
	public boolean canPause() {
		return true;
	}

	@Override
	public boolean canSeekBackward() {
		return false;
	}

	@Override
	public boolean canSeekForward() {
		return false;
	}

	@Override
	public int getBufferPercentage() {
		//making sure we don't divide by zero
		 int percentage = mMediaPlayer.getDuration() > 0 ?
				 (mMediaPlayer.getCurrentPosition() * 100) / 
				 mMediaPlayer.getDuration() : 100;
	        
		 return percentage;
	}

	@Override
	public int getCurrentPosition() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getDuration() {
		return mMediaPlayer.getCurrentPosition();
	}

	@Override
	public boolean isPlaying() {
        return mMediaPlayer.isPlaying();
	}

	@Override
	public void pause() {
        if(mMediaPlayer.isPlaying())
            mMediaPlayer.pause();
	}

	@Override
	public void seekTo(int pos) {
		mMediaPlayer.seekTo(pos);		
	}

	@Override
	public void start() {
		 mMediaPlayer.start();
	}
	
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mMediaController.show();
        
        switch (event.getAction()) {
         	case MotionEvent.ACTION_DOWN: 
         	case MotionEvent.ACTION_UP:
         	this.finish(); 
        }
        
        return false;
    }
    
	@Override
	public void onBackPressed () {
		//overriding back press (for good reason :D)
		this.finish();
	}

}
